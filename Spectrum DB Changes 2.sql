ALTER TABLE `customers` DROP `login_otp`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `payment_type_charge` decimal(15,2) NOT NULL DEFAULT '0' AFTER `vat_charge`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `special_offers`
CHANGE `customer_app_banner_file` `customer_app_banner_file` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `service_type_id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `customer_app_order_id` tinyint(1) NULL AFTER `customer_app_status`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `customer_app_banner` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_rooms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `building_type_id` bigint(20) NOT NULL,
  `building_room_id` bigint(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `building_type_id_building_room_id` (`building_type_id`,`building_room_id`),
  KEY `building_room_id` (`building_room_id`),
  CONSTRAINT `building_type_room_ibfk_1` FOREIGN KEY (`building_type_id`) REFERENCES `building_types` (`id`),
  CONSTRAINT `building_type_room_ibfk_2` FOREIGN KEY (`building_room_id`) REFERENCES `building_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_room_packages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `building_type_room_id` bigint(20) DEFAULT NULL COMMENT 'NULL means Offer',
  `is_offer` tinyint(4) DEFAULT NULL COMMENT '1 means offer',
  `title` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `actual_total_amount` decimal(15,2) NOT NULL,
  `total_amount` decimal(15,2) NOT NULL,
  `customer_app_thumbnail` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  KEY `building_type_room_id` (`building_type_room_id`),
  CONSTRAINT `building_type_room_packages_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `building_type_room_packages_ibfk_2` FOREIGN KEY (`building_type_room_id`) REFERENCES `building_type_room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `is_package` tinyint NULL;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_packages` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `building_type_room_package_id` bigint(20) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_package_id` (`booking_id`,`building_type_room_package_id`),
  KEY `building_type_room_package_id` (`building_type_room_package_id`),
  CONSTRAINT `booking_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_packages_ibfk_2` FOREIGN KEY (`building_type_room_package_id`) REFERENCES `building_type_room_packages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_wallet_credits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `credit_note` varchar(255) NOT NULL,
  `credited_by_user` int(11) DEFAULT NULL,
  `credited_by_customer` bigint(20) unsigned DEFAULT NULL COMMENT 'future use',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reference_id` (`reference_id`),
  UNIQUE KEY `transation_id` (`transaction_id`),
  KEY `customer_id` (`customer_id`),
  KEY `credited_by_user` (`credited_by_user`),
  KEY `credited_by_customer` (`credited_by_customer`),
  CONSTRAINT `customer_wallet_credits_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `customer_wallet_credits_ibfk_2` FOREIGN KEY (`credited_by_user`) REFERENCES `users` (`user_id`),
  CONSTRAINT `customer_wallet_credits_ibfk_3` FOREIGN KEY (`credited_by_customer`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `customer_wallet_credits` (`id`, `reference_id`, `transaction_id`, `customer_id`, `amount`, `credit_note`, `credited_by_user`, `credited_by_customer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	NULL,	NULL,	1,	100.00,	'test',	1,	NULL,	'2023-06-11 17:13:56',	'2023-07-11 17:17:36',	NULL),
(2,	NULL,	NULL,	1,	55.00,	'gfhghhf',	1,	NULL,	'2023-07-11 17:15:16',	'2023-07-11 17:17:36',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_wallet_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `refund_for` bigint(20) unsigned DEFAULT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `amount_before_transaction` decimal(15,2) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `amount_after_transaction` decimal(15,2) NOT NULL,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `transaction_description` text NOT NULL,
  `refund_description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_id` (`transaction_id`),
  UNIQUE KEY `reference_id` (`reference_id`),
  KEY `customer_id` (`customer_id`),
  KEY `refund_for` (`refund_for`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `customer_wallet_transactions_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `customer_wallet_transactions_ibfk_2` FOREIGN KEY (`refund_for`) REFERENCES `customer_wallet_transactions` (`id`),
  CONSTRAINT `customer_wallet_transactions_ibfk_3` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='please note the amount is signed';

INSERT INTO `customer_wallet_transactions` (`id`, `reference_id`, `transaction_id`, `refund_for`, `customer_id`, `amount_before_transaction`, `amount`, `amount_after_transaction`, `booking_id`, `transaction_description`, `refund_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	NULL,	NULL,	NULL,	1,	155.00,	-5.00,	150.00,	NULL,	'',	'',	'2023-07-11 17:26:04',	'2023-07-11 17:26:04',	NULL),
(2,	NULL,	NULL,	NULL,	1,	150.00,	-15.00,	135.00,	NULL,	'',	'',	'2023-04-11 17:26:04',	'2023-07-11 17:26:04',	NULL),
(3,	NULL,	NULL,	1,	1,	135.00,	5.00,	140.00,	NULL,	'',	'',	'2023-04-11 17:26:04',	'2023-07-11 17:26:04',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `payment_types`
ADD `charge_type` enum('F','P') NOT NULL AFTER `charge`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `frequency_discount` decimal(15,2) NULL DEFAULT '0';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `maids`
ADD `maid_name_customer_app` varchar(155) COLLATE 'utf8_general_ci' NULL AFTER `maid_name`;
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------