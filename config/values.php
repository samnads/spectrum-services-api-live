<?php
return [
    'company_name' => env('COMPANY_NAME', "Default Company Name"),
    'customer_avatar_prefix_url' => env('CUSTOMER_AVATAR_PREFIX_URL', "http://example.com/customer_images/"),
    'customer_avatar_default_file' => env('CUSTOMER_AVATAR_DEFAULT_FILE', "default.png"),
    'customer_avatar_storage_path' => env('CUSTOMER_AVATAR_STORAGE_PATH', "undefined"),
    'service_type_img_prefix_url' => env('SERVICE_TYPE_IMG_PREFIX_URL', "http://example.com/service_images/"),
    'maid_avatar_prefix_url' => env('MAID_AVATAR_PREFIX_URL', "http://example.com/maid_images/"),
    'maid_avatar_default_file' => env('MAID_AVATAR_DEFAULT_FILE', "default.png"),
    'special_offer_banner_prefix_url' => env('SPECIAL_OFFER_BANNER_PREFIX_URL', "http://example.com/offer_images/"),
    'package_offer_thumbnail_prefix_url' => env('PACKAGE_OFFER_THUMBNAIL_PREFIX_URL', "http://example.com/uploads/images/package-offers/"),
    'service_addons_img_prefix_url' => env('SERVICE_ADDONS_IMG_PREFIX_URL', "http://example.com/uploads/images/images/services/addons/"),
    'currency_code' => env('CURRENCY_CODE', ""),
    'language_code' => env('LANGUAGE_CODE', ""),
    'vat_percentage' => env('VAT_PERCENTAGE', 0),
    //
    'booking_slot_start' => env('BOOKING_SLOT_START'),
    'min_working_hours' => env('MIN_WORKING_HOURS'),
    'booking_slot_end' => env('BOOKING_SLOT_END'),
    'work_end_time' => env('WORK_END_TIME'),
    'customer_login_otp_expire_seconds' => (int) env('CUSTOMER_LOGIN_OTP_EXPIRE_SECONDS', 60),
    // payfort
    'payfort_environment' => env('PAYFORT_ENVIRONMENT'),
    'payfort_api_url' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_API_URL_LIVE') : env('PAYFORT_API_URL_SANDBOX'),
    'payfort_payment_page_url' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_PAYMENT_PAGE_URL_LIVE') : env('PAYFORT_PAYMENT_PAGE_URL_SANDBOX'),
    //
    'payfort_access_code' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_ACCESS_CODE_LIVE') : env('PAYFORT_ACCESS_CODE_SANDBOX'),
    'payfort_merchant_identifier' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_MERCHANT_IDENTIFIER_LIVE') : env('PAYFORT_MERCHANT_IDENTIFIER_SANDBOX'),
    'payfort_sha_request_phrase' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_SHA_REQUEST_PHRASE_LIVE') : env('PAYFORT_SHA_REQUEST_PHRASE_SANDBOX'),
    'payfort_sha_response_phrase' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_SHA_RESPONSE_PHRASE_LIVE') : env('PAYFORT_SHA_RESPONSE_PHRASE_SANDBOX'),
    // debug
    'debug_customer_id' => env('DEBUG_CUSTOMER_ID', 0),
    'debug_customer_address_id' => env('DEBUG_CUSTOMER_ADDRESS_ID', 0),
    //
    'google_maps_api_key' => env('GOOGLE_MAPS_API_KEY', "Unknown"),
    'booking_ref_prefix' => env('BOOKING_REF_PREFIX'),
    'online_payment_ref_prefix' => env('ONLINE_PAYMENT_REF_PREFIX'),
    //
    'file_server_url' => env('FILE_SERVER_URL'),
    'web_booking_url' => env('WEB_BOOKING_URL'),
    /*
    * 
    * Payfort Apple Pay Values
    * please add sandbox values only
    * add live values on .env file only
    *
    */
    'PAYFORT_APPLE_PAY_ENVIRONMENT'         => env('PAYFORT_APPLE_PAY_ENVIRONMENT'),
    'PAYFORT_APPLE_PAY_API_URL'             => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_API_URL')            : "https://sbpaymentservices.payfort.com/FortAPI/paymentApi",
    'PAYFORT_APPLE_PAY_PAYMENT_PAGE_URL'    => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_PAYMENT_PAGE_URL')   : "https://sbcheckout.payfort.com/FortAPI/paymentPage",
    'PAYFORT_APPLE_PAY_ACCESS_CODE'         => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_ACCESS_CODE')        : "snMGuc6LeZ7SYFHPeAbg",
    'PAYFORT_APPLE_PAY_MERCHANT_IDENTIFIER' => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_MERCHANT_IDENTIFIER'): "6a190585",
    'PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE'  => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE') : "76h6KQnqUjZyNOPX11OhTl#{",
    'PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE' => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE'): "38yNlOZsyBhnoynDiBDUqc@_",
    'APPLE_MERCHANT_ID'                     => env('PAYFORT_APPLE_PAY_ENVIRONMENT') == "LIVE" ? env('APPLE_MERCHANT_ID')                    : "merchant.com.Azinova.Spectrum.userApp",
]
    ?>