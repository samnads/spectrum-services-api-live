@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.contact-mail-section')
    @include('emails.includes.section-footer-banner')
    @include('emails.includes.section-footer-links')
@endsection