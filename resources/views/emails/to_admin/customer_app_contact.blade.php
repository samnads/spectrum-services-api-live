<html>
                <head>
                </head>
                <body>
                    <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:100%!important">
                        <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important">
                                <tbody>
                                  <tr>
                                    <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                        <tbody>
                                                          <tr>
                                                            <td valign="top" style="padding:0px"><table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                <tbody>
                                                                  <tr>
                                                                    <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="banner" src="{{url('assets/images/email_banner.jpg')}}" width="600" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
                                                                      <div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
                                                                        <div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                                                                          <div class="aSK J-J5-Ji aYr"></div>
                                                                        </div>
                                                                      </div></td>
                                                                  </tr>
                                                                </tbody>
                                                              </table></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                      <table width="600" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
                               <tbody>
                               <tr>

                               <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                               <br>
                                Dear Admin,<br>
                                <br>New Enquiry Received
                                <br><br>
                                Name : <strong><?= $customer->customer_name; ?></strong>
                                <br>
                                Phone : <i><?= $customer->mobile_number_1; ?></i>
                                <br>
                                <br>
                                <b><?= $body ?></b>
                                <br>
                                <br>
                                    Thanks & Regards<br/>
                                    <?= Config::get('values.company_name'); ?><br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                      <tbody>
                        <tr>
                          <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse">
                              <tbody>
                                <tr>
                                  <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2017 Spectrum Services.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                      <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Spectrum Services</span></div>
                                    </td>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                      </tbody>
                    </table>
                                                      </td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>

                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                        </center>
                    </div>
                </body>
                </html>