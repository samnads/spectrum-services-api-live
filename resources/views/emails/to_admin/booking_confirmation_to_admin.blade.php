<html>
    <head></head>
    <body>
        <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:800px!important">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;">
                    <tr>
                        <td colspan="2"><img align="left" alt="" src="{{url('assets/images/email_banner.jpg')}}" width="794" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; text-align: center;">
                            <br />
                            <strong>Dear Admin,</strong><br /><br />
                            New booking received with Ref. No. {{ $booking->reference_id }}<br />
                            <span style=" font-size: 18px; color:#FFF; background: #78c056; padding: 7px 30px; border-radius: 25px; margin: 20px 0px 40px 0px; display: inline-block;">Reference ID - <?php echo $booking->reference_id;?></span> 
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td valign="top" style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Customer</b><br></span>
                            Name - <?php echo $customer->customer_name; ?><br>
                            Mobile - <?php echo $customer->mobile_number_1; ?><br>
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td valign="top" style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Address</b><br></span>
                            Address - <i>Building : <?php echo $customer_address->building; ?></i><br>
                            <i>Unit : <?php echo $customer_address->unit_no; ?></i><br>
                            <i>Street : <?php echo $customer_address->street; ?></i><br>
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Booking Summary</b><br></span>
                            
                            No of Maids - <i><?php echo $input['professionals_count']; ?></i><br />
                            Total Hours - <i><?php echo $input['hours']; ?></i><br />
							Instructions - <i><?php echo $input['instructions']; ?></i><br /><br />
                            <div style="width: 100%; padding: 0px 0px 0px 0px;">
                                <div style="width: 30%; float: left; padding: 0px 0px 0px 0px;"><strong>Service Date & Time</strong></div><br>
                               
                                    <?php
                                    $clean_fee = 0;
                                    $service_charge = 0;
                                    $vat_charge = 0;
                                    $tot_service = 0;
                                    ?>
                                    <p style="width: 100%; padding: 0px 0px 0px 0px;"><?php echo $booking->service_start_date; ?> (<?= $frequency->name; ?>) (<?php echo date("l",strtotime($booking->service_start_date)); ?>)</p>
                                        <p style="width: 100%; font-size:12px; padding: 0px 0px 0px 0px;"><?php echo date('h:i a', strtotime($booking->time_from)) . ' - ' . date('h:i a', strtotime($booking->time_to)) ?></p>
                                    </div>	
                                <div style="clear:both"></div>
                        </td>
                    </tr>
                    <?php if($booking->cleaning_material=="Y"): ?>
                    <tr>
                        <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #78c056; text-align: center; padding: 20px 0px 0px 0px;"><strong>Customer has requested for cleaning materials.</strong></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td style="padding: 20px 0px 20px 20px;">
                            <b>Price Summary</b><br>
							Payment Method - <b><?php echo $payment_type->name ?></b>&nbsp;<br /><br />
                            <?php
                            if(@$coupon_fee > 0)
                            {
                            ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Service Cost</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format($service_charge, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Discount(Coupon used)</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format($coupon_fee, 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <?php    
                            }
                            ?>
                            <?php
                            if(@$coupon_fee > 0)
                            {
                                $fe = ($service_charge - $coupon_fee);
                            ?>
							<div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Total Cost</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format(@$fe, 2); ?></strong></div> 
                                <div style="clear:both"></div>
                            </div>
                            <?php
                            } else {
                            ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Total Cost</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format(@$amount['total'], 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <?php } ?>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Vat Cost</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format(@$amount['tax_amount'], 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Transaction Charge</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format(@$amount['payment_type_charge'], 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="width: 100%; padding: 0px 0px 5px 0px;">
                                <div style="width: 50%; float: left; padding: 0px 0px 0px 0px;"><strong>Net Payable</strong></div>
                                <div style="width: 50%; float: left; font-size: 20px; color: #355eac; padding: 0px 0px 0px 0px;">AED <strong><?php echo number_format(@$amount['total_payable'], 2); ?></strong></div>
                                <div style="clear:both"></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 18px;">
                        <td style="padding: 20px 0px 20px 20px;">
                            <span style="line-height:30px;"><b>Spectrum Services</b><br></span>
                            Al Barsha Heights, Tecom,<br>
                            Al Thuraya Telecom Tower, Office 906,<br>
                            Dubai, U.A.E.<br>
                            For Bookings : 8007274 / 04- 4310113 <br>
                            Email : booking@spectrumservices.ae<br>                                        
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #555; line-height: 18px; text-align:center; ">
                        <td colspan="2" style="padding: 20px 0px 20px 0px">&copy; 2018 Spectrum All Rights Reserved.</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </center>
        </div>
    </body>
</html>