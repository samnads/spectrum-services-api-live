<div class="content-section1" style="width: 700px; height: auto; margin: 30px auto 0;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p
                    style="font: bold 20px/25px 'Poppins', sans-serif;  color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    House Cleaning</p>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    27 Sept 2023, 05:00 pm to 07:00 pm</p>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50px">
                            <div class="maid-photo"
                                style="width: 40px; height: 40px; border-radius: 50px; overflow: hidden">
                                <a href="javascript:void(0);" style="display: block; cursor: default !important;"><img
                                        src="http://demo.azinova.me/website/spectrum-booking/images/maid.jpg"
                                        width="100%" height="" alt=""
                                        style="border-radius: 5px; cursor: default !important;" />
                                </a>
                            </div>
                        </td>

                        <td width="50px">
                            <div class="maid-photo"
                                style="width: 40px; height: 40px; border-radius: 50px; overflow: hidden">
                                <a href="javascript:void(0);" style="display: block; cursor: default !important;"><img
                                        src="http://demo.azinova.me/website/spectrum-booking/images/maid.jpg"
                                        width="100%" height="" alt=""
                                        style="border-radius: 5px; cursor: default !important;" />
                                </a>
                            </div>
                        </td>

                        <td width="50px">
                            <div class="maid-photo"
                                style="width: 40px; height: 40px; border-radius: 50px; overflow: hidden">
                                <a href="javascript:void(0);" style="display: block; cursor: default !important;"><img
                                        src="http://demo.azinova.me/website/spectrum-booking/images/maid.jpg"
                                        width="100%" height="" alt=""
                                        style="border-radius: 5px; cursor: default !important;" />
                                </a>
                            </div>
                        </td>

                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Booking Referance</p>
                <p
                    style="font: normal 20px/25px 'Poppins', sans-serif; color: #f76161; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    SS-5689</p>
            </td>
        </tr>
        </tr>

    </table>
</div>
