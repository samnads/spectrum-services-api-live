<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="tr" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">
@include('emails.design.head')
<body style="margin: 0px; padding: 0px;">
    <div class="wrapper-main" style="width: 800px; height: auto; margin: 30px auto 10px;">
        @include('emails.design.section-top')
        <div class="container" style="width: 700px; height: auto; margin: 30px auto 0; overflow: hidden !important;">
            @include('emails.design.section-message')
            @include('emails.design.section-summary')
            @include('emails.design.section-details')
            @include('emails.design.section-addons')
            @include('emails.design.section-price')
            @include('emails.design.section-footer-banner')
            @include('emails.design.section-app-links')
            @include('emails.design.section-footer-links')
        </div>
        @include('emails.design.section-bottom')
    </div>
</body>
</html>
<?php
die();
?>
