<div class="content-section2" style="width: 700px; height: auto; margin: 30px auto 0;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Frequency<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{$booking->frequency}}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    No. of Professionals<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{sizeof($bookings)}}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd;">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Cleaning Material<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{$booking->cleaning_material_opted}}</label>
                </p>
            </td>
        </tr>
        @if(trim($booking->booking_note) != '')
        <tr>
            <td style="border: 1px solid #ddd;" colspan="3">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Instructions<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;color: #c40000;">{{$booking->booking_note}}</label>
                </p>
            </td>
        </tr>
        @endif
    </table>
</div>