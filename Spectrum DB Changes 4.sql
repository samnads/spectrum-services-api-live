ALTER TABLE `bookings`
ADD `_service_amount` decimal(10,2) unsigned NULL,
ADD `_cleaning_materials_amount` decimal(10,2) unsigned NULL AFTER `_service_amount`,
ADD `_service_addons_amount` decimal(10,2) unsigned NULL AFTER `_cleaning_materials_amount`,
ADD `_packages_amount` decimal(10,2) unsigned NULL AFTER `_service_addons_amount`,
ADD `_amount_before_discount` decimal(10,2) unsigned NULL AFTER `_packages_amount`,
ADD `_coupon_discount` decimal(10,2) unsigned NULL AFTER `_amount_before_discount`,
ADD `_discount_total` decimal(10,2) unsigned NULL AFTER `_coupon_discount`,
ADD `_taxable_amount` decimal(10,2) unsigned NULL AFTER `_discount_total`,
ADD `_vat_percentage` decimal(10,2) unsigned NULL AFTER `_taxable_amount`,
ADD `_vat_amount` decimal(10,2) unsigned NULL AFTER `_vat_percentage`,
ADD `_taxed_amount` decimal(10,2) unsigned NULL AFTER `_vat_amount`,
ADD `_payment_type_charge` decimal(10,2) unsigned NULL AFTER `_taxed_amount`,
ADD `_total_payable` decimal(10,2) unsigned NULL AFTER `_payment_type_charge`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_addons`
ADD `service_addon_name` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `service_addons_id`,
ADD `strike_amount` decimal(15,2) unsigned NOT NULL AFTER `service_minutes`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `payment_types`
ADD `show_in_web` tinyint NOT NULL DEFAULT '0' AFTER `charge_type`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_addons` CHANGE `strike_amount` `strike_amount` DECIMAL(15,2) UNSIGNED NULL;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `customer_app_service_type_name` varchar(110) COLLATE 'utf8mb4_general_ci' NULL AFTER `service_type_name`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `settings`
ADD `customer_app_img_version` varchar(15) NULL;
ALTER TABLE `settings` CHANGE `normal_hours_fee` `normal_hours_fee` DOUBLE NULL;
ALTER TABLE `settings` CHANGE `extra_hours_fee` `extra_hours_fee` DOUBLE NULL;
ALTER TABLE `settings` CHANGE `weekend_hours_fee` `weekend_hours_fee` DOUBLE NULL;
INSERT INTO `settings` (`normal_hours_fee`, `extra_hours_fee`, `weekend_hours_fee`, `customer_app_img_version`)
VALUES (NULL, NULL, NULL, '1.0');
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `online_payments`
CHANGE `post_data` `post_data` text COLLATE 'latin1_swedish_ci' NULL AFTER `user_agent`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `online_payments`
CHANGE `transaction_id` `transaction_id` varchar(40) COLLATE 'latin1_swedish_ci' NULL DEFAULT '0' AFTER `payment_id`;
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------