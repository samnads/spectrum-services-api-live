<?php

use App\Http\Controllers\CalculateController;
use App\Http\Controllers\CustomerApiAddressController;
use App\Http\Controllers\CustomerApiAreaController;
use App\Http\Controllers\CustomerApiBookingCancelController;
use App\Http\Controllers\CustomerApiBookingCancelReasonsController;
use App\Http\Controllers\CustomerApiBookingController;
use App\Http\Controllers\CustomerApiBookingCreateController;
use App\Http\Controllers\CustomerApiBookingCreateWebController;
use App\Http\Controllers\CustomerApiBookingHistoryController;
use App\Http\Controllers\CustomerApiBookingRescheduleController;
use App\Http\Controllers\CustomerApiCardController;
use App\Http\Controllers\CustomerApiController;
use App\Http\Controllers\CustomerApiCouponController;
use App\Http\Controllers\CustomerApiDateTimeController;
use App\Http\Controllers\PayfortPaymentController;
use App\Http\Controllers\WebHookController;
use App\Http\Controllers\CustomerApiDataController;
use App\Http\Controllers\CustomerApiFaqController;
use App\Http\Controllers\CustomerApiMaidController;
use App\Http\Controllers\CustomerApiNotificationsController;
use App\Http\Controllers\CustomerApiOtpController;
use App\Http\Controllers\CustomerApiPackagesController;
use App\Http\Controllers\CustomerApiPaymentController;
use App\Http\Controllers\CustomerApiRatingController;
use App\Http\Controllers\CustomerApiReferralController;
use App\Http\Controllers\CustomerApiServiceAddonsController;
use App\Http\Controllers\CustomerApiServiceTypeController;
use App\Http\Controllers\CustomerApiSpecialOffersController;
use App\Http\Controllers\WebApiServiceTypeController;
use App\Http\Controllers\AdminApiMailController;
use App\Http\Controllers\CustomerApiTestController;
use App\Http\Controllers\CustomerApiWalletController;
use App\Http\Controllers\PaymentFrameController;
use App\Http\Controllers\ApplePayController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::get('web/service_type/{service_type_id}', [WebApiServiceTypeController::class, 'service_type_data_by_id']);
Route::prefix('customer')->group(function () {
    // for customer app only
    Route::group(['middleware' => 'validJsonInput'], function () {
        Route::get('frequencies', [CustomerApiController::class, 'frequencies']); // get | DONE
        Route::get('available_datetime', [CustomerApiDateTimeController::class, 'available_datetime']); // GET | DONE
        Route::get('available_time', [CustomerApiDateTimeController::class, 'available_time']); // GET | DONE
        Route::get('crew_list', [CustomerApiMaidController::class, 'crew_list']); // GET | DONE
        Route::get('faq', [CustomerApiFaqController::class, 'faq']); // GET | DONE
        Route::any('customer_login', [CustomerApiOtpController::class, 'customer_login']); // post | DONE
        Route::any('check_otp', [CustomerApiOtpController::class, 'check_otp']); // post | DONE
        Route::any('resend_otp', [CustomerApiOtpController::class, 'resend_otp']); // post | DONE
        Route::get('service_types', [CustomerApiServiceTypeController::class, 'service_types']); // GET | DONE
        Route::get('cancel_reasons', [CustomerApiBookingCancelReasonsController::class, 'cancel_reasons']); // GET | DONE
        Route::get('coupon_list', [CustomerApiCouponController::class, 'coupon_list']); // GET | DONE
        Route::get('hourly_rate', [CustomerApiBookingController::class, 'hourly_rate']); // GET | DONE
        Route::get('special_offers', [CustomerApiSpecialOffersController::class, 'special_offers']); // GET | DONE
        Route::get('packages', [CustomerApiPackagesController::class, 'packages']); // GET | DONE
        Route::any('coupon_apply', [CustomerApiCouponController::class, 'coupon_apply']); // post
        Route::get('service_addons', [CustomerApiServiceAddonsController::class, 'service_addons']); // GET | DONE
        Route::get('referral_credits', [CustomerApiReferralController::class, 'referral_credits']);
        Route::any('calculate', [CalculateController::class, 'calculate']); // POST
        //Route::any('apple-process-pay', [PaymentFrameController::class, 'checkout_apple_pay_process']); // POST | DONE
        //Route::any('apple-process-pay-test', [PaymentFrameController::class, 'apple_pay_process_test']); // POST | DONE
        //Route::any('payfort_apple_test', [ApplePayController::class, 'payfort_apple_test']); // POST | DONE
        // apple pay process for ios device
        Route::post('payfort-process-apple-pay', [ApplePayController::class, 'payfort_process_apple_pay']);
        //Route::get('payfort-process-apple-pay-test', [ApplePayController::class, 'payfort_process_apple_pay_test']);
        // link payment
        Route::post('new-online-payment', [CustomerApiPaymentController::class, 'new_online_payment']);
        Route::get('online-payment/{payment_id}', [CustomerApiPaymentController::class, 'get_online_payment']);
        Route::post('process-payfort-online-payment', [CustomerApiPaymentController::class, 'process_payfort_online_payment']);
    });
    Route::get('data', [CustomerApiDataController::class, 'data']);
    Route::any('payment/iframe', [PaymentFrameController::class, 'iframe']);
    Route::any('payment/payfort/tokenization', [PaymentFrameController::class, 'payfort_tokenization']);
    Route::any('payment/payfort/tokenization_form', [PaymentFrameController::class, 'tokenization_form']);
    Route::any('payment/payfort/processing', [PaymentFrameController::class, 'processing']);
    Route::any('payment/payfort/redirect', [PaymentFrameController::class, 'redirect']);
    Route::any('payment/payfort/verify', [PaymentFrameController::class, 'verify']);
    Route::any('payment/payfort/success', [PaymentFrameController::class, 'success']);
    Route::any('payment/payfort/failed', [PaymentFrameController::class, 'failed']);
    //Route::any('payment/payfort/transaction_feedback', [PaymentFrameController::class, 'payfort_transaction_feedback']);
    Route::post('booking/payment/payfort/process/{booking_id}', [PayfortPaymentController::class, 'booking_payment_payfort_process']);
    Route::group(['middleware' => ['validJsonInput', 'customerTokenCheck']], function () {
        Route::any('validate_token', [CustomerApiController::class, 'validate_token']); // POST
        Route::get('address_list', [CustomerApiAddressController::class, 'address_list']); // GET / WAIT
        Route::any('set_default_address', [CustomerApiAddressController::class, 'set_default_address']); // GET / WAIT
        Route::get('area_list', [CustomerApiAreaController::class, 'area_list']); // GET | DONE
        Route::any('name_entry', [CustomerApiController::class, 'name_entry']); // post | DONE
        Route::any('add_card', [CustomerApiCardController::class, 'add_card']); // post | DONE
        Route::any('card_details', [CustomerApiCardController::class, 'card_details']); // GET | DONE
        Route::any('link_card', [CustomerApiCardController::class, 'link_card']); // POST | DONE
        Route::any('delete_card', [CustomerApiCardController::class, 'delete_card']); // post | DONE
        Route::any('create_booking', [CustomerApiBookingCreateController::class, 'create_booking']); // post
        Route::any('create_booking_new', [CustomerApiBookingCreateWebController::class, 'create_booking_new']); // post
        Route::any('reschedule', [CustomerApiBookingRescheduleController::class, 'reschedule']); // post
        Route::any('add_address', [CustomerApiAddressController::class, 'add_address']); // POST / DONE
        Route::any('edit_address', [CustomerApiAddressController::class, 'edit_address']); // POST / DONE
        Route::any('delete_address', [CustomerApiAddressController::class, 'delete_address']); // POST / DONE
        Route::any('update_customer_data', [CustomerApiController::class, 'update_customer_data']); // POST
        Route::any('booking_history', [CustomerApiBookingHistoryController::class, 'booking_history']); // TEST
        Route::any('booking_cancel', [CustomerApiBookingCancelController::class, 'booking_cancel']); // POST
        Route::any('booking_all_cancel', [CustomerApiBookingCancelController::class, 'booking_all_cancel']); // POST
        Route::any('customer_logout', [CustomerApiController::class, 'customer_logout']); // post | DONE
        Route::any('rate_booking', [CustomerApiRatingController::class, 'rate_booking']); // post
        Route::get('notifications', [CustomerApiNotificationsController::class, 'notifications']); // get | DONE
        Route::any('all_notification_read', [CustomerApiNotificationsController::class, 'all_notification_read']); // post | DONE
        Route::any('clear_notiication', [CustomerApiNotificationsController::class, 'clear_notiication']); // post | DONE
        Route::get('customer_soa', [CustomerApiController::class, 'customer_soa']); // GET | DONE
        Route::post('contact_send', [CustomerApiController::class, 'contact_send']); // POST
        Route::any('change_payment_mode', [CustomerApiPaymentController::class, 'change_payment_mode']); // POST | DONE
        Route::get('wallet', [CustomerApiWalletController::class, 'wallet']);
        Route::any('deleteAccount', [CustomerApiController::class, 'deleteAccount']); // POST
        Route::get('customer_data', [CustomerApiController::class, 'customer_data']); // GET | DONE
        Route::any('booking_details', [CustomerApiBookingHistoryController::class, 'booking_details']); // TEST
        Route::post('booking_details_by_ref', [CustomerApiBookingHistoryController::class, 'booking_details_by_ref']); // TEST
        Route::post('retry_payment_data', [CustomerApiPaymentController::class, 'retry_payment_data']); // TEST
    });
    Route::get('/', function (Request $request) {
        return 'You\'re in customer app API base url 😀 !';
    });
});
Route::prefix('admin')->group(function () {
    Route::prefix('mail')->group(function () {
        // mail routes, helpul to resend mails from anywhere outside api
        Route::get('booking-confirmation-to-admin/{booking_id}', [AdminApiMailController::class, 'booking_confirmation_to_admin']);
        Route::get('booking-confirmation-to-customer/{booking_id}', [AdminApiMailController::class, 'booking_confirmation_to_customer']);
        Route::get('registration-success-to-customer/{customer_id}', [AdminApiMailController::class, 'registration_success_to_customer']);
        Route::get('online-payment-confirmation-to-customer/{payment_id}', [AdminApiMailController::class, 'online_payment_confirmation_to_customer']);
        Route::get('online-payment-confirmation-to-admin/{payment_id}', [AdminApiMailController::class, 'online_payment_confirmation_to_admin']);
    });
});
Route::get('test', [AdminApiMailController::class, 'test']);
/***********************************************************************************
 * 
 * 
 * WebHook urls for payment gateways
 * 
 * 
 */
// WebHook api urls for payfort gateway (called from web, no direct call)
Route::post('webhook/payfort/transaction-feedback', [WebHookController::class, 'payfort_transaction_feedback']);
/**********************************************************************************/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::fallback(function () {
    return 'Oops, requested api endpoint not found 🥶 !';
});
