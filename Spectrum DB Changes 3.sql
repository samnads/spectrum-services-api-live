CREATE TABLE `service_addons` (
  `service_addons_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `service_addon_name` varchar(255) NOT NULL,
  `service_addon_description` varchar(255) DEFAULT NULL,
  `service_minutes` decimal(4,2) NOT NULL DEFAULT 0.00,
  `strike_amount` decimal(15,2) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `cart_limit` int(5) NOT NULL DEFAULT 1 COMMENT 'maximum count allowed to add on cart',
  `customer_app_thumbnail` varchar(255) DEFAULT NULL,
  `populariry` bigint(20) NOT NULL DEFAULT 0 COMMENT 'higher the popularity means top selling addon',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_addons_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `service_addons` (`service_addons_id`, `service_type_id`, `service_addon_name`, `service_addon_description`, `service_minutes`, `strike_amount`, `amount`, `cart_limit`, `customer_app_thumbnail`, `populariry`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Balcony Cleaning',	'test 1',	15.00,	25.00,	19.00,	1,	NULL,	5,	'2023-08-22 15:41:20',	'2023-08-22 15:41:20',	NULL),
(2,	1,	'Cupboard Cleaning',	'test  1',	20.00,	30.00,	25.00,	1,	NULL,	10,	'2023-08-22 15:41:20',	'2023-08-22 15:41:20',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_addons_points` (
  `service_addons_included_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `point_type` enum('WHAT_INCLUDED','WHAT_EXCLUDED') NOT NULL,
  `sort_order_id` int(5) unsigned DEFAULT NULL,
  `point_html` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_included_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `service_addons_points_ibfk_1` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `service_addons_points` (`service_addons_included_id`, `service_addons_id`, `point_type`, `sort_order_id`, `point_html`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'WHAT_INCLUDED',	1,	'Point 1',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(2,	1,	'WHAT_INCLUDED',	2,	'Point 2',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(3,	1,	'WHAT_INCLUDED',	3,	'Point 3',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(4,	2,	'WHAT_INCLUDED',	1,	'Point 11',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(5,	2,	'WHAT_INCLUDED',	2,	'Point 22',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(6,	2,	'WHAT_INCLUDED',	3,	'Point 33',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL),
(7,	2,	'WHAT_EXCLUDED',	3,	'Point hfghfg',	'2023-08-22 16:04:32',	'2023-08-22 16:04:32',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_addons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `service_minutes` decimal(4,2) unsigned NOT NULL DEFAULT 0.00,
  `unit_price` decimal(15,2) NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `booking_addons_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_addons_ibfk_2` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `rush_slot_charges` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `extra_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `rush_slot_extra_charge` decimal(15,2) NOT NULL DEFAULT '0';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_addons`
ADD UNIQUE `booking_id_service_addons_id` (`booking_id`, `service_addons_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_addons`
ADD `amount` decimal(15,2) unsigned NOT NULL;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `addons_included` tinyint(1) NOT NULL DEFAULT '0';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `addons_amount` decimal(15,2) NOT NULL DEFAULT '0';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_addons`
CHANGE `booking_id` `booking_id` bigint(20) unsigned NULL AFTER `id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
ADD `info_html` text COLLATE 'utf8mb4_general_ci' NULL AFTER `description`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `rush_slot_id` bigint(11) unsigned NULL AFTER `is_package`,
ADD FOREIGN KEY (`rush_slot_id`) REFERENCES `rush_slot_charges` (`id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
CHANGE `rush_slot_extra_charge` `rush_slot_charge` decimal(15,2) NOT NULL DEFAULT '0.00' AFTER `rush_slot_id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
ADD `service_time` time NOT NULL DEFAULT '00:30:00' AFTER `info_html`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `rush_slot_charges` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `time` time NOT NULL COMMENT 'time slot',
  `extra_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `service_type_models` (`id`, `model`) VALUES
(1,	'normal'),
(2,	'package');
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `service_type_model_id` int(10) unsigned NULL DEFAULT '1',
ADD FOREIGN KEY (`service_type_model_id`) REFERENCES `service_type_models` (`id`);
ALTER TABLE `service_types`
ADD `web_url_slug` varchar(255) COLLATE 'latin1_swedish_ci' NULL AFTER `service_type_name`;
ALTER TABLE `service_types`
ADD UNIQUE `web_url_slug` (`web_url_slug`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_addresses`
CHANGE `latitude` `latitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `customer_address`,
CHANGE `longitude` `longitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `latitude`;
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------