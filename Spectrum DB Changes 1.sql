ALTER TABLE `users` ENGINE='InnoDB';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers` ENGINE='InnoDB';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `maids` ENGINE='InnoDB';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `day_services` ENGINE='InnoDB';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types` ENGINE='InnoDB';
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `frequencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `code` varchar(10) NOT NULL,
  `hourly_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `offer_percentage` decimal(15,2) NOT NULL DEFAULT 0.00,
  `max_working_hour` decimal(4,2) NOT NULL DEFAULT 0.00,
  `cleaning_material_hourly_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `frequencies` (`id`, `name`, `code`, `hourly_rate`, `offer_percentage`, `max_working_hour`, `cleaning_material_hourly_rate`, `description`, `deleted_at`) VALUES
(1,	'One Time',	'OD',	35.00,	5.00,	20.00,	10.00,	NULL,	NULL),
(2,	'Weekly',	'WE',	33.00,	0.00,	20.00,	10.00,	NULL,	NULL),
(3,	'Every 2 Week',	'BW',	30.00,	7.00,	20.00,	10.00,	NULL,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `charge` decimal(15,2) NOT NULL DEFAULT 0.00,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `payment_types` (`id`, `name`, `code`, `description`, `charge`, `deleted_at`) VALUES
(1,	'Cash',	'cash',	NULL,	5.25,	NULL),
(2,	'Card',	'card',	NULL,	0.00,	NULL),
(3,	'Apple Pay',	'applepay',	NULL,	0.50,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `nick_name` varchar(155) NOT NULL,
  `card_number` varchar(32) NOT NULL,
  `expiry_date` date NOT NULL,
  `cvv` varchar(8) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_cards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers`
ADD `login_otp` varchar(20) COLLATE 'utf8_general_ci' NULL AFTER `mobile_verification_code`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers`
ADD `login_otp_expired_at` datetime NULL AFTER `mobile_verification_code`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_addresses`
ADD `created_at` datetime NULL,
ADD `updated_at` datetime NULL AFTER `created_at`,
ADD `deleted_at` datetime NULL AFTER `updated_at`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `day_services`
CHANGE `rate_added_date` `rate_added_date` date NULL AFTER `comments`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `day_services`
CHANGE `rating` `rating` int(11) NULL AFTER `odoo_sync_status`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `day_services`
CHANGE `comments` `comments` text NULL AFTER `rating`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `maid_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maid_id` int(10) unsigned NOT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `rated_by_customer_id` bigint(20) unsigned DEFAULT NULL,
  `day_service_id` bigint(20) unsigned DEFAULT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maid_id` (`maid_id`),
  KEY `day_service_id` (`day_service_id`),
  KEY `rated_by_customer_id` (`rated_by_customer_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `maid_rating_ibfk_1` FOREIGN KEY (`maid_id`) REFERENCES `maids` (`maid_id`),
  CONSTRAINT `maid_rating_ibfk_2` FOREIGN KEY (`day_service_id`) REFERENCES `day_services` (`day_service_id`),
  CONSTRAINT `maid_rating_ibfk_3` FOREIGN KEY (`rated_by_customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `maid_rating_ibfk_4` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `customer_card_id` int(11) NULL,
ADD FOREIGN KEY (`customer_card_id`) REFERENCES `customer_cards` (`id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(3) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `customer_faq` (`id`, `order_priority`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	2,	'What services do you offer?',	'We offer a range of cleaning services including residential cleaning, commercial cleaning, deep cleaning, move-in/out cleaning, and post-construction cleaning.',	'2023-06-08 10:18:53',	NULL,	NULL),
(2,	1,	'Do you provide cleaning supplies and equipment?',	'Yes, we provide all the necessary cleaning supplies and equipment needed to clean your home or office.',	'2023-06-08 10:19:13',	NULL,	NULL),
(3,	4,	'What areas do you service?',	'We currently provide our cleaning services to [insert area or location]. However, we may be able to service other areas upon request.',	'2023-06-08 10:19:40',	NULL,	NULL),
(4,	5,	'How do you determine the cost of cleaning services?',	'We determine the cost of our cleaning services based on several factors including the size of the space, the type of cleaning required, and the frequency of service. We provide a personalized quote for each customer after a thorough consultation.',	'2023-06-08 10:20:00',	NULL,	NULL),
(5,	3,	'Do you offer eco-friendly cleaning options?',	'Yes, we offer eco-friendly cleaning options upon request. Our eco-friendly cleaning products are safe for use around pets and children and are just as effective as traditional cleaning products.',	'2023-06-08 10:20:16',	NULL,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `booking_delete_remarks`
ADD INDEX `service_date` (`service_date`);
ALTER TABLE `booking_delete_remarks`
ADD INDEX `booking_id` (`booking_id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_cancel_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(5) NOT NULL,
  `reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `booking_cancel_reasons` (`id`, `order_priority`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Personal schedule conflicts',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(2,	2,	'Illness or family emergency',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(3,	3,	'Dissatisfied with previous cleaning',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(4,	4,	'Financial constraints',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(5,	5,	'Moving or relocation',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(6,	6,	'Change in cleaning needs',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(7,	7,	'Unsatisfactory communication with cleaning service',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `customer_app_status` tinyint(1) NULL COMMENT 'set 1 to show the service in customer app' AFTER `service_type_status`;
ALTER TABLE `service_types`
ADD `customer_app_icon_file` varchar(255) COLLATE 'utf8_general_ci' NOT NULL DEFAULT 'customer-app-service-icon-default.png' AFTER `customer_app_status`,
ADD `customer_app_thumbnail_file` varchar(255) COLLATE 'utf8_general_ci' NOT NULL DEFAULT 'customer-app-service-thumbnail-default.png' AFTER `customer_app_icon_file`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers`
CHANGE `customer_username` `customer_username` varchar(75) COLLATE 'utf8_general_ci' NULL AFTER `odoo_customer_id`;
ALTER TABLE `customers`
CHANGE `customer_password` `customer_password` varchar(15) COLLATE 'utf8_general_ci' NULL AFTER `customer_username`;
ALTER TABLE `customers`
CHANGE `customer_name` `customer_name` varchar(110) COLLATE 'utf8_general_ci' NULL AFTER `customer_type`;
ALTER TABLE `customers`
CHANGE `customer_nick_name` `customer_nick_name` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `customer_name`;
ALTER TABLE `customers`
CHANGE `website_url` `website_url` varchar(160) COLLATE 'utf8_general_ci' NULL AFTER `fb_id`;
ALTER TABLE `customers`
CHANGE `signed` `signed` varchar(5) COLLATE 'utf8_general_ci' NULL AFTER `balance`;
ALTER TABLE `customers`
CHANGE `customer_notes` `customer_notes` text COLLATE 'utf8_general_ci' NULL AFTER `signed`;
ALTER TABLE `customers`
CHANGE `customer_source_id` `customer_source_id` int(11) NULL AFTER `customer_source_val`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `content` text NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `cleared_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `customer_notifications_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `customer_notifications_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `payment_type_id` int(11) NULL AFTER `from_justmop`,
ADD FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `booking_common_id` bigint(20) unsigned NULL COMMENT 'field created for customer app (multi maid slection for single booking)' AFTER `booking_id`;
ALTER TABLE `bookings`
ADD FOREIGN KEY (`booking_common_id`) REFERENCES `bookings` (`booking_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `online_payments`
CHANGE `user_agent` `user_agent` varchar(500) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `ip_address`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `special_offers` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `frequency_id` int(11) NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `customer_app_banner_file` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `no_of_maids` int(11) NOT NULL DEFAULT 1,
  `no_of_hours` int(11) NOT NULL DEFAULT 2,
  `no_of_visits` int(11) NOT NULL DEFAULT 1,
  `cleaning_material_included` enum('N','Y') NOT NULL DEFAULT 'N',
  `actual_cleaning_material_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `offer_cleaning_material_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `actual_hourly_rate` decimal(15,2) NOT NULL,
  `offer_hourly_rate` decimal(15,2) NOT NULL,
  `dates` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '["2023-06-25","2023-06-29"]	',
  `days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '[0,2,5]	',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `frequency_id` (`frequency_id`),
  KEY `created_by` (`created_by`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `special_offers_ibfk_1` FOREIGN KEY (`frequency_id`) REFERENCES `frequencies` (`id`),
  CONSTRAINT `special_offers_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`),
  CONSTRAINT `special_offers_ibfk_3` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DELIMITER ;;

CREATE TRIGGER `special_offers_bi` BEFORE INSERT ON `special_offers` FOR EACH ROW
begin
    declare msg varchar(128);
    if new.end_date < new.start_date then
        set msg = concat('end_date must be >= start_date');
        signal sqlstate '45000' set message_text = msg;
    end if;
end;;

CREATE TRIGGER `special_offers_dtni` BEFORE INSERT ON `special_offers` FOR EACH ROW
begin
    declare msg varchar(128);
    if new.dates IS NULL AND new.days IS NULL then
        set msg = concat('days and date cannot be null same time');
        signal sqlstate '45000' set message_text = msg;
    end if;
end;;

CREATE TRIGGER `special_offers_bu` BEFORE UPDATE ON `special_offers` FOR EACH ROW
begin
    declare msg varchar(128);
    if new.end_date < new.start_date then
        set msg = concat('end_date must be >= start_date');
        signal sqlstate '45000' set message_text = msg;
    end if;
end;;

CREATE TRIGGER `special_offers_dtnu` BEFORE UPDATE ON `special_offers` FOR EACH ROW
begin
    declare msg varchar(128);
    if new.dates IS NULL AND new.days IS NULL then
        set msg = concat('days and date cannot be null same time');
        signal sqlstate '45000' set message_text = msg;
    end if;
end;;

DELIMITER ;

INSERT INTO `special_offers` (`id`, `name`, `description`, `frequency_id`, `service_type_id`, `customer_app_banner_file`, `start_date`, `end_date`, `no_of_maids`, `no_of_hours`, `no_of_visits`, `cleaning_material_included`, `actual_cleaning_material_rate`, `offer_cleaning_material_rate`, `actual_hourly_rate`, `offer_hourly_rate`, `dates`, `days`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Special Offer 1',	NULL,	1,	1,	'special-offer-default-240x180-1.png',	'2023-06-27',	'2023-06-27',	1,	2,	1,	'N',	0.00,	0.00,	35.00,	30.00,	'[\"2023-06-27\"]',	NULL,	NULL,	'2023-06-23 19:57:02',	'2023-06-23 19:57:02',	NULL),
(2,	'Special Offer 2',	NULL,	2,	1,	'special-offer-default-240x180-2.png',	'2023-06-26',	'2023-08-26',	1,	2,	1,	'Y',	15.00,	12.00,	50.00,	40.00,	NULL,	'[1,6]',	NULL,	'2023-06-23 19:57:06',	'2023-06-23 19:57:06',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `special_offers_maids` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` bigint(11) NOT NULL,
  `maid_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `special_offer_id` (`special_offer_id`),
  KEY `maid_id` (`maid_id`),
  CONSTRAINT `special_offers_maids_ibfk_1` FOREIGN KEY (`special_offer_id`) REFERENCES `special_offers` (`id`),
  CONSTRAINT `special_offers_maids_ibfk_2` FOREIGN KEY (`maid_id`) REFERENCES `maids` (`maid_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `special_offers_maids` (`id`, `special_offer_id`, `maid_id`) VALUES
(1,	1,	424),
(2,	1,	456),
(3,	2,	420);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `special_offer_id` bigint(11) NULL,
ADD FOREIGN KEY (`special_offer_id`) REFERENCES `special_offers` (`id`);
----------------------------------------------------------------------------------------------------------------------------------------
