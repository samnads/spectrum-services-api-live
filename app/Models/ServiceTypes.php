<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTypes extends Model
{
    use HasFactory;
    protected $table = 'service_types';
    public $timestamps = false;
    protected $primaryKey = 'service_type_id';
}
