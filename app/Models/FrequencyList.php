<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrequencyList extends Model
{
    use HasFactory;
    protected $table = 'frequency_list';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
