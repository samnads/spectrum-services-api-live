<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerFaq extends Model
{
    use HasFactory;
    protected $table = 'customer_faq';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
