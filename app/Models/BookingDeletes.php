<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDeletes extends Model
{
    use HasFactory;
    protected $table = 'booking_deletes';
    public $timestamps = false;
    protected $primaryKey = 'booking_delete_id';
}
