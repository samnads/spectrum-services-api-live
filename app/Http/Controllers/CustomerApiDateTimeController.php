<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class CustomerApiDateTimeController extends Controller
{
    public function available_datetime(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        $input['date'] = date('Y-m-d');
        $input['date_from'] = date('Y-m-d');
        $min_slot = Config::get('values.booking_slot_start');
        $max_slot = Config::get('values.booking_slot_end');
        $slot_interval = 30; // in minutes
        $warm_up_time = 60; //in minutes
        $min_work_time = 120; // minutes
        $current_time = Carbon::now()->format('Y-m-d H:i:s');
        //$current_time = "2023-06-23 15:00:00";
        if (Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->addMinutes($warm_up_time)->format('Y-m-d H:i:s') > Carbon::createFromFormat('Y-m-d H:i:s', $input['date'] . ' ' . $max_slot)->format('Y-m-d H:i:s')) {
            $input['date'] = Carbon::now()->addDays(1)->format('Y-m-d');
            $input['date_from'] = Carbon::now()->addDays(1)->format('Y-m-d');
        }
        $input['date_to'] = Carbon::parse($input['date_from'])->addMonth(1)->endOfMonth()->toDateString(); // upto last day of next month
        $max_days = Carbon::parse($input['date_from'])->diffInDays($input['date_to']); // last day of next month
        /************************************************************* */
        $response['status'] = 'success';
        $response['available_dates'] = [];
        $response['available_times'] = [];
        $response['rush_times'] = [];
        $response['debug']['timezone'] = Config('app.timezone', 'UTC');
        $response['debug']['current_time'] = $current_time;
        $response['debug']['blocked_times'] = [];
        $response['debug']['past_times'] = [];
        /*******************************/
        for ($i = Carbon::createFromFormat('H:i:s', $min_slot)->addMinutes(-$slot_interval)->format('H:i:s'); $i < $max_slot; $i = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s')) {
            $slot = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s');
            $blocked_slot = DB::table('booking_slots as bs')
                ->select(
                    'bs.*',
                )
                ->where(['bs.date' => $input['date'], 'bs.status' => 1])
                ->where(function ($query) use ($slot) { // matching more fields
                    $query->where('bs.from_time', '=', $slot);
                    $query->orWhere([['bs.from_time', '<', $slot], ['bs.to_time', '>', $slot]]);
                })->first();
            if ($blocked_slot) {
                // blocked time slot found
                $response['debug']['blocked_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
            } else if (Carbon::createFromFormat('Y-m-d H:i:s', $input['date'] . " " . $slot)->format('Y-m-d H:i:s') >= Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->addMinutes($warm_up_time)->format('Y-m-d H:i:s')) {
                // if > current time - then valid
                if (@$input['hours']) {
                    $expected_finish_time = Carbon::createFromFormat('H:i:s', $slot)->addHours(@$input['hours'])->format('H:i:s');
                    if ($expected_finish_time > $slot && $expected_finish_time <= Carbon::createFromFormat('H:i:s', Config::get('values.work_end_time'))->format('H:i:s')) {
                        $response['available_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                    }
                } else {
                    $response['available_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                }
            } else {
                // if > current time - then valid
                $response['debug']['past_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
            }
        }
        $response['debug']['input'] = $input;
        /*******************************/
        // rush slots
        $response['rush_times'] = rushSlotsByDate($input['date_from']);
        /*******************************/
        for ($i = 0; $i <= $max_days; $i++) {
            $date = Carbon::createFromFormat('Y-m-d', $input['date_from'])->addDays($i);
            $response['available_dates'][] = $date->format('Y-m-d');
        }
        /*******************************/
        $response['message'] = sizeof($response['available_times']) ? "Available times fetched successfully." : "No time slots available.";
        $response['max_working_hour'] = Carbon::createFromFormat('H:i:s', Config::get('values.work_end_time'))->format('H:i');
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function available_time(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = $request->getContent();
        } else {
            // test input
            $data = '{"params":{"date": "16/11/2023"}}';
        }
        $data = json_decode($data, true);
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['hours'] = @$input['hours'] ?: 2; // minimum 2 hours atleast
        /************************************************************* */
        $validator = Validator::make((array) $input,
            [
                'date' => 'required|date_format:d/m/Y',
            ],
            [],
            [
                'date' => 'Date',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'input' => $input, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
        /************************************************************* */
        $min_slot = Config::get('values.booking_slot_start');
        $max_slot = Config::get('values.booking_slot_end');
        $slot_interval = 30; // in minutes
        $warm_up_time = 60; //in minutes
        $min_work_time = 120; // minutes
        $current_time = Carbon::now()->format('Y-m-d H:i:s');
        //$current_time = "2023-06-23 15:00:00";
        $response['available_times'] = [];
        $response['rush_times'] = [];
        /*******************************/
        // rush slots
        $response['rush_times'] = rushSlotsByDate($input['date']);
        /*******************************/
        $response['debug']['past_times'] = [];
        $response['debug']['blocked_times'] = [];
        if (Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->format('Y-m-d H:i:s') < Carbon::createFromFormat('Y-m-d H:i:s', $input['date'] . ' ' . $max_slot)->format('Y-m-d H:i:s')) {
            for ($i = Carbon::createFromFormat('H:i:s', $min_slot)->addMinutes(-$slot_interval)->format('H:i:s'); $i < $max_slot; $i = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s')) {
                $slot = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s');
                $blocked_slot = DB::table('booking_slots as bs')
                    ->select(
                        'bs.*',
                    )
                    ->where(['bs.date' => $input['date'], 'bs.status' => 1])
                    ->where(function ($query) use ($slot) { // matching more fields
                        $query->where('bs.from_time', '=', $slot);
                        $query->orWhere([['bs.from_time', '<', $slot], ['bs.to_time', '>', $slot]]);
                    })->first();
                if ($blocked_slot) {
                    // blocked time slot found
                    $response['debug']['blocked_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                } else if (Carbon::createFromFormat('Y-m-d H:i:s', $input['date'] . " " . $slot)->format('Y-m-d H:i:s') >= Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->addMinutes($warm_up_time)->format('Y-m-d H:i:s')) {
                    // if > current time - then valid
                    if (@$input['hours']) {
                        $expected_finish_time = Carbon::createFromFormat('H:i:s', $slot)->addHours(@$input['hours'])->format('H:i:s');
                        if ($expected_finish_time > $slot && $expected_finish_time <= Carbon::createFromFormat('H:i:s', Config::get('values.work_end_time'))->format('H:i:s')) {
                            $response['available_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                        }
                    } else {
                        $response['available_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                    }
                } else {
                    // if > current time - then valid
                    $response['debug']['past_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i');
                }
            }
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['debug']['timezone'] = Config('app.timezone', 'UTC');
        $response['debug']['current_time'] = $current_time;
        $response['debug']['input'] = $input;
        /*******************************/
        $response['message'] = sizeof($response['available_times']) ? "Available times fetched successfully." : "No time slots available.";
        $response['max_working_hour'] = Carbon::createFromFormat('H:i:s', Config::get('values.work_end_time'))->format('H:i');
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
