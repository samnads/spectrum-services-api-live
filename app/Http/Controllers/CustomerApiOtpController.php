<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Response;

class CustomerApiOtpController extends Controller
{
    public function customer_login(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = '0979797979';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
        if ($customer) {
            // EXISTING CUSTOMER FOUND
            DB::beginTransaction();
            try {
                if ($debug || in_array($input['mobilenumber'], debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], 'demo') !== false) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                }
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $response['status'] = 'success';
                $response['signinup_status'] = $customer->customer_name == null ? "new signup" : "already signup";
                $response['UserDetails']['mobile'] = last_digits($input['mobilenumber'], 9);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            // NEW CUSTOMER FOUND
            try {
                if ($debug || in_array($input['mobilenumber'], debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], 'demo') !== false) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                }
                $customer = new Customer();
                $customer->mobile_number_1 = $input['mobilenumber'];
                $customer->odoo_customer_id = 0;
                $customer->customer_type = 'HO';
                $customer->customer_name = null;
                $customer->customer_nick_name = null;
                $customer->website_url = null;
                $customer->payment_type = 'D';
                $customer->payment_mode = 'Cash';
                $customer->balance = 0;
                $customer->signed = null;
                $customer->mobile_status = 0;
                $customer->customer_notes = null;
                $customer->customer_source_id = null;
                $customer->customer_added_datetime = Carbon::now();
                $customer->customer_last_modified_datetime = Carbon::now();
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $customer->customer_code = 'SC-' . sprintf('%07d', $customer->customer_id);
                $customer->save();
                $response['status'] = 'success';
                $response['signinup_status'] = 'new signup';
                $response['UserDetails']['mobile'] = last_digits($input['mobilenumber'], 9);
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        }
        /************************************************************* */
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function check_otp(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = '0979797979';
            $data['params']['otp'] = '1234';
            $data['params']['fcm'] = 'test';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
                'otp' => 'required|string|numeric|digits:4',
                'fcm' => 'nullable|string',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
                'otp' => 'OTP',
                'fcm' => 'FCM',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::select('customers.*', 'ca.customer_address_id')
            ->leftJoin('customer_addresses as ca', function ($join) {
                $join->on('customers.customer_id', '=', 'ca.customer_id');
                $join->on('ca.default_address', '=', DB::raw('1'));
            })
            ->where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
        if ($customer) {
            if ($input['otp'] == $customer->mobile_verification_code && $customer->login_otp_expired_at >= date('Y-m-d H:i:s')) {
                DB::beginTransaction();
                try {
                    $customer->mobile_verification_code = null;
                    $customer->login_otp_expired_at = null;
                    $token = Str::random(40);
                    $customer->oauth_token = $token;
                    //$customer->mobile_status = 1;
                    $customer->save();
                    $response['status'] = 'success';
                    $response['UserDetails']['id'] = $customer->customer_id;
                    $response['UserDetails']['UserName'] = $customer->customer_name;
                    $response['UserDetails']['email_address'] = @$customer->email_address;
                    $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? $customer->customer_photo_file : Config::get('values.customer_avatar_default_file')) : Config::get('values.customer_avatar_default_file'));
                    $response['UserDetails']['token'] = $token;
                    $response['UserDetails']['mobile'] = last_digits($input['mobilenumber'], 9);
                    $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                    DB::commit();
                    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                } catch (\Exception $e) {
                    DB::rollback();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            } else {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid or Expired OTP !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Customer not found !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function resend_otp(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = '234567890';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
        if ($customer) {
            $startTime = Carbon::now();
            $finishTime = Carbon::parse($customer->login_otp_expired_at);
            $seconds = $finishTime->diffInSeconds($startTime);
            if ($customer->login_otp_expired_at > Carbon::now() && $seconds >= 1) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Please wait ' . $seconds . ' second' . ($seconds > 0 ? 's' : '') . ' before resend OTP.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            DB::beginTransaction();
            try {
                if ($debug || in_array($input['mobilenumber'], debug_mobiles())) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                }
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $response['status'] = 'success';
                $response['message'] = 'OTP resend successfully!';
                //$response['signinup_status'] = 'already signup';
                $response['UserDetails']['mobile'] = last_digits($input['mobilenumber'], 9);
                DB::commit();
                return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Customer not found !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
