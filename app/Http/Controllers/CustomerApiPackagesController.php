<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class CustomerApiPackagesController extends Controller
{
    public function packages(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = $request->getContent();
        } else {
            // test input
            $data = '{"params":{"service_type_id": 19}}';
        }
        $data = json_decode($data, true);
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'service_type_id' => 'required|integer',
            ],
            [],
            [
                'service_type_id' => 'Service Type ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $service_type = DB::table('service_types as st')
            ->select(
                'st.service_type_name as service_type'
            )
            ->where([['st.service_type_id', "=", $input['service_type_id']]])
            ->first();
        $response['status'] = 'success';
        $response['service_type'] = $service_type->service_type;
        //$response['data']['Apartment'] = [];
        //$response['data']['Villa'] = [];
        $packages = DB::table('building_type_room_packages as btrp')
            ->select(
                'btrp.id as package_id',
                'st.service_type_id as service_type_id',
                'btrp.building_type_room_id',
                'btrp.is_offer',
                //DB::raw('(CASE WHEN btrp.title IS NULL THEN br.name ELSE btrp.title END) as package_name'),
                DB::raw('IF(btrp.title IS NULL or btrp.title = "", br.name, btrp.title) as package_name'),
                'btrp.description as package_description',
                DB::raw('CONCAT("' . Config::get('values.package_offer_thumbnail_prefix_url') . '",IFNULL(btrp.customer_app_thumbnail,"default.png")) as imageurl'),
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                'btrp.actual_total_amount as actual_amount',
                'btrp.total_amount as amount',
                'bt.name as building_type',
                'st.service_type_name as service_type',
                'btrp.info_html',
                'btrp.service_time',
                'btrp.service_time as service_time_in_minutes',
                DB::raw('TIME_TO_SEC(btrp.service_time)/60 as service_time_in_minutes'),
            )
            ->leftJoin('service_types as st', 'btrp.service_type_id', 'st.service_type_id')
            ->leftJoin('building_type_room as btr', 'btrp.building_type_room_id', 'btr.id')
            ->leftJoin('building_types as bt', 'btr.building_type_id', 'bt.id')
            ->leftJoin('building_rooms as br', 'btr.building_room_id', 'br.id')
            ->where([['btrp.service_type_id', "=", $input['service_type_id']]])
            ->where([['btrp.deleted_at', "=", null], ['btr.deleted_at', "=", null], ['bt.deleted_at', "=", null], ['br.deleted_at', "=", null]])
            ->orderBy('bt.id', 'ASC')
            ->get();
        foreach ($packages as $key => $package) {
            $package_data = clone $package;
            unset($package_data->service_type);
            unset($package_data->building_type_room_id);
            unset($package_data->is_offer);
            if ($package->is_offer != 1) {
                // normal package
                //unset($package_data->imageurl);
                $normal[] = $package_data;
            } else {
                // offer package
                $offer[] = $package_data;
            }

        }
        $response['data'] = @$normal ?: [];
        $response['Offers'] = @$offer ?: [];
        /*$response['data']['Offers'] = DB::table('building_type_room_service_cost_offers as btrsco')
        ->select(
        'btrsco.id as package_offer_id',
        'br.name as package_name',
        'btrsco.description as package_description',
        DB::raw('CONCAT("' . Config::get('values.package_offer_thumbnail_prefix_url') . '",IFNULL(btrsco.customer_app_thumbnail,"default.png")) as imageurl'),
        DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
        'btrsco.actual_total_amount as actual_amount',
        'btrsco.total_amount as amount',
        'bt.name as building_type',
        'st.service_type_name as service_type',
        )
        ->leftJoin('service_types as st', 'btrsco.service_type_id', 'st.service_type_id')
        ->leftJoin('building_type_room as btr', 'btrsco.building_type_room_id', 'btr.id')
        ->leftJoin('building_types as bt', 'btr.building_type_id', 'bt.id')
        ->leftJoin('building_rooms as br', 'btr.building_room_id', 'br.id')
        ->where([['btrsco.service_type_id', "=", $input['service_type_id']]])
        ->where([['btrsco.deleted_at', "=", null], ['btr.deleted_at', "=", null], ['bt.deleted_at', "=", null], ['br.deleted_at', "=", null]])
        ->orderBy('br.id', 'ASC')
        ->get();
        foreach ($response['data']['Offers'] as $key => $offer) {
        $response['data']['Offers'][$key]->imageurl = Config::get('values.package_thumbnail_prefix_url') . $offer->imageurl;
        }*/
        $response['packages'] = [];
        foreach ($packages as $key => $package) {
            $package_data = clone $package;
            unset($package_data->service_type);
            unset($package_data->building_type_room_id);
            unset($package_data->is_offer);
            if ($package->is_offer != 1) {
                // normal package
                $response['packages'][$package->building_type][] = $package_data;
            } else {
                // offer package
                $response['packages']['Offers'][] = $package_data;
            }

        }
        $response['message'] = sizeof($packages) ? 'Packages fetched successfully.' : "No packages svailable.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
