<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use stdClass;

class CustomerApiSpecialOffersController extends Controller
{
    public function special_offers(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $offerbanners = DB::table('special_offers as so')
            ->select(
                'so.id as offer_id',
                'so.name as name',
                'so.service_type_id',
                'st.service_type_name as service_type',
                DB::raw('CONCAT("' . Config::get('values.special_offer_banner_prefix_url') . '",so.customer_app_banner_file) as imageurl'),
                DB::raw('"type" as type'),
                'so.start_date',
                'so.end_date',
                'f.name as frequency',
                'f.code as frequency_code',
                'f.id as frequency_id',
                'so.no_of_maids',
                'so.no_of_hours',
                'so.no_of_visits',
                DB::raw('"' . Carbon::parse(Config::get('values.work_end_time'))->format("H:i") . '" as max_working_hour'),
                DB::raw('(CASE WHEN so.cleaning_material_included= "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                DB::raw('so.offer_cleaning_material_rate as cleaning_material_rate'),
                DB::raw('so.actual_cleaning_material_rate as actual_material_cost'),
                DB::raw('so.offer_hourly_rate as hourly_rate'),
                DB::raw('so.actual_hourly_rate as actual_price'),
                'so.days as db_days',
                'so.dates as db_dates',
            )
            ->leftJoin('frequencies as f', 'so.frequency_id', 'f.id')
            ->leftJoin('service_types as st', 'so.service_type_id', 'st.service_type_id')
            ->where([['so.end_date', ">=", date('Y-m-d')], ['so.deleted_at', "=", null]]);
        if (@$input['offer_id']) {
            // get specific offer only
            $offerbanners->where([['so.id', "=", $input['offer_id']]]);
        }
        $offerbanners = $offerbanners->orderBy('so.end_date', 'ASC')
            ->get();
        $weeks = weekArray();
        foreach ($offerbanners as $key => $offer) {
            $maids = DB::table('special_offers_maids as som')
                ->select(
                    'm.maid_id as professional_id',
                    'm.maid_name as professional',
                )
                ->leftJoin('maids as m', 'som.maid_id', 'm.maid_id')
                ->where([['som.special_offer_id', "=", $offer->offer_id]])
                ->get();
            $offer_weeks = []; // reset
            $offer_dates = []; // reset
            /****************************************** */
            $days = json_decode($offerbanners[$key]->db_days, true);
            $dates = json_decode($offerbanners[$key]->db_dates, true);
            foreach ((array) $days as $week_number) {
                $offer_weeks[] = $weeks[$week_number];
            }
            $offerbanners[$key]->days = $offer_weeks;
            /****************************************** */
            if (sizeof((array) $dates)) {
                $offerbanners[$key]->type = 'date';
                // predefined offer dates
                $offer_dates[] = $offer->start_date; // always add
                foreach ((array) $dates as $date) {
                    $offer_dates[] = $date;
                }
                $offer_dates[] = $offer->end_date; // always add
                $offerbanners[$key]->dates = $offer_dates;
            } else {
                $offerbanners[$key]->type = 'day';
                for ($date = $offer->start_date; $date <= $offer->end_date; $date = Carbon::createFromFormat('Y-m-d', $date)->addDays(1)->format('Y-m-d')) {
                    if (in_array(Carbon::createFromFormat('Y-m-d', $date)->format('w'), $days)) {
                        $offer_dates[] = $date;
                    }
                }
                $offerbanners[$key]->dates = $offer_dates;
            }
            $offerbanners[$key]->serviceproviders = $maids;
            $offerbanners[$key]->dates = array_values(array_unique($offerbanners[$key]->dates)); // remove duplicate dates
        }
        $response['offerbanner'] = $offerbanners;
        $response['message'] = sizeof($response['offerbanner']) ? 'Special offers fetched successfully.' : "No special offers available.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
