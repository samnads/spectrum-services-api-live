<?php

namespace App\Http\Controllers;

use Cache;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiServiceTypeController extends Controller
{
    public function service_types(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        /************************************************************* */
        /*$service_type_rating = DB::table('day_services as ds')
        ->select(
        'st.service_type_id',
        DB::raw('COUNT(ds.rating) as rating_count'),
        DB::raw('SUM(ds.rating) as total_rating')
        )
        ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
        ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
        ->where([['b.booking_status', '=', 1], ['ds.service_status', '=', 2], ['ds.rating', '>', 0], ['ds.rating', '<=', 5], ['st.customer_app_status', '=', 1]])
        ->groupBy('st.service_type_id');*/
        /************************************************************* */
        $response['data'] = Cache::get('service_types') ?: DB::table('service_types as st')
            ->select(
                'st.service_type_id',
                'st.service_type_name as service_type',
                'st.customer_app_icon_file as icon_url',
                DB::raw('CONCAT("' . Config::get('values.service_type_img_prefix_url') . '",customer_app_icon_file) as icon_url'),
                DB::raw('CONCAT("' . Config::get('values.service_type_img_prefix_url') . '",customer_app_thumbnail_file) as thumbnail_url'),
                'st.service_type_model_id',
                'stm.model as service_type_model',
                'st.web_url_slug',
                //DB::raw('round((tr.total_rating/tr.rating_count),1) as rating'),
            )
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
        /*->leftJoinSub($service_type_rating, 'tr', function ($join) {
        $join->on('st.service_type_id', '=', 'tr.service_type_id');
        })*/
            ->where([['customer_app_status', '=', 1]])
            ->orderBy('st.customer_app_order_id', 'ASC')
            ->get();
        if (!Cache::get('service_types')) {
            Cache::put('service_types', $response['data'], 60);
        }
        /******************************************************** */
        $service_addons = DB::table('service_addons as sao')
            ->select(
                'sao.service_addons_id',
                'sao.service_addon_name',
                'sao.service_addon_description',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                'sao.service_minutes as additional_minutes',
                'sao.strike_amount',
                'sao.amount',
                'sao.populariry',
                'sao.cart_limit',
                'sao.customer_app_thumbnail as image',
                'sao.service_type_id'
            )
            ->where([['sao.deleted_at', "=", null]])
            ->orderBy('sao.populariry', 'DESC')
            ->get()->toArray();
        /******************************************************** */
        $service_addon_points_all = DB::table('service_addons_points as sap')
            ->select(
                'sao.service_type_id',
                'sap.service_addons_id',
                'sap.service_addons_id',
                'sap.point_html',
                'sap.point_type'
            )
            ->leftJoin('service_addons as sao', 'sap.service_addons_id', 'sao.service_addons_id')
            ->where([['sap.deleted_at', "=", null]])
            ->orderBy('sap.sort_order_id', 'ASC')
            ->get()->toArray();
        /******************************************************** */
        foreach ($response['data'] as $key => $service_type) {
            // filter addons by service id
            $response['data'][$key]->addons = array_filter($service_addons, function ($addon) use ($service_type) {
                return $addon->service_type_id == $service_type->service_type_id;
            });
            /******************************************************** */
            foreach ($response['data'][$key]->addons as $addon_key => $addon) {
                /******************************************************** */
                // filter points by service id
                $service_addon_points = array_filter($service_addon_points_all, function ($point) use ($service_type) {
                    return $point->service_type_id == $service_type->service_type_id;
                });
                /******************************************************** */
                // filter points by type
                $what_included_points = array_filter($service_addon_points, function ($point) {
                    return $point->point_type === "WHAT_INCLUDED";
                });
                /******************************************************** */
                // filter points by type
                $what_excluded_points = array_filter($service_addon_points, function ($point) {
                    return $point->point_type === "WHAT_EXCLUDED";
                });
                /******************************************************** */
                $response['data'][$key]->addons[$addon_key]->what_included = array_column(array_filter($what_included_points, function ($point) use ($addon) {
                    // filter points by addon
                    return $point->service_addons_id == $addon->service_addons_id;
                }), 'point_html');
                /******************************************************** */
                $response['data'][$key]->addons[$addon_key]->what_excluded = array_column(array_filter($what_excluded_points, function ($point) use ($addon) {
                    // filter points by addon
                    return $point->service_addons_id == $addon->service_addons_id;
                }), 'point_html');
            }
        }
        $response['status'] = 'success';
        $response['message'] = sizeof($response['data']) ? 'Service types fetched successfully.' : 'No services available.';
        return Response::json(array('result' => $response, 'cache' => true, 'debug' => $debug), 200, array(), customerResponseJsonConstants());

    }
}
