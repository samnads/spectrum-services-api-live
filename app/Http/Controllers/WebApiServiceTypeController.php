<?php

namespace App\Http\Controllers;

use Cache;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class WebApiServiceTypeController extends Controller
{
    public function service_type_data_by_id(Request $request, $service_type_id)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        $response['data'] = DB::table('service_types as st')
            ->select(
                'st.*',
            )
            ->where([['st.service_type_id', '=', $service_type_id]])
            ->orderBy('st.customer_app_order_id', 'ASC')
            ->first();
        $response['status'] = 'success';
        return Response::json(array('result' => $response, 'cache' => true, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
