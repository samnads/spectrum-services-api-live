<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingAddons;
use App\Models\BookingPackages;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\RushSlotCharges;
use App\Models\ServiceAddons;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use stdClass;

class CustomerApiBookingCreateWebController extends Controller
{
    public function create_booking_new(Request $request)
    {
        try {
            DB::beginTransaction();
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************************************************ */
            $data = json_decode($request->getContent(), true);
            $input = @$data['params']['calculation_data']['input'];
            $calculate = calculate($input);
            /************************************************************************************************ */
            if (($cusomer_error = isCustomerEligibleForBooking($input['id'])) !== true) {
                // check customer is allowed for booking
                throw new \ErrorException($cusomer_error);
            }
            /************************************************************************************************ */
            $input['professional_prefer'] = array_unique(@$input['professional_prefer'] ?: [], SORT_REGULAR); // remove duplicates - rare case or app bug
            if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
                throw new \ErrorException('Professional count mismatch.');
            }
            /************************************************************************************************ */
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['PaymentMethod']])
                ->first();
            if (!$payment_type) {
                // validate payment mode
                throw new \ErrorException('Invalid payment type !');
            }
            /************************************************************************************************ */
            $customer_address = CustomerAddress::where(['customer_address_id' => $input['address_id'], 'customer_id' => $input['id']])->first();
            if (!$customer_address) {
                // validate address
                throw new \ErrorException('Invalid address !');
            }
            //$input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            $input['time_from'] = Carbon::createFromFormat('H:i', $input['time'])->format('H:i:s');
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($input['hours'] * 60)->format('H:i:s');
            /************************************************************************************************ */
            if (@$input['rush_slot_id']) {
                $rush_slot = RushSlotCharges::where(['id' => $input['rush_slot_id'], 'time' => $input['time_from'], 'deleted_at' => null])->first();
            }
            /************************************************************************************************ */
            // real time time-slot availability check
            if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
                throw new \ErrorException($error);
            }
            /************************************************************************************************ */
            // real time maid availability check
            // real time maid availability check
            $booking = new stdClass();
            $booking->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking->service_start_date = $input['date'];
            $booking->booking_type = $input['frequency'];
            $booking->time_from = $input['time_from'];
            $booking->time_to = $input['time_to'];
            if ($input['frequency'] == "OD") {
                $service_end_date = $input['date'];
                $service_actual_end_date = $input['date'];
                $service_end = 1; // it's one day
            } else if ($input['frequency'] == "WE") {
                if ($input['No_weeks'] > 0) {
                    $service_end = 1;
                    $service_end_date = Carbon::parse($input['date'])->addWeeks($input['No_weeks'] - 1)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            } else if ($input['frequency'] == "BW") {
                if ($input['No_weeks'] > 0) {
                    $service_end = 1; // end date available
                    $service_end_date = Carbon::parse($input['date'])->addWeeks(($input['No_weeks'] * 2) - 2)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            }
            $booking->service_actual_end_date = $service_actual_end_date;
            $booking->service_end = $service_end;
            /*************************************** */
            $busy_bookings = get_busy_bookings($booking, []);
            //dd(array_unique(array_column($busy_bookings->toArray(), 'booking_id')));
            if ($input['frequency'] == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column($input['professional_prefer'], 'professional_id');
            /*************************************** */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available on " . Carbon::parse($input['date'])->format('d/m/Y') . " between " . Carbon::createFromFormat('H:i:s', $input['time_from'])->format('h:i A') . " - " . Carbon::createFromFormat('H:i:s', $input['time_to'])->format('h:i A') . "."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
            //dd($available_maid_ids);
            /*************************************** */
            for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
            }
            /************************************************************************************************ */
            // database transaction starts here
            $booking_common_id = null;
            $service_start_date = $input['date'];
            foreach ($selected_professionals as $key => $professional_id) {
                $row = new stdClass();
                $row->customer_id = $input['id'];
                $row->reference_id = ""; // update after getting insert id
                $row->customer_address_id = $input['address_id'];
                //$row->maid_id = $professional_id;
                $row->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                $row->service_type_id = $input['service_type_id'];
                $row->service_week_day = Carbon::parse($service_start_date)->format('w');
                $row->time_from = $input['time_from'];
                $row->time_to = $input['time_to'];
                $row->booking_type = $input['frequency'];
                $row->booking_category = "C";
                $row->booked_from = "M";
                $row->pending_amount = 0;
                $row->price_per_hr = ($calculate['service_fee'] / $input['professionals_count']) / $input['hours'];
                $row->amount_before_discount = $calculate['amount_before_discount'] / $input['professionals_count'];
                $row->service_charge = $calculate['taxable_amount'] / $input['professionals_count'];
                $row->discount = $calculate['discount_total'] / $input['professionals_count'];
                $row->frequency_discount = $calculate['frequency_discount'] / $input['professionals_count'];
                $row->cleaning_material = $calculate['cleaning_material'];
                $row->cleaning_material_fee = $calculate['cleaning_material_fee'] / $input['professionals_count'];
                $row->vat_charge = $calculate['vat_amount'] / $input['professionals_count'];
                $row->payment_type_charge = $calculate['payment_type_charge'] / $input['professionals_count'];
                $row->total_amount = $calculate['total_payable'] / $input['professionals_count'];
                $row->is_locked = 0;
                $row->no_of_maids = $input['professionals_count'];
                $row->no_of_hrs = $input['hours'];
                $row->booked_by = 1;
                $row->payment_type_id = $payment_type->id;
                $row->pay_by = $payment_type->name;
                $row->booking_note = @$input['instructions'];
                $row->booked_datetime = Carbon::now()->toDateTimeString();
                $row->service_start_date = $service_start_date;
                $row->coupon_id = @$calculate['coupon_discount'] > 0 ? $calculate['coupon_id'] : null;
                $row->special_offer_id = @$input['offer_id'];
                //
                $row->rush_slot_id = @$rush_slot->id;
                $row->rush_slot_charge = $calculate['rush_slot_charge'] ? ($calculate['rush_slot_charge'] / $input['professionals_count']) : 0;
                //
                $row->addons_included = @$calculate['addons'] ? 1 : 0;
                $row->addons_amount = @$calculate['addons_amount'] ? ($calculate['addons_amount'] / $input['professionals_count']) : 0;
                //$row->is_package = @$input['package_ids'];
                if ($payment_type->id == "1") {
                    // cash mode
                    $row->web_book_show = 1;
                } else {
                    $row->web_book_show = 0;
                }
                $row->booking_status = 0;
                if ($input['frequency'] == "OD") {
                    $row->service_end_date = $service_start_date;
                    $row->service_actual_end_date = $service_start_date;
                    $row->service_end = 1; // it's one day
                } else if ($input['frequency'] == "WE") {
                    if ($input['No_weeks'] > 0) {
                        $row->service_end = 1; // end date available
                        $service_end_date = Carbon::parse($service_start_date)->addWeeks($input['No_weeks'] - 1)->format('Y-m-d');
                        $row->service_end_date = $service_end_date;
                        $row->service_actual_end_date = $service_end_date;
                    } else {
                        $row->service_end = 0; // never ends
                        $row->service_end_date = $service_start_date;
                        $row->service_actual_end_date = $service_start_date;
                    }
                } else if ($input['frequency'] == "BW") {
                    if ($input['No_weeks'] > 0) {
                        $row->service_end = 1; // end date available
                        $service_end_date = Carbon::parse($service_start_date)->addWeeks(($input['No_weeks'] * 2) - 2)->format('Y-m-d');
                        $row->service_end_date = $service_end_date;
                        $row->service_actual_end_date = $service_end_date;
                    } else {
                        $row->service_end = 0; // never ends
                        $row->service_end_date = $service_start_date;
                        $row->service_actual_end_date = $service_start_date;
                    }
                }
                /********************************************************** */
                // extra amount fields for more accuracy (currently used for mail purpose only)
                // these field not devided by professionals count
                $row->_service_amount = $calculate['service_fee'] ? $calculate['service_fee'] / $input['professionals_count']: 0;
                $row->_cleaning_materials_amount = $calculate['cleaning_material_fee'] ?: 0;
                $row->_service_addons_amount = $calculate['addons_amount'] ? $calculate['addons_amount'] / $input['professionals_count'] : 0;
                $row->_packages_amount = $calculate['packages_amount'] ?: 0;
                $row->_amount_before_discount = $calculate['amount_before_discount'] ?: 0;
                $row->_coupon_discount = $calculate['coupon_discount'] ?: 0;
                $row->_discount_total = $calculate['discount_total'] ?: 0;
                $row->_taxable_amount = $calculate['taxable_amount'] ? $calculate['taxable_amount'] / $input['professionals_count'] : 0;
                $row->_vat_percentage = $calculate['tax_percentage'] ?: 0;
                $row->_vat_amount = $calculate['vat_amount'] ? $calculate['vat_amount'] / $input['professionals_count'] : 0;
                $row->_taxed_amount = $calculate['taxed_amount'] ? $calculate['taxed_amount'] / $input['professionals_count'] : 0;
                $row->_payment_type_charge = $calculate['payment_type_charge'] ? $calculate['payment_type_charge'] / $input['professionals_count'] : 0;
                $row->_total_payable = $calculate['total_payable'] ? $calculate['total_payable'] / $input['professionals_count'] : 0;
                /********************************************************** */
                $id = Booking::insertGetId((array) $row);
                $booking = Booking::find($id);
                $booking_common_id = @$booking_common_id ?: $id;
                if ($input['professionals_count'] > 1) {
                    // for act as single booking use common id
                    $booking->booking_common_id = $booking_common_id;
                }
                $booking->reference_id = Config::get('values.booking_ref_prefix') . $booking_common_id;
                $booking->save();
                $bookings[] = $booking;
            }
            /************************************************************************************************ */
            // addons data insertion
            if (@$calculate['addons']) {
                // insert addon data rows
                foreach ($calculate['addons'] as $key => $addon) {
                    $booking_addon = new BookingAddons();
                    $booking_addon->booking_id = $booking_common_id; // will update later
                    $booking_addon->service_addons_id = $addon['service_addons_id'];
                    $booking_addon->service_addon_name = $addon['service_addon_name'];
                    $booking_addon->service_minutes = $addon['service_minutes'];
                    $booking_addon->strike_amount = $addon['strike_amount'];
                    $booking_addon->unit_price = $addon['amount'];
                    $booking_addon->quantity = $addon['quantity'];
                    $booking_addon->amount = $addon['amount'] * $addon['quantity'];
                    $booking_addon->save();
                }
            }
            /************************************************************************************************ */
            // package data insertion
            if (@$calculate['packages']) {
                foreach ($calculate['packages'] as $key => $package) {
                    $booking_package = new BookingPackages();
                    $booking_package->booking_id = $booking_common_id;
                    $booking_package->building_type_room_package_id = $package->id;
                    $booking_package->unit_price = $package->total_amount;
                    $booking_package->quantity = $package->quantity;
                    $booking_package->save();
                }
            }
            /************************************************************************************************ */
            $response['status'] = 'success';
            $response['message'] = 'Booking saved with Ref. No. ' . $bookings[0]->reference_id . '.';
            /********************************************************************************************* */
            $response['payment_details'] = array(
                // based on no. of profs
                'service_fee' => (float) number_format($calculate['service_fee'], 2, ".", ""),
                "cleaning_materials" => (float) number_format($calculate['cleaning_material_fee'], 2, ".", ""),
                "addons_amount" => (float) number_format($calculate['addons_amount'], 2, ".", ""),
                "rush_slot_extra_charge" => (float) number_format($calculate['rush_slot_charge'], 2, ".", ""),
                "discount" => (float) number_format($calculate['discount_total'], 2, ".", ""),
                "taxable_amount" => (float) number_format($calculate['taxable_amount'], 2, ".", ""),
                "tax_amount" => (float) number_format($calculate['vat_amount'], 2, ".", ""),
                "payment_type_charge" => (float) number_format($calculate['payment_type_charge'], 2, ".", ""),
                "total" => (float) number_format($calculate['total_payable'], 2, ".", ""),
            );
            /********************************************************************************************* */
            $response['No_weeks'] = $input['No_weeks'];
            $response['Cards'] = [];
            /********************************************************************************************* */
            $response['booking_details'] = array(
                "booking_id" => $booking_common_id,
                "booking_reference_id" => $bookings[0]->reference_id,
                "total_hours" => $input['hours'],
                "professionals_count" => sizeof($selected_professionals),
                "booking_ref" => $bookings[0]->reference_id,
                "payment_method" => $payment_type->name,
                "status" => $input['PaymentMethod'] == 1 ? "Pending Approval" : "Payment Pending",
            );
            /********************************************************************************************* */
            $mail = new AdminApiMailController;
            if ($payment_type->id == "1") {
                // cash mode
                $notify = new stdClass();
                $notify->customer_id = $input['id'];
                $notify->booking_id = $booking_common_id;
                $notify->service_date = $input['frequency'] == "OD" ? $input['date'] : null;
                if (@$calculate['packages']) {
                    $notify->content = "Booking (Package) is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                } else {
                    $notify->content = "Booking is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                }
                /********************************************************************************************* */
                addCustomerNotification($notify);
            } else if ($payment_type->id == "2") {
                $customer = Customer::find($booking->customer_id);
                $payFortData = [
                    'command' => 'PURCHASE',
                    'access_code' => Config::get('values.payfort_access_code'),
                    'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
                    'amount' => (float) $response['payment_details']['total'] * 100,
                    'currency' => Config::get('values.currency_code'),
                    'language' => strtolower(Config::get('values.language_code')),
                    'customer_name' => preg_replace('/[^A-Za-z0-9\-]/', '', $customer->customer_name),
                    'customer_email' => Customer::where('customer_id', $booking->customer_id)->first()->email_address ?: 'test@example.com',
                    'order_description' => $bookings[0]->reference_id,
                    'merchant_reference' => $bookings[0]->reference_id,
                    'merchant_extra' => $bookings[0]->reference_id,
                    //'eci' => 'RECURRING',
                    //'token_name' => "70c9101526bf42a599b0e0189b57779e",
                    //'return_url' => url('api/customer/payment/payfort/processing'),
                    'return_url' => Config::get('values.web_booking_url') . "booking/payment/payfort/process/" . $booking_common_id,
                ];
                ksort($payFortData);
                $hash_value = Config::get('values.payfort_sha_request_phrase');
                foreach ($payFortData as $name => $value) {
                    if ($value != null) {
                        $hash_value .= $name . '=' . $value;
                    }
                }
                $hash_value .= Config::get('values.payfort_sha_request_phrase');
                $payFortData['signature'] = hash('sha256', $hash_value);
                $response['checkout_url'] = Config::get('values.payfort_payment_page_url');
                $response['checkout_data']['payfort_data'] = $payFortData;
                initiate_online_payment_for_booking($booking_common_id, $payFortData, $response['payment_details']['total'] - $response['payment_details']['payment_type_charge'], $response['payment_details']['payment_type_charge']);
            } else if ($payment_type->id == "3") {
                initiate_online_payment_for_booking($booking_common_id, @$data['params']['checkout_token_data'], $response['payment_details']['total'] - $response['payment_details']['payment_type_charge'], $response['payment_details']['payment_type_charge']);
                // apple pay
                Mail::send(
                    'emails.debug_mail',
                    [
                        'input' => json_encode(@$data['params']['checkout_token_data']),
                        'ip' => $_SERVER['REMOTE_ADDR']
                    ],
                    function ($m) {
                        $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                        $m->to('samnad.s@azinova.info', 'Samnad S');
                        $m->subject('Apple Pay Test Web');
                    }
                );
                $apple_pay = new ApplePayController();
                //$input['checkout_token_data'] = '{\"shippingContact\":{\"addressLines\":[\"uae\",\"business bay\"],\"administrativeArea\":\"\",\"country\":\"United Arab Emirates\",\"countryCode\":\"AE\",\"familyName\":\"L\",\"givenName\":\"Revathy\",\"locality\":\"uae\",\"postalCode\":\"\",\"subAdministrativeArea\":\"\",\"subLocality\":\"silicon oasis\"},\"token\":{\"paymentData\":{\"data\":\"5dP1CzgI6eGBs2Pyq23gVh2FQi7nW9y5J2A5vFBzb4Y0znGN40AOUIrTsg5C6479l+0Qud4kAz1inV51BBlsGfQ1vYJddRoY8DJLIHWcty16TlkB19m+r6UvSb\/fYZ31gzzG1hqMVx22YSg5iDOcQQzyIn2Mqqo5aF9f22Yr45q65EDYLrDOhHwmvVLxD+A6jksna73s3vr6iXIxkjpBBcyKkruYuaQnD2CpPDzc3XctACckmYumzkcNIKv2ZQ53pndAZgmVePr+C5JHNGyBDNmXtqy9dlTabfzWc4mrhHdbAzObsLapeXDU7NWUNi+cfFnJgIwGJG1SawjooLUWKg8xCEVbfusqKxhQ+CNVB4MN8IdClOj64JHTCt66\/fxJvu\/iL4edL0MtUDcPz\/MP\/DvrGFF6uWpJ1nG\/clCZ4w==\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7\/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB\/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI\/JJxE+T5O8n5sT2KGw\/orv9LkswDwYDVR0TAQH\/BAUwAwEB\/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv\/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn\/Rd8LCFtlk\/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYkwggGFAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTI0MDIxOTA3MDYxNlowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEILn9pDCOiXGtQ3MMrauoKoKMOlBCeOh9hAZh+xL\/A7KsMAoGCCqGSM49BAMCBEgwRgIhAIOW0ZuWeQA5yOiA60T0V2xyiC6ogAD4pwFiO3yLbo+PAiEAjk7JYPxEGTI0zEViwbEq9YjNchsLoAIX9YgBzWxWGSgAAAAAAAA=\",\"header\":{\"publicKeyHash\":\"UoMmcgRJQIDTPgHvMdzPMHhaxJw3oqpC\/FLZbKx4QhQ=\",\"ephemeralPublicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE6H5GtVD5n6KOT1c+3qbdHY4gk\/oDcqwxR+F0w9rYm8sMaJ5DVRtG6RJbzuo2c8Z3aFJWgoEU\/7Bw0fDbWJ1JAQ==\",\"transactionId\":\"2dbf6226ba5bbfb08d025cd84f81c1ee7d5079aa30950317ce3447e58d6c1aa5\"},\"version\":\"EC_v1\"},\"paymentMethod\":{\"displayName\":\"Visa 1920\",\"network\":\"Visa\",\"type\":\"debit\"},\"transactionIdentifier\":\"2DBF6226BA5BBFB08D025CD84F81C1EE7D5079AA30950317CE3447E58D6C1AA5\"}}';
                if ($input['id'] == '87812') {
                    $response['payment_details']['total'] = 1;
                }
                $payfort_data = $apple_pay->payfort_apple_pay(@$data['params']['checkout_token_data'], $booking_common_id, $response['payment_details']['total']);
                after_payfort_apple_pay_success($payfort_data, $booking_common_id, $input['platform']);
            }
            /********************************************************************************************* */
            // send booking confirmation mail to admin
            try {
                if ($payment_type->id == "1") {
                    $mail->booking_confirmation_to_admin($booking_common_id);
                    $mail->booking_confirmation_to_customer($booking_common_id);
                }
            } catch (\Exception $e) {
                //throw new \ErrorException($e->getMessage());
            }
            /********************************************************************************************* */
            DB::commit();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage(), 'input' => $input), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
