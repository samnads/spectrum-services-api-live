<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingAddons;
use App\Models\BookingPackages;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\RushSlotCharges;
use App\Models\ServiceAddons;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use stdClass;

class CustomerApiBookingCreateController extends Controller
{
    public function create_booking(Request $request)
    {
        try {
            set_time_limit(300);
            $debug = toggleDebug(false); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['date'] = "15/03/2024"; // dd/mm/yyyy format
                $data['params']['frequency'] = "OD";
                $data['params']['time'] = "08:00";
                $data['params']['hours'] = 2;
                $data['params']['professionals_count'] = 2;
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    //array('professional_id' => 424),
                    //array('professional_id' => 457),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
                $data['params']['cleaning_materials'] = "yes";
                $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
                $data['params']['instructions'] = "my instruction";
                $data['params']['promocode_id'] = null;
                $data['params']['PaymentMethod'] = 1;
                $data['params']['No_weeks'] = 0;
                $data['params']['service_type_id'] = 1;
                $data['params']['offer_id'] = null;
                $data['params']['package_ids'] = []; //[1, 2];
                $data['params']['frequency_discount'] = 0;
                $data['params']['rush_slot_id'] = null;
                //$data['params']['addons'] = [['service_addons_id' => 1, 'quantity' => 1], ['service_addons_id' => 2, 'quantity' => 2]];
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            /************************************************************* */
            // if offer get data from offer data
            if (@$input['offer_id']) {
                $offer = DB::table('special_offers as so')
                    ->select(
                        'so.*',
                        'f.code as frequency_code',
                    )
                    ->leftJoin('frequencies as f', 'so.frequency_id', 'f.id')
                    ->where(['so.id' => $input['offer_id']])
                    ->first();
                if (!$offer) {
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid offer !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
                // change based on offer
                $input['frequency'] = $offer->frequency_code;
                $input['professionals_count'] = $offer->no_of_maids;
                $input['hours'] = $offer->no_of_hours;
                $input['cleaning_materials'] = $offer->cleaning_material_included == 'Y' ? 'yes' : 'no';
            }
            // if offer get data from offer data
            else if (@$input['package_ids']) {
                $packages = DB::table('building_type_room_packages as btrp')
                    ->select(
                        'btrp.*'
                    )
                    ->whereIn('btrp.id', $input['package_ids'])
                    ->where([['btrp.deleted_at', "=", null]])
                    ->get();
                //dd(@$packages);
                if (!$packages) {
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid package !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
                // change based on package
                $input['frequency'] = "OD";
                $input['professionals_count'] = 1;
                $input['hours'] = 2;
                $input['cleaning_materials'] = 'no';
            }
            //dd(@$packages);
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [
                    'frequency' => 'required|in:OD,WE,BW',
                    'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                    'time' => 'required|date_format:H:i',
                    'hours' => 'required|integer|min:2',
                    'professionals_count' => 'required|integer',
                    'professional_prefer' => 'array',
                    'cleaning_materials' => 'required|string',
                    'address_id' => 'required|integer',
                    'instructions' => 'nullable|string',
                    'promocode_id' => 'nullable|integer',
                    'PaymentMethod' => 'required|integer',
                    'No_weeks' => 'integer',
                    'service_type_id' => 'required|integer',
                    'offer_id' => 'nullable|integer',
                    'package_ids' => 'nullable|array',
                    'frequency_discount' => 'nullable|numeric',
                    'rush_slot_id' => 'nullable|integer',
                    'addons' => 'array',
                ],
                [],
                [
                    'frequency' => 'Frequency',
                    'date' => 'Date',
                    'time' => 'Time',
                    'hours' => 'Hours',
                    'professionals_count' => 'Professionals Count',
                    'professional_prefer' => 'Selected Professionals',
                    'cleaning_materials' => 'Cleaning Materials',
                    'address_id' => 'Address',
                    'instructions' => 'Instructions',
                    'PaymentMethod' => 'Payment Method',
                    'promocode_id' => 'Promo Code ID',
                    'No_weeks' => 'No. of Weeks',
                    'service_type_id' => 'Service Type ID',
                    'offer_id' => 'Offer ID',
                    'package_ids' => 'Package IDS',
                    'frequency_discount' => 'Frequency Discount',
                    'rush_slot_id' => 'Rush Slot ID',
                    'addons' => 'Addon IDS',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            if (($cusomer_error = isCustomerEligibleForBooking($input['id'])) !== true) {
                // check customer is allowed for booking
                return Response::json(array('result' => array('status' => 'failed', 'message' => $cusomer_error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $input['professional_prefer'] = array_unique(@$input['professional_prefer'] ?: [], SORT_REGULAR); // remove duplicates - rare case or app bug
            if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => "Professional count mismatch."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['PaymentMethod']])
                ->first();
            if (!$payment_type) {
                // validate payment mode
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid payment type !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $customer_address = CustomerAddress::where(['customer_address_id' => $input['address_id'], 'customer_id' => $input['id']])->first();
            if (!$customer_address) {
                // validate address
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid address !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            $input['time_from'] = Carbon::createFromFormat('H:i', $input['time'])->format('H:i:s');
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($input['hours'] * 60)->format('H:i:s');
            /****************************************************************************************** */
            if (@$input['rush_slot_id']) {
                $rush_slot = RushSlotCharges::where(['id' => $input['rush_slot_id'], 'time' => $input['time_from'], 'deleted_at' => null])->first();
            }
            /****************************************************************************************** */
            // real time time-slot availability check
            if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /****************************************************************************************** */
            // real time maid availability check
            $booking = new stdClass();
            $booking->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking->service_start_date = $input['date'];
            $booking->booking_type = $input['frequency'];
            $booking->time_from = $input['time_from'];
            $booking->time_to = $input['time_to'];
            if ($input['frequency'] == "OD") {
                $service_end_date = $input['date'];
                $service_actual_end_date = $input['date'];
                $service_end = 1; // it's one day
            } else if ($input['frequency'] == "WE") {
                if ($input['No_weeks'] > 0) {
                    $service_end = 1;
                    $service_end_date = Carbon::parse($input['date'])->addWeeks($input['No_weeks'] - 1)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            } else if ($input['frequency'] == "BW") {
                if ($input['No_weeks'] > 0) {
                    $service_end = 1; // end date available
                    $service_end_date = Carbon::parse($input['date'])->addWeeks(($input['No_weeks'] * 2) - 2)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            }
            $booking->service_actual_end_date = $service_actual_end_date;
            $booking->service_end = $service_end;
            /*************************************** */
            $busy_bookings = get_busy_bookings($booking, []);
            //dd(array_unique(array_column($busy_bookings->toArray(), 'booking_id')));
            if ($input['frequency'] == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column($input['professional_prefer'], 'professional_id');
            /*************************************** */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available on " . Carbon::parse($input['date'])->format('d/m/Y') . " between " . Carbon::createFromFormat('H:i:s', $input['time_from'])->format('h:i A') . " - " . Carbon::createFromFormat('H:i:s', $input['time_to'])->format('h:i A') . "."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
            //dd($available_maid_ids);
            /*************************************** */
            for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
            }
            /****************************************************************************************** */
            DB::beginTransaction();
            try {
                $tax_percentage = Config::get('values.vat_percentage');
                $coupon_discount = 0;
                $frequency_discount = 0;
                $want_cleaning_material = strtoupper($input['cleaning_materials'][0]); // Y or N
                $service_start_date = $input['date'];
                $service_fee_total = 0;
                $rush_slot_extra_charge = @$rush_slot->extra_charge ? ($rush_slot->extra_charge / $input['professionals_count']) : 0;
                /********************************************* */
                // insert addons to booking addons
                $booking_addons_amount_per_maid = 0;
                $booking_addons_ids = [];
                if (@$input['addons']) {
                    foreach ($input['addons'] as $key => $addon) {
                        $service_addon = ServiceAddons::findOrFail($addon['service_addons_id']);
                        $booking_addon = new stdClass();
                        $booking_addon->booking_id = null; // will update later
                        $booking_addon->service_addons_id = $addon['service_addons_id'];
                        $booking_addon->service_addon_name = $service_addon->service_addon_name;
                        $booking_addon->service_minutes = $service_addon->service_minutes;
                        $booking_addon->unit_price = $service_addon->amount;
                        $booking_addon->quantity = $addon['quantity'];
                        $booking_addon->amount = $booking_addon->unit_price * $booking_addon->quantity;
                        $booking_addons_amount_per_maid += $booking_addon->amount;
                        $booking_addons_ids[] = BookingAddons::insertGetId((array) $booking_addon);
                        $response['addons'][$key]['id'] = $addon['service_addons_id'];
                        $response['addons'][$key]['name'] = $service_addon->service_addon_name;
                        $response['addons'][$key]['quantity'] = $addon['quantity'];
                    }
                }
                $booking_addons_amount_per_maid = $booking_addons_amount_per_maid / $input['professionals_count'];
                /********************************************* */
                if (@$offer) {
                    // change rates based on offer
                    $price_per_hr_with_material = $offer->offer_hourly_rate + $offer->offer_cleaning_material_rate;
                    $price_per_hr_without_material = $offer->offer_hourly_rate;
                    $price_per_hr = $price_per_hr_without_material;
                    //
                    $service_fee_total = $price_per_hr * $input['hours'];
                    $cleaning_material_rate_per_hr = ($want_cleaning_material == 'Y') ? ($price_per_hr_with_material - $price_per_hr_without_material) : 0;
                    $cleaning_material_fee_total = $cleaning_material_rate_per_hr * $input['hours'];
                    $total_amount_before_discount = $service_fee_total + $cleaning_material_fee_total + $rush_slot_extra_charge + $booking_addons_amount_per_maid;
                    if (@$input['promocode_id']) {
                        $coupon_discount = applyCoupon($input['promocode_id'], $input['id'], $input['date'], $input['hours'], $total_amount_before_discount);
                    }
                    if (@$input['frequency_discount']) {
                        $frequency_discount = $input['frequency_discount'] / $input['professionals_count'];
                    }
                    $total_amount_after_discount = $total_amount_before_discount - $coupon_discount - $frequency_discount;
                    $tax_amount = number_format((float) $total_amount_after_discount * ($tax_percentage / 100), 2, '.', '');
                    $amount_with_tax = $total_amount_after_discount + $tax_amount;
                    if ($payment_type->charge_type == "F") {
                        // fixed charge
                        $payment_type_charge = $payment_type->charge / $input['professionals_count'];
                    } else {
                        // percentage of total
                        $payment_type_charge = ($payment_type->charge / 100) * $amount_with_tax; // dont divice by prof.
                    }
                    $amount_payable = $amount_with_tax + $payment_type_charge;
                } else if (@$packages) {
                    $price_per_hr_with_material = 0;
                    $price_per_hr_without_material = 0;
                    $price_per_hr = $price_per_hr_without_material;
                    //
                    foreach ($packages as $key => $package) {
                        $service_fee_total += $package->total_amount;
                    }
                    $cleaning_material_fee_total = 0;
                    $cleaning_material_rate_per_hr = 0;
                    $total_amount_before_discount = $service_fee_total + $cleaning_material_fee_total + $rush_slot_extra_charge + $booking_addons_amount_per_maid;
                    if (@$input['promocode_id']) {
                        $coupon_discount = applyCoupon($input['promocode_id'], $input['id'], $input['date'], $input['hours'], $total_amount_before_discount);
                    }
                    if (@$input['frequency_discount']) {
                        $frequency_discount = $input['frequency_discount'] / $input['professionals_count'];
                    }
                    $total_amount_after_discount = $total_amount_before_discount - $coupon_discount - $frequency_discount;
                    $tax_amount = number_format((float) $total_amount_after_discount * ($tax_percentage / 100), 2, '.', '');
                    $amount_with_tax = $total_amount_after_discount + $tax_amount;
                    if ($payment_type->charge_type == "F") {
                        // fixed charge
                        $payment_type_charge = $payment_type->charge / $input['professionals_count'];
                    } else {
                        // percentage of total
                        $payment_type_charge = ($payment_type->charge / 100) * $amount_with_tax; // dont divice by prof.
                    }
                    $amount_payable = $amount_with_tax + $payment_type_charge;
                } else {
                    $price_per_hr_with_material = getHourlyRateByHourAndMaterial($input['hours'], 'Y');
                    $price_per_hr_without_material = getHourlyRateByHourAndMaterial($input['hours'], 'N');
                    $price_per_hr = $price_per_hr_without_material;
                    //
                    $service_fee_total = $price_per_hr * $input['hours'];
                    $cleaning_material_rate_per_hr = ($want_cleaning_material == 'Y') ? ($price_per_hr_with_material - $price_per_hr_without_material) : 0;
                    $cleaning_material_fee_total = $cleaning_material_rate_per_hr * $input['hours'];
                    $total_amount_before_discount = $service_fee_total + $cleaning_material_fee_total + $rush_slot_extra_charge + $booking_addons_amount_per_maid;
                    if (@$input['promocode_id']) {
                        $coupon_discount = applyCoupon($input['promocode_id'], $input['id'], $input['date'], $input['hours'], $total_amount_before_discount);
                    }
                    if (@$input['frequency_discount']) {
                        $frequency_discount = $input['frequency_discount'] / $input['professionals_count'];
                    }
                    $total_amount_after_discount = $total_amount_before_discount - $coupon_discount - $frequency_discount;
                    $tax_amount = number_format((float) $total_amount_after_discount * ($tax_percentage / 100), 2, '.', '');
                    $amount_with_tax = $total_amount_after_discount + $tax_amount;
                    if ($payment_type->charge_type == "F") {
                        // fixed charge
                        $payment_type_charge = $payment_type->charge / $input['professionals_count'];
                    } else {
                        // percentage of total
                        $payment_type_charge = ($payment_type->charge / 100) * $amount_with_tax; // dont divice by prof.
                    }
                    /************************ */
                    $amount_payable = $amount_with_tax + $payment_type_charge;
                }
                $total_discount = $coupon_discount + $frequency_discount; // + add all discounts
                /************************ */
                $booking_common_id = null;
                foreach ($selected_professionals as $key => $professional_id) {
                    $row = new stdClass();
                    $row->customer_id = $input['id'];
                    $row->reference_id = ""; // update after getting insert id
                    $row->customer_address_id = $input['address_id'];
                    //$row->maid_id = $professional_id;
                    $row->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                    $row->service_type_id = $input['service_type_id'];
                    $row->service_week_day = Carbon::parse($service_start_date)->format('w');
                    $row->time_from = $input['time_from'];
                    $row->time_to = $input['time_to'];
                    $row->booking_type = $input['frequency'];
                    $row->booking_category = "C";
                    $row->booked_from = "M";
                    $row->pending_amount = 0;
                    $row->price_per_hr = $price_per_hr;
                    $row->amount_before_discount = $total_amount_before_discount;
                    $row->service_charge = $total_amount_after_discount;
                    $row->discount = $total_discount;
                    $row->frequency_discount = $frequency_discount;
                    $row->cleaning_material = $want_cleaning_material;
                    $row->cleaning_material_fee = $cleaning_material_fee_total;
                    $row->vat_charge = $tax_amount;
                    $row->payment_type_charge = $payment_type_charge;
                    $row->total_amount = $amount_payable;
                    $row->is_locked = 0;
                    $row->no_of_maids = $input['professionals_count'];
                    $row->no_of_hrs = $input['hours'];
                    $row->booked_by = 1;
                    $row->payment_type_id = $payment_type->id;
                    $row->pay_by = $payment_type->name;
                    $row->booking_note = $input['instructions'];
                    $row->booked_datetime = Carbon::now()->toDateTimeString();
                    $row->service_start_date = $service_start_date;
                    $row->coupon_id = $coupon_discount > 0 ?: null;
                    $row->special_offer_id = @$input['offer_id'];
                    //
                    $row->rush_slot_id = @$rush_slot->id;
                    $row->rush_slot_charge = $rush_slot_extra_charge;
                    //
                    $row->addons_included = @$input['addons'] ? 1 : 0;
                    $row->addons_amount = @$input['addons'] ? $booking_addons_amount_per_maid : 0;
                    //$row->is_package = @$input['package_ids'];
                    if ($payment_type->id == "1") {
                        // cash mode
                        $row->web_book_show = 1;
                    } else {
                        $row->web_book_show = 0;
                    }
                    $row->booking_status = 0;
                    if ($input['frequency'] == "OD") {
                        $row->service_end_date = $service_start_date;
                        $row->service_actual_end_date = $service_start_date;
                        $row->service_end = 1; // it's one day
                    } else if ($input['frequency'] == "WE") {
                        if ($input['No_weeks'] > 0) {
                            $row->service_end = 1; // end date available
                            $service_end_date = Carbon::parse($service_start_date)->addWeeks($input['No_weeks'] - 1)->format('Y-m-d');
                            $row->service_end_date = $service_end_date;
                            $row->service_actual_end_date = $service_end_date;
                        } else {
                            $row->service_end = 0; // never ends
                            $row->service_end_date = $service_start_date;
                            $row->service_actual_end_date = $service_start_date;
                        }
                    } else if ($input['frequency'] == "BW") {
                        if ($input['No_weeks'] > 0) {
                            $row->service_end = 1; // end date available
                            $service_end_date = Carbon::parse($service_start_date)->addWeeks(($input['No_weeks'] * 2) - 2)->format('Y-m-d');
                            $row->service_end_date = $service_end_date;
                            $row->service_actual_end_date = $service_end_date;
                        } else {
                            $row->service_end = 0; // never ends
                            $row->service_end_date = $service_start_date;
                            $row->service_actual_end_date = $service_start_date;
                        }
                    }
                    /********************************************************** */
                    $row->_service_amount = $total_amount_after_discount;
                    $row->_cleaning_materials_amount = $cleaning_material_fee_total;
                    $row->_service_addons_amount = $booking_addons_amount_per_maid;
                    $row->_packages_amount = null;
                    $row->_amount_before_discount = $total_amount_before_discount;
                    $row->_coupon_discount = $total_discount;
                    $row->_discount_total = $total_discount;
                    $row->_taxable_amount = $total_amount_after_discount;
                    $row->_vat_percentage = Config::get('values.vat_percentage');
                    $row->_vat_amount = $tax_amount;
                    $row->_taxed_amount = $total_amount_after_discount + $tax_amount;
                    $row->_payment_type_charge = $payment_type_charge;
                    $row->_total_payable = $amount_payable;
                    /********************************************************** */
                    $id = Booking::insertGetId((array) $row);
                    $booking = Booking::find($id);
                    $booking_common_id = @$booking_common_id ?: $id;
                    if ($input['professionals_count'] > 1) {
                        // for act as single booking use common id
                        $booking->booking_common_id = $booking_common_id;
                    }
                    $booking->reference_id = Config::get('values.booking_ref_prefix') . $id;
                    $booking->save();
                    $bookings[] = $booking;
                }
                /********************************************* */
                // update booking addons (already inserted) with booking id
                BookingAddons::whereIn('id', $booking_addons_ids)->update(['booking_id' => $booking_common_id]);
                /********************************************* */
                if (@$packages) {
                    foreach ($packages as $key => $package) {
                        $booking_package = new BookingPackages();
                        $booking_package->booking_id = $bookings[0]->booking_id;
                        $booking_package->building_type_room_package_id = $package->id;
                        $booking_package->unit_price = $package->total_amount;
                        $booking_package->quantity = 1;
                        $booking_package->save();
                    }
                }
                /********************************************* */
                $response['status'] = 'success';
                $response['message'] = 'Booking saved with Ref. No. ' . $bookings[0]->reference_id . '.';
                $response['debug'] = array(
                    'want_cleaning_material' => $want_cleaning_material,
                    'service_rate_per_hour' => $price_per_hr,
                    'cleaning_material_rate_per_hour' => $cleaning_material_rate_per_hr,
                    'service_hours' => $input['hours'],
                    'service_rate_before_discount' => $total_amount_before_discount,
                    'coupon_id' => @$input['promocode_id'],
                    'coupon_applied' => $coupon_discount > 0 ? true : false,
                    "coupon_discount" => $coupon_discount,
                    "frequency_discount" => $frequency_discount,
                    "total_discount" => $total_discount,
                    'service_rate_after_discount' => $total_amount_after_discount,
                    'tax_amount' => $tax_amount,
                    'amount_with_tax' => $total_amount_after_discount + $tax_amount,
                    'payment_type_charge' => $payment_type_charge,
                    'amount_payable' => $amount_payable,
                );
                $response['payment_details'] = array(
                    // based on no. of profs
                    'service_fee' => (float) number_format($service_fee_total * $input['professionals_count'], 2, ".", ""),
                    "cleaning_materials" => (float) number_format($cleaning_material_fee_total * $input['professionals_count'], 2, ".", ""),
                    "addons_amount" => (float) number_format($booking_addons_amount_per_maid * $input['professionals_count'], 2, ".", ""),
                    "rush_slot_extra_charge" => (float) number_format($rush_slot_extra_charge * $input['professionals_count'], 2, ".", ""),
                    "discount" => (float) number_format($total_discount * $input['professionals_count'], 2, ".", ""),
                    "taxable_amount" => (float) number_format($total_amount_after_discount * $input['professionals_count'], 2, ".", ""),
                    "tax_amount" => (float) number_format($tax_amount * $input['professionals_count'], 2, ".", ""),
                    "payment_type_charge" => (float) number_format($payment_type_charge * $input['professionals_count'], 2, ".", ""),
                    "total" => (float) number_format($amount_payable * $input['professionals_count'], 2, ".", ""),
                );
                $response['No_weeks'] = $input['No_weeks'];
                $response['Cards'] = [];
                $response['booking_details'] = array(
                    "booking_id" => $bookings[0]->booking_id,
                    "booking_reference_id" => $bookings[0]->reference_id,
                    "total_hours" => $input['hours'],
                    "professionals_count" => sizeof($selected_professionals),
                    "booking_ref" => $bookings[0]->reference_id,
                    "payment_method" => $payment_type->name,
                    "status" => $input['PaymentMethod'] == 1 ? "Pending Approval" : "Payment Pending",
                );
                $response['debug_input'] = $input;
                /********************************************************************************************* */
                //$response['checkout_data'] = [];
                //$response['checkout_data']['payfort_url'] = Config::get('values.payfort_payment_page_url');
                $requestParams = [
                    'command' => 'PURCHASE',
                    'access_code' => Config::get('values.payfort_access_code'),
                    'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
                    'amount' => (float) $response['payment_details']['total'] * 100,
                    'currency' => Config::get('values.currency_code'),
                    'language' => strtolower(Config::get('values.language_code')),
                    'customer_email' => Customer::where('customer_id', $booking->customer_id)->first()->email_address ?: 'test@example.com',
                    'order_description' => $bookings[0]->reference_id,
                    'merchant_reference' => $bookings[0]->reference_id,
                    'merchant_extra' => $bookings[0]->reference_id,
                    //'eci' => 'RECURRING',
                    //'token_name' => "70c9101526bf42a599b0e0189b57779e",
                    'return_url' => url('api/customer/payment/payfort/processing'),
                ];
                ksort($requestParams);
                $hash_value = Config::get('values.payfort_sha_request_phrase');
                foreach ($requestParams as $name => $value) {
                    if ($value != null) {
                        $hash_value .= $name . '=' . $value;
                    }
                }
                $hash_value .= Config::get('values.payfort_sha_request_phrase');
                $requestParams['signature'] = hash('sha256', $hash_value);
                //$response['checkout_data']['payfort_data'] = $requestParams;
                $mail = new AdminApiMailController;
                /********************************************************************************************* */
                if ($payment_type->id == "1") {
                    // cash mode
                    $notify = new stdClass();
                    $notify->customer_id = $input['id'];
                    $notify->booking_id = $bookings[0]->booking_id;
                    $notify->service_date = $input['frequency'] == "OD" ? $input['date'] : null;
                    if (@$packages) {
                        $notify->content = "Booking (Package) is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                    } else {
                        $notify->content = "Booking is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                    }
                    addCustomerNotification($notify);
                    $mail->booking_confirmation_to_admin($booking_common_id);
                    $mail->booking_confirmation_to_customer($booking_common_id);
                } else if ($payment_type->id == "2") {
                    initiate_online_payment_for_booking($booking_common_id, null, $response['payment_details']['total'] - $response['payment_details']['payment_type_charge'], $response['payment_details']['payment_type_charge']);
                } else if ($payment_type->id == "3") {
                    initiate_online_payment_for_booking($booking_common_id, null, $response['payment_details']['total'] - $response['payment_details']['payment_type_charge'], $response['payment_details']['payment_type_charge']);
                }
                /********************************************************************************************* */
                // send booking confirmation mail to admin
                /*try {
                    $customer = Customer::where('customer_id', $input['id'])->first();
                    $booking = Booking::where('booking_id', $bookings[0]->booking_id)->first();
                    $frequency = Frequencies::where('code', $input['frequency'])->first();
                    // send booking mail to admin
                    Mail::send(
                        'emails.to_admin.booking_confirmation_to_admin',
                        [
                            'input' => $input,
                            'customer' => $customer,
                            'booking' => $booking,
                            'frequency' => $frequency,
                            'customer_address' => $customer_address,
                            'payment_type' => $payment_type,
                            'offer' => @$offer,
                            'packages' => @$packages,
                            'amount' => array(
                                'total' => $total_amount_after_discount * $input['professionals_count'],
                                'tax_amount' => $tax_amount * $input['professionals_count'],
                                'payment_type_charge' => $payment_type_charge * $input['professionals_count'],
                                'total_payable' => $amount_payable * $input['professionals_count'],
                            ),
                        ],
                        function ($m) use ($debug) {
                            $m->from(env('MAIL_USERNAME'), 'Spectrum Admin');
                            if (strpos($_SERVER['REQUEST_URI'], 'live') !== false) { // LIVE
                                $m->to(env('MAIL_USERNAME'), 'Admin')
                                    ->bcc('samnad.s@azinova.info', 'Samnad S')
                                    //->bcc('revathy.l@azinova.info', 'Revathy L')
                                    ->subject('Spectrum Booking Confirmation');
                            } else { // DEMO
                                $m->to('samnad.s@azinova.info', 'Samnad S')
                                    ->cc('revathy.l@azinova.info', 'Revathy L')
                                    ->subject('Spectrum Booking Confirmation - DEMO');
                            }
                        }
                    );
                } catch (\Exception $e) {
                    //return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }*/
                /********************************************************************************************* */
                DB::commit();
                return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
