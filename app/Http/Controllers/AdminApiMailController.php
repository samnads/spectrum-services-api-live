<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use App\Models\Settings;
use stdClass;

class AdminApiMailController extends Controller
{
    public function booking_confirmation_to_admin($booking_id)
    {
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $booking_addons = DB::table('booking_addons as ba')
                ->select(
                    'ba.service_addons_id',
                    DB::raw('IFNULL(ba.service_addon_name,sao.service_addon_name) as service_addon_name'),
                    'sao.service_addon_description',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'ba.service_minutes',
                    'ba.strike_amount',
                    'ba.strike_amount',
                    'ba.unit_price',
                    'ba.quantity',
                    'ba.amount',
                    DB::raw('CONCAT("' . Config::get('values.file_server_url') . 'customer-app-assets/service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v=' . $settings->customer_app_img_version . '") as image_url'),
                )
                ->leftJoin('service_addons as sao', 'ba.service_addons_id', 'sao.service_addons_id')
                ->where([['ba.booking_id', "=", $booking_id]])
                ->orderBy('sao.populariry', 'DESC')
                ->get()->toArray();
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            Mail::send(
                'emails.booking-confirmation-to-admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Dear Admin',
                    'text_body' => 'You have got a new booking with Ref. No. <b>' . $booking->reference_id . '</b> from ' . $customer_address->area_name . ' area.',
                    'booking' => $booking,
                    'bookings' => $bookings,
                    'booking_addons' => $booking_addons,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'test' => isset($_GET['test'])
                ],
                function ($m) use ($booking) {
                    $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                    $m->to(Config::get('mail.mail_admin'), Config::get('mail.mail_admin_name'));
                    $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                    $m->subject('New Booking Received ' . $booking->reference_id);
                }
            );
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function booking_confirmation_to_customer($booking_id)
    {
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $booking_addons = DB::table('booking_addons as ba')
                ->select(
                    'ba.service_addons_id',
                    DB::raw('IFNULL(ba.service_addon_name,sao.service_addon_name) as service_addon_name'),
                    'sao.service_addon_description',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'ba.service_minutes',
                    'ba.strike_amount',
                    'ba.strike_amount',
                    'ba.unit_price',
                    'ba.quantity',
                    'ba.amount',
                    DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v=' . $settings->customer_app_img_version . '") as image_url'),
                )
                ->leftJoin('service_addons as sao', 'ba.service_addons_id', 'sao.service_addons_id')
                ->where([['ba.booking_id', "=", $booking_id]])
                ->orderBy('sao.populariry', 'DESC')
                ->get()->toArray();
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.booking-confirmation-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your booking with Ref. No. <b>' . $booking->reference_id . '</b> is received, we\'ll contact you soon for confirmation.',
                        'booking' => $booking,
                        'bookings' => $bookings,
                        'booking_addons' => $booking_addons,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($booking, $customer) {
                        $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Booking Received ' . $booking->reference_id);
                    }
                );
            }
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function registration_success_to_customer($customer_id)
    {
        /**
         * 
         * send registration confirmation mail to customer
         * 
         */
        try {
            $settings = Settings::first();
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.registration-success-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Thank you for your registration, you\'ve successfully registered on ' . Config::get('values.company_name') . '.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Registration Confirmation');
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function online_payment_confirmation_to_customer($payment_id)
    {
        /**
         * 
         * send online payment confirmation mail to customer
         * 
         */
        try {
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            $customer = Customer::find($online_payment->customer_id);
            if ($customer->email_address != '' && $online_payment->payment_status == 'success') {
                $total_payable = number_format($online_payment->amount + $online_payment->transaction_charge, 2, ".", ",");
                $text_body = 'Your online payment of <b>AED ' . $total_payable . '</b> to <i>' . Config::get('values.company_name') . '</i> is confirmed.';
                Mail::send(
                    'emails.online-payment-confirmation-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => $text_body,
                        'online_payment' => $online_payment,
                        'customer' => $customer,
                        'total_payable' => $total_payable,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $online_payment) {
                        $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Payment Confirmation - #' . $online_payment->transaction_id);
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function online_payment_confirmation_to_admin($payment_id)
    {
        /**
         * 
         * send online payment confirmation mail to admin
         * 
         */
        try {
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            /******************************************************************************************* */
            $customer = Customer::find($online_payment->customer_id);
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
                ->where(function ($query) use ($customer) {
                    $query->where([['customer_addresses.customer_id', '=', $customer->customer_id], ['customer_addresses.default_address', '=', '1']]);
                })
                ->first();
            /******************************************************************************************* */
            if ($online_payment->payment_status == 'success') {
                $total_payable = number_format($online_payment->amount + $online_payment->transaction_charge, 2, ".", ",");
                $text_body = 'New online payment of <b>AED ' . $total_payable . '</b> received.';
                Mail::send(
                    'emails.online-payment-confirmation-to-admin',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear Admin',
                        'text_body' => $text_body,
                        'online_payment' => $online_payment,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'total_payable' => $total_payable,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($online_payment) {
                        $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                        //$m->to(Config::get('mail.mail_admin'), Config::get('mail.mail_admin_name'));
                        $m->to(Config::get('mail.to_mails_for_online_payment')); // array of mail list
                        if (Config::get('mail.bcc_mails_for_online_payment')) {
                            $m->bcc(Config::get('mail.bcc_mails_for_online_payment')); // array of mail list
                        }
                        $m->subject('Payment Received - #' . $online_payment->transaction_id);
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function test()
    {
        dd(Config::get('mail.bcc_mails_for_online_payment'));
    }
}
