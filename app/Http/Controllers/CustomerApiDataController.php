<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use stdClass;

class CustomerApiDataController extends Controller
{
    public function data()
    {
        $debug = toggleDebug(); // pass boolean to overide default
        $response['banner_images'] = array(
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_01.jpg",
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_02.jpg",
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_03.jpg",
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_04.jpg",
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_05.jpg",
            Config::get('values.file_server_url') . "uploads/images/app-banners/Banner_06.jpg",
        );
        $response['insurance_info'] = array(
            "Being a customer-focused firm, at Spectrum, we pay utmost importance to maintaining the trust of our customers. Our commitment to excellence facilitated us to provide consistent services by paying attention to detail.",
            "We always put our customers in mind and deliver what they want. For this reason, we've initiated the Spectrum Customer Insurance Plan, a damage protection insurance plan focused on customer safety. With the Spectrum Customer Insurance Plan, our users will be eligible to receive up to a maximum of AED 1000 insurance against any damage generated during our service.",
            "To avail of Spectrum Customer Insurance Plan, all you have to do is to pay online for your service. Be assured that we've got you covered in the case of an unlikely event that causes any damage from our service. The plan is not applicable for cash payments.",
        );
        $response['payment_types'] = DB::table('payment_types as pt')
            ->select(
                'pt.id',
                'pt.name',
                'pt.code',
                'pt.charge',
                'pt.charge_type',
                'pt.show_in_web'
            )
            ->where(['deleted_at' => null])
            ->get();
        $response['areas'] = DB::table('areas as a')
            ->select(
                'a.area_id as id',
                'a.area_name as name',
                //DB::raw('CONCAT(a.area_name," [",z.zone_name,"]") as name')
            )
            ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
            ->where(['a.area_status' => 1])
            ->orderBy('a.area_name', 'ASC')
            ->orderBy('z.zone_name', 'ASC')
            ->get();
        $response['ip'] = @$_SERVER['SERVER_ADDR'];
        $response['protocol'] = @$_SERVER['SERVER_PROTOCOL'];
        $response['otp_expire_seconds'] = Config::get('values.customer_login_otp_expire_seconds');
        $response['message'] = 'Data fetched successfully.';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
