<?php
function get_available_maids_for_move(){
		// in development mode
		header('Content-type: application/json; charset=UTF-8');
		try {
			//$time_from = '06:00:00';
			//$time_to = '07:30:00';
			//$_POST['booking_id'] = 3506;
			//$_POST['service_date'] = '26-05-2023';
			$booking = $this->bookings_model->get_booking_by_id($this->input->post('booking_id'));
			//print_r($booking);die();
			$timed_free_maids = array();
			if($booking->booking_type == "OD"){
				// ONE DAY
				$service_date = DateTime::createFromFormat('d-m-Y', $this->input->post('service_date'))->format('Y-m-d'); // input format is d-m-Y
				$time_from = $booking->time_from;
				$time_to = $booking->time_to;
				$timed_bookings = $this->maids_model->get_ongoing_bookings_on_time($service_date, $time_from, $time_to, true); // check on one date
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			}
			else if($booking->booking_type == "WE"){
				// WEEKLY
				$service_start_date = $booking->service_start_date;
				$service_end_date = $booking->service_end_date;
				$service_end = $booking->service_end;
				$service_week_day = $booking->service_week_day;
				$timed_bookings = $this->maids_model->get_same_timed_bookings($booking); // get bookings by same creteria of the booking
				//print_r($timed_bookings);die();
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			}
			else if($booking->booking_type == "BW"){
				// BI-WEEKLY
				$service_start_date = $booking->service_start_date;
				$service_end_date = $booking->service_end_date;
				$service_end = $booking->service_end;
				$service_week_day = $booking->service_week_day;
				$timed_bookings = $this->maids_model->get_same_timed_bookings($booking); // get bookings by same creteria of the selected booking
				// trick to exclude some bw bookings
				foreach($timed_bookings as $key => $value) {
					if($value->service_start_date_week_difference & 1){ // if odd number
						// week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
						unset($timed_bookings[$key]);
					}
				}
				//
				//print_r($timed_bookings);die();
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			}
			else{
				die (json_encode(array('status' => false, 'message' => 'Unknown booking type !'), JSON_PRETTY_PRINT));
			}
			//echo('<pre>');print_r($timed_free_maids);echo('<pre>');die();
		} catch (Exception $e) {
			die (json_encode(array('status' => false, 'message' => $e->getMessage()), JSON_PRETTY_PRINT));
		}
		die (json_encode(array('status' => true, 'data' => $timed_free_maids, 'message' => 'Free maids retrieved successfully !'), JSON_PRETTY_PRINT));
	}