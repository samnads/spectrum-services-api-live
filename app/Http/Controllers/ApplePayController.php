<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Mail;
use Response;
use Illuminate\Support\Facades\DB;
use App\Models\Booking;
use App\Models\Customer;

class ApplePayController extends Controller
{
    public function payfort_apple_test(Request $request)
    {
        /****************************************************************************** */
        $js_string = '{\"shippingContact\":{\"addressLines\":[\"Uae\",\"uae\"],\"administrativeArea\":\"\",\"country\":\"United Arab Emirates\",\"countryCode\":\"AE\",\"familyName\":\"L\",\"givenName\":\"Revathy\",\"locality\":\"UAE \",\"postalCode\":\"\",\"subAdministrativeArea\":\"\",\"subLocality\":\"silicon oasis\"},\"token\":{\"paymentData\":{\"data\":\"gonaVd8cqRSnbBIlrJekEYAbnaGHpqbVRvBf+YWVaQjltGLki9D4HWcYWbmLg5XzjLZLYULelMFn0teYRhnAzrZ3KxB7WEgurYLkVgIu73i9JdRSr0r4Y\/eccjaY8y7miIQb7VmLlGmzN5ot9FeMFgKaeN5q24Gx+yV0a\/maSboQ7ZPjiDgcNR52XvlyTkhGZbxDB4y5ec\/DN7VEyyRjiAnpjYAd7I0ox4DwEKhGBhu0C581G4rxw3oNFFDnAWxWYBh6gk0CDEfNLM65etypcqdowL7vI5tYKeECCjLKj8HstzQjwEnMNGPf2hbsvuvK5yzHn0Qah1jeeVnsEyzgyXeIS8lRJDcREoqCIRVpOMYMS0mrptxylsN3HpgOWswlR9N2hL6e\/G5\/PNAl3coi2nA9Q8WyqIyde7JsOYOZrQ==\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7\/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB\/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI\/JJxE+T5O8n5sT2KGw\/orv9LkswDwYDVR0TAQH\/BAUwAwEB\/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv\/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn\/Rd8LCFtlk\/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYgwggGEAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTI0MDIyMTExMDMwMFowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEIPOwPCxEtQ9xHIB8GX7otymdFDJqdRzvftb9FfpU5o0KMAoGCCqGSM49BAMCBEcwRQIhAP0aOFB2O1HdpwgRI3XyYbbaEX5mFnY74a6w6WG5jSguAiBHEkt3ZXPbO+3hOvH7z10LI\/UNcYp8Z\/fd83X1lPNkKQAAAAAAAA==\",\"header\":{\"publicKeyHash\":\"v2EEnjeU6CF929+j7grL2z5v\/o7lPnOhj8PrxnRlmCg=\",\"ephemeralPublicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEkCZczj+4eqJBDAOYyozrH\/UhnXnmTBqJbhEEKly3nbk44CY8hs6fDT3kKCKhkWJeK3zaaQ\/rt\/1uMkEH2SB9tQ==\",\"transactionId\":\"dd8d506f7b1ef1956ed0508ec712869faa5a536ab3f176e0d00dc464a59ca64d\"},\"version\":\"EC_v1\"},\"paymentMethod\":{\"displayName\":\"Visa 1920\",\"network\":\"Visa\",\"type\":\"debit\"},\"transactionIdentifier\":\"DD8D506F7B1EF1956ED0508EC712869FAA5A536AB3F176E0D00DC464A59CA64D\"}}';
        $js_string = stripslashes($js_string);
        /****************************************************************************** */
        $js_data = json_decode($js_string, true);
        $token_data = $js_data['token'];
        /****************************************************************************** */
        $payFortData = [
            'digital_wallet' => 'APPLE_PAY',
            'command' => 'PURCHASE',
            'access_code' => Config::get('values.PAYFORT_APPLE_PAY_ACCESS_CODE'),
            'merchant_identifier' => Config::get('values.PAYFORT_APPLE_PAY_MERCHANT_IDENTIFIER'),
            'merchant_reference' => 'Test-' . rand(10, 100) . rand(10, 100),
            'amount' => '1', // 100 means 1 AED
            'currency' => 'AED',
            'language' => 'en',
            'customer_name' => 'Samnad S',
            'customer_email' => 'samnad.s@azinova.info',
            'apple_data' => $token_data['paymentData']['data'],
            'apple_signature' => $token_data['paymentData']['signature'],
            'apple_header' => [
                'apple_ephemeralPublicKey' => $token_data['paymentData']['header']['ephemeralPublicKey'],
                'apple_publicKeyHash' => $token_data['paymentData']['header']['publicKeyHash'],
                'apple_transactionId' => $token_data['paymentData']['header']['transactionId'],
            ],
            'apple_paymentMethod' => [
                'apple_displayName' => $token_data['paymentMethod']['displayName'],
                'apple_network' => $token_data['paymentMethod']['network'],
                'apple_type' => $token_data['paymentMethod']['type'],
            ],
            'customer_ip' => @$_SERVER['REMOTE_ADDR'],
            'signature' => null, // will update later
        ];
        /****************************************************************************** */
        ksort($payFortData);
        $payFortData['signature'] = $this->payfortCalculateSignature($payFortData); // update signature
        echo "<fieldset style='padding:1em;margin:1em;background:#f6fff6;'><legend><b>Payfort API Input</b></legend><pre>";
        echo json_encode($payFortData, JSON_PRETTY_PRINT);
        echo "</pre></fieldset>";
        /****************************************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $post_header = ["Content-Type" => "application/json"];
        $response_token_data = $client->request('POST', Config::get('values.PAYFORT_APPLE_PAY_API_URL'), [
            'headers' => $post_header, // no exception fix
            'http_errors' => false,
            'json' => $payFortData,
        ]);
        $token_data = json_decode((string) $response_token_data->getBody(), true);
        /****************************************************************************** */
        echo "<fieldset style='padding:1em;margin:1em;background:#fff6f6;'><legend><b>Payfort API Response</b></legend><pre>";
        echo json_encode($token_data, JSON_PRETTY_PRINT);
        echo "</pre></fieldset>";
        //dd($token_data['response_message']);
        $token_data = json_decode('{
    "amount": "1",
    "response_code": "14000",
    "card_number": "427409352*******",
    "digital_wallet": "APPLE_PAY",
    "signature": "1c2b89db2e82ad27750dee16611593fdab12fce28ed73531606488e7f36c7647",
    "merchant_identifier": "6a190585",
    "access_code": "snMGuc6LeZ7SYFHPeAbg",
    "payment_option": "VISA",
    "expiry_date": "3007",
    "customer_ip": "162.0.222.84",
    "language": "en",
    "eci": "ECOMMERCE",
    "fort_id": "169996200014432569",
    "command": "PURCHASE",
    "response_message": "Success",
    "merchant_reference": "Test-9864",
    "authorization_code": "831000",
    "customer_email": "samnad.s@azinova.info",
    "reconciliation_reference": "405209161358",
    "currency": "AED",
    "customer_name": "Samnad S",
    "acquirer_response_code": "00",
    "status": "14"
}', true);
        after_payfort_apple_pay_success($token_data, 359482, 'web');
    }
    public function payfort_apple_pay($apple_data, $booking_id, $amount)
    {
        //$amount = 1; // test
        /****************************************************************************** */
        $apple_data = stripslashes($apple_data);
        /****************************************************************************** */
        $js_data = json_decode($apple_data, true);
        $token_data = $js_data['token'];
        /****************************************************************************** */
        $booking = Booking::find($booking_id);
        $customer = Customer::find($booking->customer_id);
        /****************************************************************************** */
        $payFortData = [
            'digital_wallet' => 'APPLE_PAY',
            'command' => 'PURCHASE',
            'access_code' => Config::get('values.PAYFORT_APPLE_PAY_ACCESS_CODE'),
            'merchant_identifier' => Config::get('values.PAYFORT_APPLE_PAY_MERCHANT_IDENTIFIER'),
            'merchant_reference' => $booking->reference_id ?: $booking->booking_id,
            'amount' => ((float) $amount) * 100,
            'currency' => 'AED',
            'language' => 'en',
            'customer_name' => preg_replace('/[^A-Za-z0-9\-]/', '', $customer->customer_name),
            'customer_email' => $customer->email_address,
            'apple_data' => $token_data['paymentData']['data'],
            'apple_signature' => $token_data['paymentData']['signature'],
            'apple_header' => [
                'apple_ephemeralPublicKey' => $token_data['paymentData']['header']['ephemeralPublicKey'],
                'apple_publicKeyHash' => $token_data['paymentData']['header']['publicKeyHash'],
                'apple_transactionId' => $token_data['paymentData']['header']['transactionId'],
            ],
            'apple_paymentMethod' => [
                'apple_displayName' => $token_data['paymentMethod']['displayName'],
                'apple_network' => $token_data['paymentMethod']['network'],
                'apple_type' => $token_data['paymentMethod']['type'],
            ],
            'customer_ip' => @$_SERVER['REMOTE_ADDR'],
            'signature' => null, // will update later
        ];
        /****************************************************************************** */
        ksort($payFortData);
        $payFortData['signature'] = $this->payfortCalculateSignature($payFortData); // update signature
        /****************************************************************************** */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $post_header = ["Content-Type" => "application/json"];
        $response_token_data = $client->request('POST', Config::get('values.PAYFORT_APPLE_PAY_API_URL'), [
            'headers' => $post_header, // no exception fix
            'http_errors' => false,
            'json' => $payFortData,
        ]);
        $token_data = json_decode((string) $response_token_data->getBody(), true);
        /****************************************************************************** */
        if (@$token_data['response_message'] == "Success") {
            return $token_data;
        } else {
            throw new \ErrorException('Payfort Exception - ' . @$token_data['response_message']);
        }
    }

    public function payfort_process_apple_pay_test(Request $request)
    {
        try {
            /****************************************************************************** */
            $data = json_decode('{"params":{"paymentToken":"{\"paymentData\":{\"version\":\"EC_v1\",\"data\":\"3pvnnkmMegc+LLUJeqJz4VxOhjh70Y1HToBFc4En\/nr5TVKPb1x0N\/bCWqX8bSBIOlUM4ojmx+AifqFSI8O3OSLmFtzl7YMWkv7VpPPUQjpsbFDTtU010flB5Q9W0MsAShSogUEYDjk9KeoEvmCUX2voaUPEvWqBQWT0L\/DFI20qWzxeZHFTLi8YKhVWHhNDTjjeXhpCJaso\/iQ61HSe8P9Zj4860vY06yXmYDWGws3+KKQ9Iwy3W09c401EyPa7PWNLJMrglrzuWTVtiMyDmZAZm\/REW+zErOUp4R1kEkRikCObymF6PTI7cl0Hd3OqWy5hZVh\/2AwJc2FPcQUvc6eN9yZoPbF5QYxAKTxc0zDtUWHYxQrKQx9VDXvz10W\/NAs8uMU+8VdvCa2Jac6vhehQiHY0BHgNqf4SRJpOxA==\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7\/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB\/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI\/JJxE+T5O8n5sT2KGw\/orv9LkswDwYDVR0TAQH\/BAUwAwEB\/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv\/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn\/Rd8LCFtlk\/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYkwggGFAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTI0MDIyMjA5MzgwNlowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEIMmEWbajUyMiBGWWVqWRpuXfu3pFHtLFJWbnhr8HAJjPMAoGCCqGSM49BAMCBEgwRgIhALxZoSA7d0xfhjC1UWtAI\/u1OGHRB2+7gZUYNvI0LyNoAiEA\/N+4QXb5zPWhLXTLs\/orifwPaGW4+w7VXJBY3GV5hEcAAAAAAAA=\",\"header\":{\"ephemeralPublicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEEXwFohhkfBCF\/u5yimetn1ydPCjA+B4P7EJ2epWSijrnTwfiVRc1rUvzDGxNGGyR\/oPmfvwDKjtqaZ1ZVJL8fA==\",\"publicKeyHash\":\"v2EEnjeU6CF929+j7grL2z5v\/o7lPnOhj8PrxnRlmCg=\",\"transactionId\":\"930173783fac6d4b536468e21af9313ccfd63ef99f12a229b8860fcb2757845a\"}},\"paymentMethod\":{\"network\":\"Visa\",\"displayName\":\"Visa 1920\",\"type\":\"debit\"},\"transactionIdentifier\":\"930173783FAC6D4B536468E21AF9313CCFD63EF99F12A229B8860FCB2757845A\"}","payment_type_id":3,"platform":"mobile","amount":1,"booking_id":"359496","reference_id":"SS-359391"}}', true);
            $input = $data['params'];
            $paymentToken = $input['paymentToken'];
            $paymentToken = stripslashes($paymentToken);
            $apple_data['token'] = json_decode($paymentToken, true);
            $payfort_data = $this->payfort_apple_pay(json_encode($apple_data), $input['booking_id'], 1);
            /****************************************************************************** */
            if (@$payfort_data['fort_id']) {
                // payment success
            }
            $booking = Booking::find($input['booking_id']);
            $response['status'] = 'success';
            $response['message'] = 'Payment verified successfully';
            $booking_details = [
                'reference_id' => $booking->reference_id ?: $booking->booking_id,
                'booking_id' => $booking->booking_id,
                'payment_method' => $booking->pay_by,
                'status' => 'Confirmed',
            ];
            $response['booking_details'] = $booking_details;
            $response['payfort_data'] = @$payfort_data;
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        }
    }
    public function payfort_process_apple_pay(Request $request)
    {
        try {
            /****************************************************************************** */
            $data = json_decode($request->getContent(), true);
            $input = $data['params'];
            $paymentToken = $input['paymentToken'];
            $paymentToken = stripslashes($paymentToken);
            $apple_data['token'] = json_decode($paymentToken, true);
            /****************************************************************************** */
            Mail::send(
                'emails.debug_mail',
                [
                    'input' => json_encode($data)
                ],
                function ($m) {
                    $m->from(Config::get('mail.mail_username'), Config::get('mail.mail_admin_name'));
                    $m->to('samnad.s@azinova.info', 'Samnad S - Developer');
                    $m->subject('Spectrum IOS - payfort-process-apple-pay DEBUG');
                }
            );
            /****************************************************************************** */
            $booking = Booking::findOrFail($input['booking_id']);
            if ($booking->customer_id == '87812') {
                $input['amount'] = 1;
            }
            $payfort_data = $this->payfort_apple_pay(json_encode($apple_data), $input['booking_id'], $input['amount']);
            if (@$payfort_data['response_message'] == "Success") {
                // payment success
                after_payfort_apple_pay_success($payfort_data, $input['booking_id'], 'mobile');
            }
            $response['status'] = 'success';
            $response['message'] = 'Payment verified successfully';
            $booking_details = [
                'reference_id' => $booking->reference_id ?: $booking->booking_id,
                'booking_id' => $booking->booking_id,
                'payment_method' => $booking->pay_by,
                'status' => 'Confirmed',
            ];
            $response['booking_details'] = $booking_details;
            $response['payfort_data'] = @$payfort_data;
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        }
    }
    public function payfortCalculateSignature(array $arrData, $signType = 'request')
    {
        $shaString = '';
        foreach ($arrData as $key => $val) {
            if ($key === 'signature') {
                continue;
            }
            if (gettype($val) !== 'array') {
                $shaString .= "$key=$val";
            } else {
                $sub_str = $key . '={';
                $index = 0;
                // ksort($val);
                foreach ($val as $k => $v) {
                    $sub_str .= "$k=$v";
                    if ($index < count($val) - 1) {
                        $sub_str .= ', ';
                    }
                    $index++;
                }
                $sub_str .= '}';
                $shaString .= $sub_str;
            }
        }
        if ($signType == 'request') {
            $shaString = Config::get('values.PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE') . $shaString . Config::get('values.PAYFORT_APPLE_PAY_SHA_REQUEST_PHRASE');
        } else {
            $shaString = Config::get('values.PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE') . $shaString . Config::get('values.PAYFORT_APPLE_PAY_SHA_RESPONSE_PHRASE');
        }
        $signature = hash('sha256', $shaString);
        return $signature;
    }
}
