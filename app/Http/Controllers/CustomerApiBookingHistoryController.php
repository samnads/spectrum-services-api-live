<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingDeleteRemarks;
use App\Models\BookingAddons;
use Carbon\Carbon;
use Config;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingHistoryController extends Controller
{
    public function booking_history(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_type'] = "waiting";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'id' => 'required|integer',
                'booking_type' => 'required|in:waiting,ongoing,done,cancelled',
            ],
            [],
            [
                'id' => 'ID',
                'booking_type' => 'Booking Type',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $response['status'] = 'success';
        if ($input['booking_type'] == 'ongoing') {
            // based on day services
            $response['completed_list'] = DB::table('day_services as ds')
                ->select(
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    DB::raw('(CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END) as booking_reference'),
                    'ds.day_service_id as schedule_reference',
                    DB::raw('"service_details" as service_details'),
                    'ds.service_date as date',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(b.time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.customer_id as customer_id',
                    'b.customer_address_id as address_id',
                    DB::raw('CONCAT(ca.customer_address," ~ ",a.area_name) as address'),
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(ds.start_time,"%H:%i") as start_time'),
                )
                ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                ->where([['b.customer_id', '=', $input['id']], ['ds.service_status', '=', 1]])
                ->orderBy('ds.service_date', 'DESC')
                ->orderBy('ds.start_time', 'DESC');
            $response['completed_list']->where(function ($query) {
                $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
                $query->orWhereNull('b.booking_common_id');
            });
            $response['completed_list'] = $response['completed_list']->get();
            $booking_addons = BookingAddons::select('id', 'booking_id', 'service_addon_name')->whereIn('booking_id', array_column($response['completed_list']->toArray(), 'booking_id'))->get()->toArray();
            foreach ($response['completed_list'] as $key => $row) {
                /*********************************************************************************** */
                // include booked addons
                $response['completed_list'][$key]->addons = array_filter($booking_addons, function ($booking_addon) use ($row) {
                    return $booking_addon['booking_id'] == $row->booking_common_id;
                });
                /*********************************************************************************** */
                $professional_count = 1;
                if ($row->booking_common_id) {
                    $bookings = DB::table('bookings as b')
                        ->select(
                            'b.*',
                            'm.*',
                        )
                        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                        ->where('booking_common_id', $row->booking_common_id)->get();
                    $professional_count = sizeof($bookings);
                    foreach ($bookings as $index => $booking) {
                        $crew = new stdClass();
                        $crew->professional = $booking->maid_name;
                        $crew->professional_id = $booking->maid_id;
                        $crew->verified = true;
                        $response['completed_list'][$key]->crew[] = $crew;
                    }
                } else {
                    $crew = new stdClass();
                    $crew->professional = $response['completed_list'][$key]->maid_name;
                    $crew->professional_id = $response['completed_list'][$key]->maid_id;
                    $crew->verified = true;
                    $response['completed_list'][$key]->crew[] = $crew;
                }
                /*************************************************************** */
                // rating DUMMy data goes here
                $response['completed_list'][$key]->rating = new stdClass();
                $response['completed_list'][$key]->rating->reference = null;
                $response['completed_list'][$key]->rating->date = null;
                $response['completed_list'][$key]->rating->service_rating = null;
                $response['completed_list'][$key]->rating->maid_rating = null;
                $response['completed_list'][$key]->rating->comment = null;
                //
                $response['completed_list'][$key]->total = $row->total * $professional_count;
                $response['completed_list'][$key]->status = "Running...";
                $response['completed_list'][$key]->service_details = $response['completed_list'][$key]->hours . " hours, " . $professional_count . " Professional " . ($response['completed_list'][$key]->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                unset($response['completed_list'][$key]->maid_id);
                unset($response['completed_list'][$key]->maid_name);
            }
            $response['message'] = sizeof($response['completed_list']) ? "Ongoing schedules fetched successfully." : "No ongoing schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else if ($input['booking_type'] == 'done') {
            // based on day services
            $response['completed_list'] = DB::table('day_services as ds')
                ->select(
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    DB::raw('(CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END) as booking_reference'),
                    'ds.day_service_id as schedule_reference',
                    DB::raw('"service_details" as service_details'),
                    'ds.service_date as date',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.customer_address_id as address_id',
                    DB::raw('CONCAT(ca.customer_address," ~ ",a.area_name) as address'),
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(ds.start_time,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(ds.end_time,"%H:%i") as end_time'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(ds.end_time,ds.start_time))),"%H:%i") as hours_worked'),
                    'mr.id as maid_rating_id',
                    DB::raw('DATE_FORMAT(mr.created_at,"%Y-%m-%d") as maid_rating_date'),
                    DB::raw('(CASE WHEN (ds.rating < 1 OR ds.rating = "") THEN null ELSE ds.rating END) as service_rating'),
                    DB::raw('(CASE WHEN ds.comments = ""  THEN null ELSE ds.comments END) as service_rating_comment'),
                    'mr.rating as maid_rating',
                )
                ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('maid_rating as mr', 'ds.day_service_id', 'mr.day_service_id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                ->where([['b.customer_id', '=', $input['id']], ['ds.service_status', '=', 2]])
                ->orderBy('ds.service_date', 'DESC')
                ->orderBy('ds.start_time', 'DESC');
            $response['completed_list']->where(function ($query) {
                $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
                $query->orWhereNull('b.booking_common_id');
            });
            $response['completed_list'] = $response['completed_list']->get();
            $booking_addons = BookingAddons::select('id', 'booking_id', 'service_addon_name')->whereIn('booking_id', array_column($response['completed_list']->toArray(), 'booking_id'))->get()->toArray();
            foreach ($response['completed_list'] as $key => $row) {
                /*********************************************************************************** */
                // include booked addons
                $response['completed_list'][$key]->addons = array_filter($booking_addons, function ($booking_addon) use ($row) {
                    return $booking_addon['booking_id'] == $row->booking_id;
                });
                /*********************************************************************************** */
                $professional_count = 1;
                if ($row->booking_common_id) {
                    $bookings = DB::table('bookings as b')
                        ->select(
                            'b.*',
                            'm.*',
                        )
                        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                        ->where('booking_common_id', $row->booking_common_id)->get();
                    $professional_count = sizeof($bookings);
                    foreach ($bookings as $index => $booking) {
                        $crew = new stdClass();
                        $crew->professional = $booking->maid_name;
                        $crew->professional_id = $booking->maid_id;
                        $crew->verified = true;
                        $response['completed_list'][$key]->crew[] = $crew;
                    }
                } else {
                    $crew = new stdClass();
                    $crew->professional = $response['completed_list'][$key]->maid_name;
                    $crew->professional_id = $response['completed_list'][$key]->maid_id;
                    $crew->verified = true;
                    $response['completed_list'][$key]->crew[] = $crew;
                }
                /*************************************************************** */
                // rating data goes here
                $response['completed_list'][$key]->rating = new stdClass();
                $response['completed_list'][$key]->rating->reference = $response['completed_list'][$key]->schedule_reference;
                $response['completed_list'][$key]->rating->date = $response['completed_list'][$key]->maid_rating_date;
                $response['completed_list'][$key]->rating->service_rating = $response['completed_list'][$key]->service_rating;
                $response['completed_list'][$key]->rating->maid_rating = $response['completed_list'][$key]->maid_rating;
                $response['completed_list'][$key]->rating->comment = $response['completed_list'][$key]->service_rating_comment;
                if ($response['completed_list'][$key]->service_rating == null) {
                    //$response['completed_list'][$key]->rating = new stdClass();
                    $response['completed_list'][$key]->rating->reference = null;
                }
                unset($response['completed_list'][$key]->maid_rating_id);
                unset($response['completed_list'][$key]->maid_rating_date);
                unset($response['completed_list'][$key]->service_rating);
                unset($response['completed_list'][$key]->maid_rating);
                unset($response['completed_list'][$key]->service_rating_comment);
                //
                $response['completed_list'][$key]->total = $row->total * $professional_count;
                $response['completed_list'][$key]->status = "Completed";
                $response['completed_list'][$key]->service_details = $response['completed_list'][$key]->hours . " hours, " . $professional_count . " Professional " . ($response['completed_list'][$key]->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                unset($response['completed_list'][$key]->maid_id);
                unset($response['completed_list'][$key]->maid_name);
            }
            $response['message'] = sizeof($response['completed_list']) ? "Completed schedules fetched successfully." : "No completed schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else if ($input['booking_type'] == 'cancelled') {
            // based on day services
            $response['completed_list'] = DB::table('booking_delete_remarks as bdr')
                ->select(
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    DB::raw('(CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END) as booking_reference'),
                    DB::raw('null as schedule_reference'),
                    DB::raw('"service_details" as service_details'),
                    //'bdr.service_date as date',
                    DB::raw('(CASE WHEN bdr.service_date IS NOT NULL THEN bdr.service_date ELSE DATE_FORMAT(bdr.deleted_date_time,"%Y-%m-%d") END) as date'),
                    DB::raw('bdr.deleted_date_time as cancelled_date'),
                    //DB::raw('DATE_FORMAT(bdr.deleted_date_time,"%Y-%m-%d") as cancelled_date'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.customer_address_id as address_id',
                    DB::raw('CONCAT(customer_address," ~ ",a.area_name) as address'),
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                )
                ->leftJoin('bookings as b', 'bdr.booking_id', 'b.booking_id')
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                ->where([['b.customer_id', '=', $input['id']]]);
            //->where([['bdr.service_date', '!=', null]]);
            $response['completed_list']->where(function ($query) {
                $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
                $query->orWhereNull('b.booking_common_id');
            });
            $response['completed_list'] = $response['completed_list']->orderBy('bdr.id', 'DESC')->get();
            $booking_addons = BookingAddons::select('id', 'booking_id', 'service_addon_name')->whereIn('booking_id', array_column($response['completed_list']->toArray(), 'booking_id'))->get()->toArray();
            foreach ($response['completed_list'] as $key => $row) {
                /*********************************************************************************** */
                // include booked addons
                $response['completed_list'][$key]->addons = array_filter($booking_addons, function ($booking_addon) use ($row) {
                    return $booking_addon['booking_id'] == $row->booking_id;
                });
                /*********************************************************************************** */
                $professional_count = 1;
                if ($row->booking_common_id) {
                    $bookings = Booking::where('booking_common_id', $row->booking_common_id)->leftJoin('maids as m', 'bookings.maid_id', 'm.maid_id')->get();
                    $professional_count = sizeof($bookings);
                    foreach ($bookings as $sel_booking) {
                        $professional = new stdClass();
                        $professional->professional = $sel_booking->maid_name ?: "[Not Assigned]";
                        $professional->professional_id = $sel_booking->maid_id;
                        $professional->verified = $sel_booking->maid_id || false;
                        $response['completed_list'][$key]->crew[] = $professional;
                    }
                } else {
                    $crew = new stdClass();
                    $crew->professional = $response['completed_list'][$key]->maid_name ?: "[Not Assigned]";
                    $crew->professional_id = $response['completed_list'][$key]->maid_id;
                    $crew->verified = $response['completed_list'][$key]->maid_id || false;
                    $response['completed_list'][$key]->crew[] = $crew;
                }
                // rating DUMMy data goes here
                $response['completed_list'][$key]->rating = new stdClass();
                $response['completed_list'][$key]->rating->reference = null;
                $response['completed_list'][$key]->rating->date = null;
                $response['completed_list'][$key]->rating->service_rating = null;
                $response['completed_list'][$key]->rating->maid_rating = null;
                $response['completed_list'][$key]->rating->comment = null;
                //
                $response['completed_list'][$key]->total = $row->total * $professional_count;
                $response['completed_list'][$key]->status = "Cancelled";
                $response['completed_list'][$key]->service_details = $response['completed_list'][$key]->hours . " hours, " . $professional_count . " Professional " . ($response['completed_list'][$key]->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                unset($response['completed_list'][$key]->maid_id);
                unset($response['completed_list'][$key]->maid_name);
            }
            $response['message'] = sizeof($response['completed_list']) ? "Cancelled schedules fetched successfully." : "No cancelled schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else if ($input['booking_type'] == 'waiting') {
            // based on day services
            $upcoming_booking_ids = DB::table('bookings as b')
                ->select(
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    'b.web_book_show',
                    DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),(CASE WHEN b.booking_common_id IS NULL THEN "" ELSE CONCAT("","","") END)) as booking_reference'),
                    DB::raw('null as schedule_reference'),
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.booking_type',
                    'b.customer_address_id as address_id',
                    DB::raw('CONCAT(customer_address," ~ ",a.area_name) as address'),
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'b.payment_type_id',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                    'c.customer_name',
                    'b.service_week_day',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'b.service_start_date',
                    'b.service_end',
                    'b.service_end_date',
                    'b.service_actual_end_date',
                    'op.payment_status as op_payment_status',
                    'c.email_address'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                /*************************************************************/
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                })
                /*************************************************************/
                ->where([['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])
                ->orderBy('b.booking_status', 'ASC')
                ->orderBy('b.booked_datetime', 'DESC')
                ->orderBy('b.booking_id', 'DESC');
            $today = date('Y-m-d');
            $today_time = date('H:i:s');
            $upcoming_booking_ids->where(function ($query) use ($today, $today_time) {
                $query->where([['b.service_end', '=', 1], ['b.service_actual_end_date', '>=', $today]]);
                $query->orWhere([['b.service_end', '=', 0]]);
            });
            $upcoming_booking_ids->where(function ($query) {
                $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
                $query->orWhereNull('b.booking_common_id');
            });
            $upcoming_booking_ids->where(function ($query) {
                $query->where('b.payment_type_id', null); // show old data
                $query->orWhere('b.payment_type_id', 1); // show if cash mode
                $query->orWhere([['b.payment_type_id', '!=', 1], ['b.web_book_show', '=', 1]]);
            });
            $upcoming_booking_ids = $upcoming_booking_ids->get();
            $booking_addons = BookingAddons::select('id','booking_id','service_addon_name')->whereIn('booking_id', array_column($upcoming_booking_ids->toArray(), 'booking_id'))->get()->toArray();
            $waiting_list = [];
            foreach ($upcoming_booking_ids as $key => $booking) {
                $max_weeks = 5;
                /****************************************************** */
                // change data based on prof count
                if ($booking->booking_common_id) {
                    $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->leftJoin('maids as m', 'bookings.maid_id', 'm.maid_id')->get();
                    $professional_count = sizeof($bookings);
                    $upcoming_booking_ids[$key]->service_details = $upcoming_booking_ids[$key]->hours . " hours, " . $professional_count . " Professional " . ($upcoming_booking_ids[$key]->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                    foreach ($bookings as $sel_booking) {
                        // add all maids to crew
                        $professional = new stdClass();
                        $professional->professional = $sel_booking->maid_name ?: "[Not Assigned]";
                        $professional->professional_id = $sel_booking->maid_id;
                        $professional->verified = $sel_booking->maid_id || false;
                        $upcoming_booking_ids[$key]->crew[] = $professional;
                    }
                    $upcoming_booking_ids[$key]->total *= $professional_count;
                    $upcoming_booking_ids[$key]->payment_type_charge *= $professional_count;
                    /*********************************************************************************** */
                    // include booked addons
                    $upcoming_booking_ids[$key]->addons = array_filter($booking_addons, function ($booking_addon) use ($booking) {
                        return $booking_addon['booking_id'] == $booking->booking_common_id;
                    });
                    /*********************************************************************************** */
                } else {
                    $professional = new stdClass();
                    $professional->professional = $booking->maid_name ?: "[Not Assigned]";
                    $professional->professional_id = $booking->maid_id;
                    $professional->verified = $booking->maid_id || false;
                    $upcoming_booking_ids[$key]->crew[] = $professional;
                    $upcoming_booking_ids[$key]->service_details = $upcoming_booking_ids[$key]->hours . " hours, 1 Professional " . ($upcoming_booking_ids[$key]->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                    /*********************************************************************************** */
                    // include booked addons
                    $upcoming_booking_ids[$key]->addons = array_filter($booking_addons, function ($booking_addon) use ($booking) {
                        return $booking_addon['booking_id'] == $booking->booking_id;
                    });
                    /*********************************************************************************** */
                }
                $upcoming_booking_ids[$key]->addons = array_values($upcoming_booking_ids[$key]->addons);
                /****************************************************** */
                // rating DUMMy data goes here
                $upcoming_booking_ids[$key]->rating = new stdClass();
                $upcoming_booking_ids[$key]->rating->reference = null;
                $upcoming_booking_ids[$key]->rating->date = null;
                $upcoming_booking_ids[$key]->rating->service_rating = null;
                $upcoming_booking_ids[$key]->rating->maid_rating = null;
                $upcoming_booking_ids[$key]->rating->comment = null;
                //
                if ($booking->booking_status == 1) {
                    $status = "Confirmed";
                } elseif ($booking->payment_type_id == 1 && $booking->booking_status == 0) {
                    // cash
                    $status = "Pending";
                } elseif ($booking->payment_type_id != 1 && $booking->web_book_show == 1) {
                    $status = "Pending";
                } else {
                    $status = "Unknown";
                }
                $upcoming_booking_ids[$key]->status = $status;
                unset($upcoming_booking_ids[$key]->maid_id);
                unset($upcoming_booking_ids[$key]->maid_name);
                if ($upcoming_booking_ids[$key]->booking_status == 0) {
                    $max_weeks = 1; // show only 1 schedule for 'pending approval' bookings
                }
                /****************************************************** */
                /*                   LOGIC GOES HERE ↓                  */
                /****************************************************** */
                if ($booking->booking_type == "OD") {
                    $upcoming_booking_ids[$key]->loop_start_date = $booking->service_start_date;
                    $upcoming_booking_ids[$key]->loop_end_date = $booking->service_start_date;
                    $services[] = array('booking_id' => $booking->booking_id, 'date' => $booking->service_start_date);
                    $waiting_list[] = $this->return_service($booking, $booking->service_start_date);
                } else if ($booking->booking_type == "WE") {
                    // loop to get future dates
                    if ($booking->service_end == 0) {
                        // never ends, don't care about future date
                        $start_date = Carbon::parse($today)->subDays(1)->next($booking->service_week_day_name)->format('Y-m-d');
                        if ($start_date < $booking->service_start_date) {
                            // have future service start date
                            $start_date = $booking->service_start_date;
                        }
                        $upcoming_booking_ids[$key]->loop_start_date = $start_date;
                        $upcoming_booking_ids[$key]->loop_end_date = $start_date;
                        for ($i = 0; $i < $max_weeks; $i++) {
                            $end_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks($i)->format('Y-m-d');
                            $services[] = array('booking_id' => $booking->booking_id, 'date' => $end_date);
                            $waiting_list[] = $this->return_service($booking, $end_date);
                            $upcoming_booking_ids[$key]->loop_end_date = $end_date;
                        }
                    } else {
                        // end date available
                        $start_date = Carbon::parse($today)->subDays(1)->next($booking->service_week_day_name)->format('Y-m-d');
                        if ($start_date < $booking->service_start_date) {
                            // have future service start date
                            $start_date = $booking->service_start_date;
                        }
                        $upcoming_booking_ids[$key]->loop_start_date = $start_date;
                        $upcoming_booking_ids[$key]->loop_end_date = $start_date;
                        if ($booking->service_actual_end_date <= $start_date) {
                            // ends on same date or bug
                            $services[] = array('booking_id' => $booking->booking_id, 'date' => $start_date);
                            $waiting_list[] = $this->return_service($booking, $start_date);
                        } else {
                            $services[] = array('booking_id' => $booking->booking_id, 'date' => $start_date);
                            $waiting_list[] = $this->return_service($booking, $start_date);
                            $i = 1;
                            do {
                                $end_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks($i)->format('Y-m-d');
                                if ($end_date <= $booking->service_actual_end_date) {
                                    $services[] = array('booking_id' => $booking->booking_id, 'date' => $end_date);
                                    $waiting_list[] = $this->return_service($booking, $end_date);
                                    $upcoming_booking_ids[$key]->loop_end_date = $end_date;
                                }
                                $i++;
                            } while ($i < $max_weeks && $end_date < $booking->service_actual_end_date);
                        }
                    }
                } else if ($booking->booking_type == "BW") {
                    // loop to get future dates
                    if ($booking->service_end == 0) {
                        // never ends, don't care about future date
                        $start_date = Carbon::parse($today)->subDays(1)->next($booking->service_week_day_name)->format('Y-m-d');
                        $week_difference = Carbon::parse($booking->service_start_date)->diffInWeeks($start_date);
                        if ($week_difference % 2 == 0) {
                        } else {
                            $start_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks(1)->format('Y-m-d');
                        }
                        if ($start_date < $booking->service_start_date) {
                            // have future service start date
                            $start_date = $booking->service_start_date;
                        }
                        $upcoming_booking_ids[$key]->loop_start_date = $start_date;
                        $upcoming_booking_ids[$key]->loop_end_date = $start_date;
                        for ($i = 0; $i < $max_weeks; $i++) {
                            $end_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks($i * 2)->format('Y-m-d');
                            $services[] = array('booking_id' => $booking->booking_id, 'date' => $end_date);
                            $waiting_list[] = $this->return_service($booking, $end_date);
                            $upcoming_booking_ids[$key]->loop_end_date = $end_date;
                        }
                    } else {
                        // end date available
                        $start_date = Carbon::parse($today)->subDays(1)->next($booking->service_week_day_name)->format('Y-m-d');
                        $week_difference = Carbon::parse($booking->service_start_date)->diffInWeeks($start_date);
                        if ($week_difference % 2 == 0) {
                        } else {
                            $start_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks(1)->format('Y-m-d');
                        }
                        if ($start_date <= $booking->service_actual_end_date) {
                            if ($start_date < $booking->service_start_date) {
                                // have future service start date
                                $start_date = $booking->service_start_date;
                            }
                            $upcoming_booking_ids[$key]->loop_start_date = $start_date;
                            $upcoming_booking_ids[$key]->loop_end_date = $start_date;
                            if ($booking->service_actual_end_date <= $start_date) {
                                // ends on same date or bug
                                $services[] = array('booking_id' => $booking->booking_id, 'date' => $start_date);
                                $waiting_list[] = $this->return_service($booking, $start_date);
                            } else {
                                $services[] = array('booking_id' => $booking->booking_id, 'date' => $start_date);
                                $waiting_list[] = $this->return_service($booking, $start_date);
                                $i = 1;
                                do {
                                    $end_date = Carbon::createFromFormat('Y-m-d', $start_date)->addWeeks($i * 2)->format('Y-m-d');
                                    if ($end_date <= $booking->service_actual_end_date) {
                                        $services[] = array('booking_id' => $booking->booking_id, 'date' => $end_date);
                                        $completed_list[] = $this->return_service($booking, $end_date);
                                        $upcoming_booking_ids[$key]->loop_end_date = $end_date;
                                    }
                                    $i++;
                                } while ($i < $max_weeks && $end_date < $booking->service_actual_end_date);
                            }
                        }
                    }
                }
            }
            foreach ($waiting_list as $key => $booking) {
                /************************************************************************** */
                // dynamic menu for app/web
                $waiting_list[$key]->menu = bookingHistoryMenu(); // default
                if ($booking->payment_type_id == 1) {
                    // cash
                    $waiting_list[$key]->menu->retry_payment = false;
                    $waiting_list[$key]->menu->change_payment_method = true;
                } else if ($booking->payment_type_id == 2) {
                    // card
                    if ($booking->op_payment_status != "success") {
                        $waiting_list[$key]->menu->retry_payment = true;
                        /**
                         * payfort data adding for use in frontend
                         */
                        $requestParams = [
                            'command' => 'PURCHASE',
                            'access_code' => Config::get('values.payfort_access_code'),
                            'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
                            'amount' => (float) $booking->total * 100,
                            'currency' => Config::get('values.currency_code'),
                            'language' => strtolower(Config::get('values.language_code')),
                            'customer_email' => $booking->email_address ?: 'test@example.com',
                            'order_description' => $booking->booking_reference,
                            'merchant_reference' => $booking->booking_reference,
                            'merchant_extra' => $booking->booking_reference,
                            'return_url' => Config::get('values.web_booking_url') . "payment/payfort/processing",
                        ];
                        ksort($requestParams);
                        $hash_value = Config::get('values.payfort_sha_request_phrase');
                        foreach ($requestParams as $name => $value) {
                            if ($value != null) {
                                $hash_value .= $name . '=' . $value;
                            }
                        }
                        $hash_value .= Config::get('values.payfort_sha_request_phrase');
                        $requestParams['signature'] = hash('sha256', $hash_value);
                        $waiting_list[$key]->checkout_url = Config::get('values.payfort_payment_page_url');
                        $waiting_list[$key]->checkout_data = ['payfort_data' => $requestParams];
                    } else {
                        if ($booking->booking_type == "OD") {
                            $waiting_list[$key]->menu->reschedule = true;
                        }
                    }
                }
                $waiting_list[$key]->menu->cancel_this = true;
                $waiting_list[$key]->menu->cancel_all = true;
                /************************************************************************** */
                // remove unwanted data
                unset($waiting_list[$key]->loop_start_date);
                unset($waiting_list[$key]->loop_end_date);
                //unset($waiting_list[$key]->service_week_day);
                unset($waiting_list[$key]->service_week_day_name);
                //unset($waiting_list[$key]->service_end);
                unset($waiting_list[$key]->service_end_date);
                unset($waiting_list[$key]->service_actual_end_date);
                // check with day services
                $day_service = DB::table('day_services as ds')
                    ->select(
                        'ds.day_service_id',
                    )
                    ->where([['ds.booking_id', '=', $booking->booking_id], ['ds.service_date', '=', $booking->date]])
                    ->first();
                $booking_delete_remark = BookingDeleteRemarks::where(function ($query) use ($today, $booking) {
                    $query->where([['booking_id', '=', $booking->booking_id], ['service_date', '=', $booking->date]]);
                    $query->orWhere([['booking_id', '=', $booking->booking_id], ['service_date', '=', null]]);
                })
                    ->first();
                if ($day_service || $booking_delete_remark) {
                    // service entry exist
                    unset($waiting_list[$key]);
                }
            }
            $waiting_list = array_values($waiting_list);
            $response['completed_list'] = $waiting_list;
            //$response['debug_upcoming_services'] = $services;
            //$response['debug_upcoming_booking_ids'] = $upcoming_booking_ids;
            $response['message'] = sizeof($response['completed_list']) ? "Upcoming schedules fetched successfully." : "No upcoming schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function booking_details(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['booking_id'] = "waiting";
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            $validator = Validator::make(
                (array) $input,
                [
                    'id' => 'required|integer',
                    'booking_id' => 'required|integer',
                ],
                [],
                [
                    'id' => 'ID',
                    'booking_id' => 'Booking ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $response['status'] = 'success';
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    'b.web_book_show',
                    DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),(CASE WHEN b.booking_common_id IS NULL THEN "" ELSE CONCAT("","","") END)) as booking_reference'),
                    DB::raw('null as schedule_reference'),
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.booking_type',
                    'b.customer_address_id as address_id',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'b.payment_type_id',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                    'c.customer_name',
                    'b.service_week_day',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'b.service_start_date',
                    'b.service_end',
                    'b.service_end_date',
                    'b.service_actual_end_date',
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->where([['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])
                ->orderBy('b.booking_id', 'ASC');
            $bookings->where(function ($query) use ($input) {
                $query->where('b.booking_common_id', $input['booking_id']);
                $query->orWhere('b.booking_id', $input['booking_id']);
            });
            $response['data']['bookings'] = $bookings->get();
            $response['data']['booking_address'] = DB::table('customer_addresses as ca')
                ->select(
                    'ca.customer_address_id as address_id',
                    'ca.street as street',
                    'ca.building as building',
                    DB::raw('ca.unit_no as flat_no'),
                    'ca.latitude as lat',
                    'ca.longitude as long',
                    'ca.default_address',
                    'z.zone_id',
                    'a.area_id',
                    'z.zone_name',
                    'a.area_name'
                )
                ->leftJoin('areas as a', 'ca.area_id', 'a.area_id')
                ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
                ->where([['ca.customer_id', '=', $input['id']], ['ca.customer_address_id', '=', $response['data']['bookings'][0]->address_id]])
                ->orderBy('ca.customer_address_id', 'DESC')
                ->first();
            $response['message'] = sizeof($response['data']['bookings']) ? "Booking details fetched successfully." : "No upcoming schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function booking_details_by_ref(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['reference_id'] = "757857";
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            $validator = Validator::make(
                (array) $input,
                [
                    'id' => 'required|integer',
                    'reference_id' => 'required|string',
                ],
                [],
                [
                    'id' => 'ID',
                    'reference_id' => 'Booking ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $response['status'] = 'success';
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.booking_status',
                    'b.web_book_show',
                    DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),(CASE WHEN b.booking_common_id IS NULL THEN "" ELSE CONCAT("","","") END)) as booking_reference'),
                    DB::raw('null as schedule_reference'),
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                    'f.name as frequency',
                    'b.booking_type',
                    'b.customer_address_id as address_id',
                    DB::raw('CONCAT(customer_address," ~ ",a.area_name) as address'),
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'b.payment_type_id',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'b.total_amount as total',
                    'pt.charge as payment_type_charge',
                    DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                    'c.customer_name',
                    'b.service_week_day',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'b.service_start_date',
                    'b.service_end',
                    'b.service_end_date',
                    'b.service_actual_end_date',
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                ->where([['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])
                ->where('b.reference_id', $input['reference_id'])
                ->orderBy('b.booking_id', 'ASC');
            $response['data']['bookings'] = $bookings->get();
            $response['data']['booking_address'] = DB::table('customer_addresses as ca')
                ->select(
                    DB::raw('CONCAT(customer_address," ~ ",a.area_name) as address'),
                    'ca.customer_address_id as address_id',
                    'ca.street as street',
                    'ca.building as building',
                    DB::raw('ca.unit_no as flat_no'),
                    'ca.latitude as lat',
                    'ca.longitude as long',
                    'ca.default_address',
                    'z.zone_id',
                    'a.area_id',
                    'z.zone_name',
                    'a.area_name'
                )
                ->leftJoin('areas as a', 'ca.area_id', 'a.area_id')
                ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
                ->where([['ca.customer_id', '=', $input['id']], ['ca.customer_address_id', '=', $response['data']['bookings'][0]->address_id]])
                ->orderBy('ca.customer_address_id', 'DESC')
                ->first();
            $response['message'] = sizeof($response['data']['bookings']) ? "Booking details fetched successfully." : "No upcoming schedules found.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function return_service($booking, $service_date)
    {
        $object = clone $booking;
        $object->date = $service_date;
        return $object;
    }
}
