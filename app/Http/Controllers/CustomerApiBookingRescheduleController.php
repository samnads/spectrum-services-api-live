<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingRescheduleController extends Controller
{
    public function reschedule(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_id'] = 359777;
            $data['params']['date'] = "22/06/2023";
            $data['params']['time_from'] = "14:00";
            $professional_prefer = array(
                // currently allowing same hours for all profs.
                //array('professional_id' => 424),
                //array('professional_id' => 266),
            );
            $data['params']['professional_prefer'] = $professional_prefer;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'booking_id' => 'required|integer',
                'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                'time_from' => 'required|date_format:H:i',
                'professional_prefer' => 'array',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'date' => 'Date',
                'time_from' => 'Start Time',
                'professional_prefer' => 'Selected Professionals',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /*$input['professional_prefer'] = array_unique($input['professional_prefer'], SORT_REGULAR); // remove duplicates - rare case or app bug
        if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
        return Response::json(array('result' => array('status' => 'failed', 'message' => "Professional count mismatch."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }*/
        /************************************************************* */
        // validate booking
        $booking = DB::table('bookings as b')
            ->select(
                'b.*',
            )
            ->where([['b.booking_id', '=', $input['booking_id']], ['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])->first();
        if (!$booking) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        // format for machine :/
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
        $input['time_from'] = Carbon::createFromFormat('H:i', $input['time_from'])->format('H:i:s');
        $input['hours'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->diffInMinutes(Carbon::createFromFormat('H:i:s', $booking->time_to)) / 60; // based on db
        $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($input['hours'] * 60)->format('H:i:s');
        //dd($input['time_to']);
        /****************************************************************************************** */
        // real time time-slot availability check
        if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /****************************************************************************************** */
        if ($booking->booking_common_id) {
            // this booking is acting as single booking
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'b.booking_common_id',
                )
                ->where([['booking_common_id', '=', $booking->booking_common_id], ['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])->get();
            $booking_ids = array_column($bookings->toArray(), 'booking_id');
            $input['professionals_count'] = sizeof($booking_ids);
            /******* check if any day service entry found for any booking */
            $day_service = DB::table('day_services as ds')
                ->select(
                    'ds.day_service_id',
                )
                ->whereIn('ds.booking_id', $booking_ids)->first();
            if ($day_service) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Schedule entry found for this booking, reschedule not possible.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /********** find end date based on no. of weeks and service end */
            $service_actual_end_date = $input['date'];
            if ($booking->service_end == "1") {
                if ($booking->booking_type == "WE") {
                    $no_of_service = Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1;
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service - 1)->format('Y-m-d');
                    //dd($no_of_service);
                } else if ($booking->booking_type == "BW") {
                    $no_of_service = (int) round(round(Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1 / 2) / 2);
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service * 2 - 2)->format('Y-m-d');
                    //dd($service_actual_end_date);
                }
            }
            /*********************** */
            $booking_search = new stdClass();
            $booking_search->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking_search->service_start_date = $input['date'];
            $booking_search->service_actual_end_date = $service_actual_end_date;
            $booking_search->booking_type = $booking->booking_type;
            $booking_search->time_from = $input['time_from'];
            $booking_search->time_to = $input['time_to'];
            $booking_search->service_end = $booking->service_end;
            $busy_bookings = get_busy_bookings($booking_search, $booking_ids)->toArray();
            // trick for BW skipping
            if ($booking->booking_type == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings, 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column(@$input['professional_prefer'] ?: [], 'professional_id');
            //*************************************** */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
            //dd($available_maid_ids);
            /*************************************** */
            for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                shuffle($available_maid_ids); // just shuffle
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
            }
            /****************************************************************************************** */
            DB::beginTransaction();
            $booking = Booking::find($booking->booking_id);
            try {
                foreach ($selected_professionals as $key => $professional_id) {
                    // add new booking rows
                    $new_booking = Booking::find($bookings[$key]->booking_id);
                    $new_booking->service_start_date = $input['date'];
                    $new_booking->service_week_day = Carbon::parse($input['date'])->format('w');
                    $new_booking->service_end_date = $service_actual_end_date;
                    $new_booking->service_actual_end_date = $service_actual_end_date;
                    $new_booking->time_from = $input['time_from'];
                    $new_booking->time_to = $input['time_to'];
                    $new_booking->booking_status = 0;
                    $new_booking->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                    $new_booking->save();
                }
                // delete (change status) old booking rows
                $booking_common_id = $bookings[0]->booking_id;
                $response['activelist'] = DB::table('bookings as b')
                    ->select(
                        'b.booking_id',
                        DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                        DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),"","","") as booking_reference'),
                        'f.name as frequency',
                        'ca.customer_address as address',
                        DB::raw('"AED" as currency'),
                        'b.total_amount as total',
                        DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                        DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                        DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                        DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours_worked'),
                        DB::raw('"Waiting" as status'),
                        DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    )
                    ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                    ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                    ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                    ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                    ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                    ->where([['b.booking_id', '=', $booking_common_id]])
                    ->first();
                $response['activelist']->service_details = $response['activelist']->hours_worked . " hours, " . sizeof($selected_professionals) . " Professional " . ($response['activelist']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                $response['status'] = 'success';
                $response['message'] = 'Booking rescheduled successfully with Ref. No. ' . $booking_common_id . '.';
                /********************************************************************************************* */
                $notify = new stdClass();
                $notify->customer_id = $input['id'];
                $notify->booking_id = $booking_common_id;
                $notify->service_date = $booking->booking_type == "OD" ? $input['date'] : null;
                $notify->content = "Booking is rescheduled with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                addCustomerNotification($notify);
                /********************************************************************************************* */
                DB::commit();
                //DB::rollback();
                return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            // single prof. booking
            $input['professionals_count'] = 1;
            /******* check if any day service entry found for any booking */
            $day_service = DB::table('day_services as ds')
                ->select(
                    'ds.day_service_id',
                )
                ->where('ds.booking_id', $booking->booking_id)->first();
            if ($day_service) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Schedule entry found for this booking, reschedule not possible.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } else {
                /********** find end date based on no. of weeks and service end */
                $service_actual_end_date = $input['date'];
                if ($booking->service_end == "1") {
                    if ($booking->booking_type == "WE") {
                        $no_of_service = Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1;
                        $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service - 1)->format('Y-m-d');
                        //dd($no_of_service);
                    } else if ($booking->booking_type == "BW") {
                        $no_of_service = (int) round(round(Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1 / 2) / 2);
                        $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service * 2 - 2)->format('Y-m-d');
                        //dd($service_actual_end_date);
                    }
                }
                /*********************** */
                $booking_search = new stdClass();
                $booking_search->service_week_day = Carbon::parse($input['date'])->format('w');
                $booking_search->service_start_date = $input['date'];
                $booking_search->service_actual_end_date = $service_actual_end_date;
                $booking_search->booking_type = $booking->booking_type;
                $booking_search->time_from = $input['time_from'];
                $booking_search->time_to = $input['time_to'];
                $booking_search->service_end = $booking->service_end;
                $except_booking_ids = array($booking->booking_id);
                $busy_bookings = get_busy_bookings($booking_search, $except_booking_ids)->toArray();
                $busy_maid_ids = array_unique(array_column($busy_bookings, 'maid_id'));
                $available_maids = DB::table('maids as m')
                    ->select(
                        'm.maid_id',
                        'm.maid_name as name',
                    )
                    ->where(['maid_status' => 1])
                    ->whereNotIn('maid_id', $busy_maid_ids)
                    ->orderBy('maid_name', 'ASC')
                    ->get();
                $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
                $preferred_professional_ids = array_column(@$input['professional_prefer'] ?: [], 'professional_id');
                //*************************************** */
                // check selected prof. availability
                //dd($available_maid_ids);
                $selected_professionals = [];
                $prof_validated = 0;
                foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                    // manual professional select
                    if (in_array($preferred_professional_id, $available_maid_ids)) {
                        // selected prof. available
                        $prof_validated += 1;
                        $selected_professionals[] = $preferred_professional_id;
                        unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                        $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                    } else {
                        // selected prof. not available
                        $maid = DB::table('maids as m')
                            ->select(
                                'm.maid_id',
                                'm.maid_name as name',
                            )
                            ->where(['maid_id' => $preferred_professional_id])
                            ->first();
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
                //dd($available_maid_ids);
                /*************************************** */
                for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                    shuffle($available_maid_ids); // just shuffle
                    // auto assign professional
                    //die($input['professionals_count']);
                    if (isset($available_maid_ids[0])) {
                        $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                        unset($available_maid_ids[0]); // remove from available list
                        $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                    } else {
                        if (sizeof($selected_professionals) > 0) {
                            return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                        } else {
                            return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                        }
                    }
                }
                /****************************************************************************************** */
                DB::beginTransaction();
                $booking = Booking::find($booking->booking_id);
                try {
                    $booking->maid_id = in_array($selected_professionals[0], $preferred_professional_ids) ? $selected_professionals[0] : 0; // dont auto assign :/
                    $booking->service_start_date = $input['date'];
                    $booking->service_week_day = Carbon::parse($input['date'])->format('w');
                    $booking->service_end_date = $service_actual_end_date;
                    $booking->service_actual_end_date = $service_actual_end_date;
                    $booking->time_from = $input['time_from'];
                    $booking->time_to = $input['time_to'];
                    $booking->booked_datetime = Carbon::now();
                    $booking->booking_status = 0;
                    $booking->save();
                    $response['activelist'] = DB::table('bookings as b')
                        ->select(
                            'b.booking_id',
                            DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                            DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),"","","") as booking_reference'),
                            'f.name as frequency',
                            'ca.customer_address as address',
                            DB::raw('"AED" as currency'),
                            'b.total_amount as total',
                            DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                            DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                            DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                            DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours_worked'),
                            DB::raw('"Waiting" as status'),
                            DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                        )
                        ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                        ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                        ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                        ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                        ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                        ->where([['b.booking_id', '=', $booking->booking_id]])
                        ->first();
                    $response['activelist']->service_details = $response['activelist']->hours_worked . " hours, " . sizeof($selected_professionals) . " Professional " . ($response['activelist']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                    $response['status'] = 'success';
                    $response['message'] = 'Booking rescheduled successfully with Ref. No. ' . $booking->reference_id . '.';
                    /********************************************************************************************* */
                    $notify = new stdClass();
                    $notify->customer_id = $input['id'];
                    $notify->booking_id = $booking->booking_id;
                    $notify->service_date = $booking->booking_type == "OD" ? $input['date'] : null;
                    $notify->content = "Booking is rescheduled with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                    addCustomerNotification($notify);
                    /********************************************************************************************* */
                    DB::commit();
                    //DB::rollback();
                    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                } catch (\Exception $e) {
                    DB::rollback();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
        }
        /************************************************************* */
    }
    public function reschedule_old(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_id'] = 359777;
            $data['params']['date'] = "22/06/2023";
            $data['params']['time_from'] = "14:00";
            $professional_prefer = array(
                // currently allowing same hours for all profs.
                //array('professional_id' => 424),
                //array('professional_id' => 266),
            );
            $data['params']['professional_prefer'] = $professional_prefer;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'booking_id' => 'required|integer',
                'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                'time_from' => 'required|date_format:H:i',
                'professional_prefer' => 'array',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'date' => 'Date',
                'time_from' => 'Start Time',
                'professional_prefer' => 'Selected Professionals',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /*$input['professional_prefer'] = array_unique($input['professional_prefer'], SORT_REGULAR); // remove duplicates - rare case or app bug
        if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
        return Response::json(array('result' => array('status' => 'failed', 'message' => "Professional count mismatch."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }*/
        /************************************************************* */
        // validate booking
        $booking = DB::table('bookings as b')
            ->select(
                'b.*',
            )
            ->where([['b.booking_id', '=', $input['booking_id']], ['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])->first();
        if (!$booking) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        // format for machine :/
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
        $input['time_from'] = Carbon::createFromFormat('H:i', $input['time_from'])->format('H:i:s');
        $input['hours'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->diffInMinutes(Carbon::createFromFormat('H:i:s', $booking->time_to)) / 60; // based on db
        $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($input['hours'] * 60)->format('H:i:s');
        //dd($input['time_to']);
        /****************************************************************************************** */
        // real time time-slot availability check
        if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /****************************************************************************************** */
        if ($booking->booking_common_id) {
            // this booking is acting as single booking
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'b.booking_common_id',
                )
                ->where([['booking_common_id', '=', $booking->booking_common_id], ['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])->get();
            $booking_ids = array_column($bookings->toArray(), 'booking_id');
            $input['professionals_count'] = sizeof($booking_ids);
            /******* check if any day service entry found for any booking */
            $day_service = DB::table('day_services as ds')
                ->select(
                    'ds.day_service_id',
                )
                ->whereIn('ds.booking_id', $booking_ids)->first();
            if ($day_service) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Schedule entry found for this booking, reschedule not possible.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /********** find end date based on no. of weeks and service end */
            $service_actual_end_date = $input['date'];
            if ($booking->service_end == "1") {
                if ($booking->booking_type == "WE") {
                    $no_of_service = Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1;
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service - 1)->format('Y-m-d');
                    //dd($no_of_service);
                } else if ($booking->booking_type == "BW") {
                    $no_of_service = (int) round(round(Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1 / 2) / 2);
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service * 2 - 2)->format('Y-m-d');
                    //dd($service_actual_end_date);
                }
            }
            /*********************** */
            $booking_search = new stdClass();
            $booking_search->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking_search->service_start_date = $input['date'];
            $booking_search->service_actual_end_date = $service_actual_end_date;
            $booking_search->booking_type = $booking->booking_type;
            $booking_search->time_from = $input['time_from'];
            $booking_search->time_to = $input['time_to'];
            $booking_search->service_end = $booking->service_end;
            $busy_bookings = get_busy_bookings($booking_search, $booking_ids)->toArray();
            // trick for BW skipping
            if ($booking->booking_type == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings, 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column(@$input['professional_prefer'] ?: [], 'professional_id');
            //*************************************** */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
            //dd($available_maid_ids);
            /*************************************** */
            for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                shuffle($available_maid_ids); // just shuffle
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
            }
            /****************************************************************************************** */
            DB::beginTransaction();
            $booking = Booking::find($booking->booking_id);
            try {
                foreach ($selected_professionals as $key => $professional_id) {
                    // add new booking rows
                    $new_booking = $booking->replicate(); // we only change some fields, so it's better
                    $new_booking->save();
                    $new_booking->reference_id = Config::get('values.booking_ref_prefix') . $new_booking->booking_id;
                    $new_booking->service_start_date = $input['date'];
                    $new_booking->service_week_day = Carbon::parse($input['date'])->format('w');
                    $new_booking->service_end_date = $service_actual_end_date;
                    $new_booking->service_actual_end_date = $service_actual_end_date;
                    $new_booking->time_from = $input['time_from'];
                    $new_booking->time_to = $input['time_to'];
                    $new_booking->booked_datetime = Carbon::now();
                    $new_booking->booking_status = 0;
                    $booking_common_id = @$booking_common_id ?: $new_booking->booking_id;
                    $new_booking->booking_common_id = $booking_common_id;
                    //$new_booking->maid_id = $professional_id;
                    $new_booking->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                    $new_booking->save();
                }
                // delete (change status) old booking rows
                Booking::whereIn('booking_id', $booking_ids)->update(['booking_status' => 2]);
                $response['activelist'] = DB::table('bookings as b')
                    ->select(
                        'b.booking_id',
                        DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                        DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),"","","") as booking_reference'),
                        'f.name as frequency',
                        'ca.customer_address as address',
                        DB::raw('"AED" as currency'),
                        'b.total_amount as total',
                        DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                        DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                        DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                        DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours_worked'),
                        DB::raw('"Waiting" as status'),
                        DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    )
                    ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                    ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                    ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                    ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                    ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                    ->where([['b.booking_id', '=', $booking_common_id]])
                    ->first();
                $response['activelist']->service_details = $response['activelist']->hours_worked . " hours, " . sizeof($selected_professionals) . " Professional " . ($response['activelist']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                $response['status'] = 'success';
                $response['message'] = 'Booking rescheduled successfully with Ref. No. ' . $booking_common_id . '.';
                /********************************************************************************************* */
                $notify = new stdClass();
                $notify->customer_id = $input['id'];
                $notify->booking_id = $booking_common_id;
                $notify->service_date = $booking->booking_type == "OD" ? $input['date'] : null;
                $notify->content = "Booking is rescheduled with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                addCustomerNotification($notify);
                /********************************************************************************************* */
                DB::commit();
                //DB::rollback();
                return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            // single prof. booking
            $input['professionals_count'] = 1;
            /******* check if any day service entry found for any booking */
            $day_service = DB::table('day_services as ds')
                ->select(
                    'ds.day_service_id',
                )
                ->where('ds.booking_id', $booking->booking_id)->first();
            if ($day_service) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Schedule entry found for this booking, reschedule not possible.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } else {
                /********** find end date based on no. of weeks and service end */
                $service_actual_end_date = $input['date'];
                if ($booking->service_end == "1") {
                    if ($booking->booking_type == "WE") {
                        $no_of_service = Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1;
                        $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service - 1)->format('Y-m-d');
                        //dd($no_of_service);
                    } else if ($booking->booking_type == "BW") {
                        $no_of_service = (int) round(round(Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1 / 2) / 2);
                        $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service * 2 - 2)->format('Y-m-d');
                        //dd($service_actual_end_date);
                    }
                }
                /*********************** */
                $booking_search = new stdClass();
                $booking_search->service_week_day = Carbon::parse($input['date'])->format('w');
                $booking_search->service_start_date = $input['date'];
                $booking_search->service_actual_end_date = $service_actual_end_date;
                $booking_search->booking_type = $booking->booking_type;
                $booking_search->time_from = $input['time_from'];
                $booking_search->time_to = $input['time_to'];
                $booking_search->service_end = $booking->service_end;
                $except_booking_ids = array($booking->booking_id);
                $busy_bookings = get_busy_bookings($booking_search, $except_booking_ids)->toArray();
                $busy_maid_ids = array_unique(array_column($busy_bookings, 'maid_id'));
                $available_maids = DB::table('maids as m')
                    ->select(
                        'm.maid_id',
                        'm.maid_name as name',
                    )
                    ->where(['maid_status' => 1])
                    ->whereNotIn('maid_id', $busy_maid_ids)
                    ->orderBy('maid_name', 'ASC')
                    ->get();
                $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
                $preferred_professional_ids = array_column(@$input['professional_prefer'] ?: [], 'professional_id');
                //*************************************** */
                // check selected prof. availability
                //dd($available_maid_ids);
                $selected_professionals = [];
                $prof_validated = 0;
                foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                    // manual professional select
                    if (in_array($preferred_professional_id, $available_maid_ids)) {
                        // selected prof. available
                        $prof_validated += 1;
                        $selected_professionals[] = $preferred_professional_id;
                        unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                        $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                    } else {
                        // selected prof. not available
                        $maid = DB::table('maids as m')
                            ->select(
                                'm.maid_id',
                                'm.maid_name as name',
                            )
                            ->where(['maid_id' => $preferred_professional_id])
                            ->first();
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
                //dd($available_maid_ids);
                /*************************************** */
                for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                    shuffle($available_maid_ids); // just shuffle
                    // auto assign professional
                    //die($input['professionals_count']);
                    if (isset($available_maid_ids[0])) {
                        $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                        unset($available_maid_ids[0]); // remove from available list
                        $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                    } else {
                        if (sizeof($selected_professionals) > 0) {
                            return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                        } else {
                            return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                        }
                    }
                }
                /****************************************************************************************** */
                DB::beginTransaction();
                $booking = Booking::find($booking->booking_id);
                try {
                    foreach ($selected_professionals as $key => $professional_id) {
                        // add new booking rows
                        $new_booking = $booking->replicate(); // we only change some fields, so it's better
                        $new_booking->booking_common_id = null; // because this booking have only 1 prof.
                        //$new_booking->maid_id = $professional_id;
                        $new_booking->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                        $new_booking->service_start_date = $input['date'];
                        $new_booking->service_week_day = Carbon::parse($input['date'])->format('w');
                        $new_booking->service_end_date = $service_actual_end_date;
                        $new_booking->service_actual_end_date = $service_actual_end_date;
                        $new_booking->time_from = $input['time_from'];
                        $new_booking->time_to = $input['time_to'];
                        $new_booking->booked_datetime = Carbon::now();
                        $new_booking->booking_status = 0;
                        $new_booking->save();
                        $new_booking->reference_id = Config::get('values.booking_ref_prefix') . $new_booking->booking_id;
                        $new_booking->save();
                    }
                    // delete (change status) old booking rows
                    Booking::where('booking_id', $booking->booking_id)->update(['booking_status' => 2]);
                    $response['activelist'] = DB::table('bookings as b')
                        ->select(
                            'b.booking_id',
                            DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                            DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),"","","") as booking_reference'),
                            'f.name as frequency',
                            'ca.customer_address as address',
                            DB::raw('"AED" as currency'),
                            'b.total_amount as total',
                            DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                            DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                            DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                            DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours_worked'),
                            DB::raw('"Waiting" as status'),
                            DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                        )
                        ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                        ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                        ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                        ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                        ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                        ->where([['b.booking_id', '=', $new_booking->booking_id]])
                        ->first();
                    $response['activelist']->service_details = $response['activelist']->hours_worked . " hours, " . sizeof($selected_professionals) . " Professional " . ($response['activelist']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
                    $response['status'] = 'success';
                    $response['message'] = 'Booking rescheduled successfully with Ref. No. ' . $new_booking->reference_id . '.';
                    /********************************************************************************************* */
                    $notify = new stdClass();
                    $notify->customer_id = $input['id'];
                    $notify->booking_id = $new_booking->booking_id;
                    $notify->service_date = $booking->booking_type == "OD" ? $input['date'] : null;
                    $notify->content = "Booking is rescheduled with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                    addCustomerNotification($notify);
                    /********************************************************************************************* */
                    DB::commit();
                    //DB::rollback();
                    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                } catch (\Exception $e) {
                    DB::rollback();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
        }
        /************************************************************* */
    }
}
