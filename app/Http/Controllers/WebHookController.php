<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\CustomerPayments;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use stdClass;

class WebHookController extends Controller
{
    public function payfort_transaction_feedback(Request $request)
    {
        // The URL where the Merchant's server receives Amazon Payment Services responses after the transaction is processed.
        $payfort_data = $request->all();
        if (@$payfort_data['response_message'] == 'Success') {
            $online_payment = OnlinePayment::where('reference_id', $payfort_data['merchant_reference'])->first();
            // update database
            $online_payment->transaction_id = $payfort_data['fort_id'];
            $online_payment->payment_status = 'success';
            $online_payment->post_data = json_encode($payfort_data);
            $online_payment->return_data = null;
            $online_payment->paid_from = bookingPlatform('web');
            $online_payment->payment_datetime = now();
            $online_payment->save();
            if (@$online_payment->customer_id) {
                // we don't use this from anywhere other than this hook segment
                save_card_data_from_payfort_payment_data($online_payment->customer_id, $payfort_data);
            }
            // send mails
            $mail = new AdminApiMailController;
            $online_payment_ref_prefix = substr($payfort_data['merchant_reference'], 0, strlen(Config::get('values.online_payment_ref_prefix')));
            $booking_payment_ref_prefix = substr($payfort_data['merchant_reference'], 0, strlen(Config::get('values.booking_ref_prefix')));
            if ($online_payment_ref_prefix == Config::get('values.online_payment_ref_prefix')) {
                // if it's an online direct payment (not an booking payment)
                // send to customer
                $mail->online_payment_confirmation_to_customer($online_payment->payment_id); // send payment confirmation mail
                $mail->online_payment_confirmation_to_admin($online_payment->payment_id); // send payment received mail
            }
            else if ($booking_payment_ref_prefix == Config::get('values.booking_ref_prefix')) {
                // if it's an payment for booking exclusively
                // send to admin
                $mail->online_payment_confirmation_to_admin($online_payment->payment_id); // send payment received mail
            }
        }
    }
    public function payfort_notification(Request $request)
    {
        // The URL where the Merchant receives offline notifications regarding any status updates for any of his transactions and orders.
    }
}
