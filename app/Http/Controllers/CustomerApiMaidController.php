<?php

namespace App\Http\Controllers;

use Cache;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiMaidController extends Controller
{
    public function crew_list(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['date'] = "16/11/2023"; // dd/mm/yyyy format
            $data['params']['time_from'] = "12:30";
            $data['params']['time_to'] = "13:00";
            $data['params']['frequency'] = "WE";
            $data['params']['booking_id'] = null;
            $data['params']['zone_id'] = 39; //zone 12
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['debug_input'] = @$data['params'];
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                'time_from' => 'required|date_format:H:i',
                'time_to' => 'required|date_format:H:i',
                'frequency' => 'required|in:OD,WE,BW',
                'booking_id' => 'nullable|integer',
                'zone_id' => 'nullable|integer',
            ],
            [],
            [
                'date' => 'Service Date',
                'time_from' => 'Service Start Time',
                'time_to' => 'Service End Time',
                'frequency' => 'Frequency',
                'booking_id' => 'Booking ID',
                'zone_id' => 'Zone ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
        $input['time_from'] = Carbon::createFromFormat('H:i', $input['time_from'])->format('H:i:s');
        $input['time_to'] = Carbon::createFromFormat('H:i', $input['time_to'])->format('H:i:s');
        /************************************************************* */
        //rating data
        $maid_reviews_by_customer = Cache::get('maid_reviews_by_customer') ?: DB::table('maid_rating as mr')
            ->select(
                'm.maid_id',
                'c.customer_id',
                'c.customer_name',
                'mr.rating',
                DB::raw('null as rating_details'),
                DB::raw('DATE_FORMAT(mr.created_at,"%Y-%m-%d") as date'),
            )
            ->leftJoin('maids as m', 'mr.maid_id', 'm.maid_id')
            ->leftJoin('service_types as st', 'mr.service_type_id', 'st.service_type_id')
            ->leftJoin('customers as c', 'mr.rated_by_customer_id', 'c.customer_id')
            ->where(['c.customer_status' => 1])
            ->where(['mr.deleted_at' => null])
            ->orderBy('mr.created_at', 'DESC')
            ->get();
        //Cache::put('maid_reviews_by_customer', $maid_reviews_by_customer, 60); // cache it if slow down
        $maid_reviews_by_customer_summary = Cache::get('maid_reviews_by_customer_summary') ?: DB::table('maid_rating as mr')
            ->select(
                'm.maid_id',
                DB::raw('SUM(CASE WHEN mr.rating = "5" THEN "5" ELSE "0" END)/COUNT(mr.id) as excellent'),
                DB::raw('SUM(CASE WHEN mr.rating = "4" THEN "4" ELSE "0" END)/COUNT(mr.id) as good'),
                DB::raw('SUM(CASE WHEN mr.rating = "3" THEN "3" ELSE "0" END)/COUNT(mr.id) as average'),
                DB::raw('SUM(CASE WHEN mr.rating = "2" THEN "2" ELSE "0" END)/COUNT(mr.id) as bad'),
                DB::raw('SUM(CASE WHEN mr.rating = "1" THEN "1" ELSE "0" END)/COUNT(mr.id) as poor'),
            )
            ->leftJoin('maids as m', 'mr.maid_id', 'm.maid_id')
            ->leftJoin('service_types as st', 'mr.service_type_id', 'st.service_type_id')
            ->leftJoin('customers as c', 'mr.rated_by_customer_id', 'c.customer_id')
            ->where(['c.customer_status' => 1])
            ->groupBy('m.maid_id')
            ->orderBy('m.maid_id', 'ASC')
            ->get();
        //Cache::put('maid_reviews_by_customer_summary', $maid_reviews_by_customer_summary, 60); // cache it if slow down
        /************************************************************* */
        /// get all maids except leave maids
        $other_company_maids = getActiveOtherCompanyMaids();
        $other_company_maids_ids = array_column($other_company_maids->toArray(), 'maid_id');
        $maids = getActiveMaids();
        $maid_ids = array_column($maids->toArray(), 'maid_id');
        $maid_leaves = getLeaveMaidsByDate($input['date']);
        $maid_leaves_maid_ids = array_column($maid_leaves->toArray(), 'maid_id');
        $except_maid_ids = $other_company_maids_ids; // except other company maids
        $except_maid_ids = array_merge($except_maid_ids, $maid_leaves_maid_ids); // except leave maids also
        $maid_ids = array_values(array_diff($maid_ids, $except_maid_ids));
        //dd($maid_ids);
        /************************************************************* */
        $booking = new stdClass();
        $booking->service_week_day = Carbon::parse($input['date'])->format('w');
        $booking->service_start_date = $input['date'];
        $booking->service_actual_end_date = $input['date'];
        $booking->booking_type = $input['frequency'];
        $booking->time_from = $input['time_from'];
        $booking->time_to = $input['time_to'];
        $booking->service_end = $input['frequency'] == "OD" ? "1" : "0";
        /*************************************** */
        $except_booking_ids = [];
        if (@$input['booking_id']) {
            $except_booking_ids = array($input['booking_id']);
        }
        /************************************************************* */
        if (@$input['zone_id']) {
            // get all current bookings on the date
            $busy_bookings_by_date = get_busy_bookings_by_date($booking, $except_booking_ids);
            $busy_bookings_by_date_busy_maid_ids = array_unique(array_column($busy_bookings_by_date->toArray(), 'maid_id'));
            $full_free_maids = array_diff($maid_ids, $busy_bookings_by_date_busy_maid_ids);
            //dd($full_free_maids);
            $zone_id = $input['zone_id'];
            // filter bookings under the selected zone
            $zone_id_bookings = array_filter($busy_bookings_by_date->toArray(), function ($booking) use ($zone_id) {
                return in_array($zone_id, [$booking->booking_zone_id]);
            });
            // get busy maids from filtered zone
            $zone_id_booking_maid_ids = array_unique(array_column($zone_id_bookings, 'maid_id'));
            //dd($zone_id_booking_maid_ids);
            $booking->where_in_maids = $zone_id_booking_maid_ids;
            // get busy bookings by maids and zone id by date
            /************************************************************* */
            $busy_bookings = get_busy_bookings($booking, $except_booking_ids);
            //dd($input['time_from']);
            // trick for BW skipping
            if ($input['frequency'] == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                    if ($busy_booking->booking_type == "BW" && $busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            /************************************************************* */
            // get busy maids
            $zone_id_booking_busy_maid_ids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
            //dd($zone_id_booking_busy_maid_ids);
            // compare all busy maids (any time on same day) with busy maids on selected time
            $free_maids = array_diff($zone_id_booking_maid_ids, $zone_id_booking_busy_maid_ids);
            $free_maids = array_merge($free_maids, $full_free_maids); // freemaid includes total free maids and zonely free maids
            //dd($free_maids);
            //dd($free_maids);
        } else {
            /************************************************************* */
            $busy_bookings = get_busy_bookings($booking, $except_booking_ids);
            // trick for BW skipping
            if ($input['frequency'] == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                    if ($busy_booking->booking_type == "BW" && $busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            /************************************************************* */
            $busy_maids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
            $free_maids = array_diff($maid_ids, $busy_maids);
        }
        /************************************************************* */
        $response['available_maid_list'] = DB::table('maids as m')
            ->select(
                'm.maid_id',
                //'m.maid_name as name',
                //DB::raw('IFNULL(maid_name_customer_app,m.maid_name) as name'),
                DB::raw('IF(m.maid_name_customer_app IS NULL or m.maid_name_customer_app = "", m.maid_name, m.maid_name_customer_app) as name'),
                DB::raw('CONCAT("' . Config::get('values.maid_avatar_prefix_url') . '",(CASE WHEN m.maid_photo_file = "" THEN "' . Config::get('values.maid_avatar_default_file') . '" ELSE m.maid_photo_file END)) as maid_img_url'),
                'm.maid_notes as maid_details',
                DB::raw('"0.0" as rating_count'),
            )
            ->whereIn('m.maid_id', $free_maids)
            ->whereNotIn('m.maid_id', $except_maid_ids)
            ->orderByRaw("FIELD(m.maid_id, " . implode(',', $free_maids) . ")") // order by fully free maids asc
        //->orderBy('m.maid_name', 'ASC')
            ->get();
        $rating_details = array('excellent' => 0, 'good' => 0, 'average' => 0, 'bad' => 0, 'poor' => 0);
        foreach ($response['available_maid_list'] as $key => $maid) {
            $review_list = [];
            $overall_rating = 0;
            $rating_count = 0;
            foreach ($maid_reviews_by_customer as $maid_review) {
                if ($maid_review->maid_id == $maid->maid_id) {
                    $overall_rating += $maid_review->rating;
                    $rating_count++;
                    $review_list[] = $maid_review;
                }
            }
            $response['available_maid_list'][$key]->rating_count = $rating_count;
            $response['available_maid_list'][$key]->overall_rating = $rating_count > 0 ? ($overall_rating / $rating_count) : 0;
            $summary_key = array_search($maid->maid_id, array_column($maid_reviews_by_customer_summary->toArray(), 'maid_id'));
            //dd($summary_key);
            $response['available_maid_list'][$key]->review_list = $review_list;
            $response['available_maid_list'][$key]->rating_details = ($summary_key !== false) ? $maid_reviews_by_customer_summary->toArray()[$summary_key] : $rating_details;
        }
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function test(Request $request)
    {
        $maid_reviews_by_customer = DB::table('maid_rating as mr')
            ->select(
                'm.maid_id',
                'c.customer_id',
                'c.customer_name',
                'mr.rating',
                DB::raw('null as rating_details'),
                DB::raw('DATE_FORMAT(mr.created_at,"%Y-%m-%d") as date'),
            )
            ->leftJoin('maids as m', 'mr.maid_id', 'm.maid_id')
            ->leftJoin('service_types as st', 'mr.service_type_id', 'st.service_type_id')
            ->leftJoin('customers as c', 'mr.rated_by_customer_id', 'c.customer_id')
            ->where(['c.customer_status' => 1])
            ->orderBy('m.maid_id', 'ASC')
            ->get();
        dd($maid_reviews_by_customer->toArray());
        $maid_reviews_by_customer_summary = DB::table('maid_rating as mr')
            ->select(
                'm.maid_id',
                DB::raw('CAST(SUM(CASE WHEN mr.rating = "5" THEN "1" ELSE "0" END) AS INT) as excellent'),
                DB::raw('CAST(SUM(CASE WHEN mr.rating = "4" THEN "1" ELSE "0" END) AS INT) as good'),
                DB::raw('CAST(SUM(CASE WHEN mr.rating = "3" THEN "1" ELSE "0" END) AS INT) as average'),
                DB::raw('CAST(SUM(CASE WHEN mr.rating = "2" THEN "1" ELSE "0" END) AS INT) as bad'),
                DB::raw('CAST(SUM(CASE WHEN mr.rating = "1" THEN "1" ELSE "0" END) AS INT) as poor'),
            )
            ->leftJoin('maids as m', 'mr.maid_id', 'm.maid_id')
            ->leftJoin('service_types as st', 'mr.service_type_id', 'st.service_type_id')
            ->leftJoin('customers as c', 'mr.rated_by_customer_id', 'c.customer_id')
            ->where(['c.customer_status' => 1])
            ->groupBy('m.maid_id', 'c.customer_id')
            ->orderBy('m.maid_id', 'ASC')
            ->get();
        dd($maid_reviews_by_customer_summary->toArray());
    }
}
