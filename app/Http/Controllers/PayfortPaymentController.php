<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Frequencies;
use App\Models\CustomerPayments;
use App\Models\OnlinePayment;
use Response;
use stdClass;

class PayfortPaymentController extends Controller
{
    public function booking_payment_payfort_process(Request $request, $booking_id)
    {
        try {
            DB::beginTransaction();
            $payfort_data = $request->all();
            $booking = Booking::where('booking_id', $booking_id)->first();
            if (@$payfort_data['response_message'] != 'Success') {
                throw new \ErrorException('Payment failed.');
            }
            /********************************************************************************************* */
            $onlinepayment = OnlinePayment::where([['reference_id', '=', $payfort_data['merchant_reference']], ['booking_id', '=', $booking->booking_id]])->first();
            if (!$onlinepayment) {
                $payment = new OnlinePayment();
                $payment->booking_id = $booking->booking_id;
                $payment->transaction_id = $payfort_data['fort_id'];
                $payment->amount = $payfort_data['amount'] / 100;
                $payment->transaction_charge = 0;
                $payment->customer_id = $booking->customer_id;
                $payment->description = null;
                $payment->payment_status = 'success';
                $payment->ip_address = @$request->ip();
                $payment->user_agent = @$request->header('User-Agent');
                $payment->return_data = json_encode($payfort_data);
                $payment->paid_from = 'W';
                $payment->payment_datetime = Carbon::now();
                $payment->debitType = "MANUAL";
                $payment->reference_id = $booking->reference_id;
                $payment->save();
            } else {
                $onlinepayment->payment_status = 'success';
                $onlinepayment->return_data = json_encode($payfort_data);
                $onlinepayment->save();
            }
            /********************************************************************************************* */
            if ($booking->booking_common_id) {
                $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            } else {
                $bookings_ids = [$booking->booking_id];
            }
            Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
            /********************************************************************************************* */
            // success app notify
            $notify = new stdClass();
            $notify->customer_id = $booking->customer_id;
            $notify->booking_id = $booking->booking_id;
            $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
            $notify->content = "Booking is confirmed with Ref. Id. {{booking_ref_id}} and payment Ref. Id. " . $payfort_data['fort_id'] . ".";
            addCustomerNotification($notify);
            /********************************************************************************************* */
            // send mails
            try {
                $mail = new AdminApiMailController;
                $mail->booking_confirmation_to_admin($booking->booking_id);
                $mail->booking_confirmation_to_customer($booking->booking_id);
            } catch (\Exception $e) {
            }
            /********************************************************************************************* */
            DB::commit();
            return Response::json([
                'result' =>
                    [
                        'status' => 'success',
                        'message' => 'Payment verified successfully.',
                        'payfort_data' => $payfort_data,
                        'booking' => $booking
                    ]
            ], 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json([
                'result' =>
                    [
                        'status' => 'failed',
                        'message' => $e->getMessage(),
                        'payfort_data' => $payfort_data,
                        'booking' => $booking
                    ]
            ], 200, [], JSON_PRETTY_PRINT);
        }
    }
}
