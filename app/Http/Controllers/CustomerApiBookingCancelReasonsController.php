<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiBookingCancelReasonsController extends Controller
{
    public function cancel_reasons(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $reasons = DB::table('booking_cancel_reasons as bcr')
            ->select(
                'bcr.id',
                'bcr.reason',
            )
            ->where([['bcr.deleted_at', '=', null]])
            ->orderBy('bcr.order_priority', 'ASC')
            ->get();
        $response['reason_list'] = [];
        foreach($reasons as $key => $row){
            $response['reason_list'][] = $row->reason;
        }
        $response['message'] = sizeof($response['reason_list']) ? "Cancel reasons fetched successfully." : "No cancel reasons found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
