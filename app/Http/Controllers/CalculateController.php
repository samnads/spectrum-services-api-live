<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CalculateController extends Controller
{
    public function calculate(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['date'] = "12/09/2023"; // dd/mm/yyyy format
                $data['params']['frequency'] = "WE";
                $data['params']['time'] = "18:00";
                $data['params']['hours'] = 2;
                $data['params']['professionals_count'] = 2;
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    //array('professional_id' => 424),
                    //array('professional_id' => 457),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
                $data['params']['cleaning_materials'] = "no";
                $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
                $data['params']['instructions'] = "my instruction";
                $data['params']['promocode_id'] = null;
                $data['params']['PaymentMethod'] = 1;
                $data['params']['No_weeks'] = 0;
                $data['params']['service_type_id'] = 1;
                $data['params']['offer_id'] = null;
                $data['params']['package_ids'] = []; //[1, 2];
                $data['params']['frequency_discount'] = 10;
                $data['params']['rush_slot_id'] = 7;
                $data['params']['addons'] = [array('service_addons_id' => 2, 'quantity' => 1), array('service_addons_id' => 1, 'quantity' => 1)];
            }
            $input = @$data['params'];
            /************************************************************* */
            // format for machine :/
            if (@$input['date']) {
                $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            }
            /************************************************************* */
            $response['calculation_data'] = calculate($input);
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function calculateOld(Request $request)
    {
        try {
            set_time_limit(300);
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['date'] = "04/10/2023"; // dd/mm/yyyy format
                $data['params']['frequency'] = "OD";
                $data['params']['time'] = "13:00";
                $data['params']['hours'] = 2;
                $data['params']['professionals_count'] = 2;
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    array('professional_id' => 424),
                    array('professional_id' => 457),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
                $data['params']['cleaning_materials'] = "yes";
                $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
                $data['params']['instructions'] = "my instruction";
                $data['params']['promocode_id'] = 131;
                $data['params']['PaymentMethod'] = 2;
                $data['params']['No_weeks'] = 0;
                $data['params']['service_type_id'] = 1;
                $data['params']['offer_id'] = null;
                $data['params']['package_ids'] = []; //[1, 2];
                $data['params']['frequency_discount'] = 10;
                $data['params']['rush_slot_id'] = 8;
                $data['params']['addons'] = [array('service_addons_id' => 1, 'quantity' => 1), array('service_addons_id' => 2, 'quantity' => 1)];
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            /************************************************************* */
            // if offer get data from offer data
            if (@$input['offer_id']) {
                $offer = DB::table('special_offers as so')
                    ->select(
                        'so.*',
                        'f.code as frequency_code',
                    )
                    ->leftJoin('frequencies as f', 'so.frequency_id', 'f.id')
                    ->where(['so.id' => $input['offer_id']])
                    ->first();
                if (!$offer) {
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid offer !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
                // change based on offer
                $input['frequency'] = $offer->frequency_code;
                $input['professionals_count'] = $offer->no_of_maids;
                $input['hours'] = $offer->no_of_hours;
                $input['cleaning_materials'] = $offer->cleaning_material_included == 'Y' ? 'yes' : 'no';
            }
            // if offer get data from offer data
            else if (@$input['package_ids']) {
                $packages = DB::table('building_type_room_packages as btrp')
                    ->select(
                        'btrp.*'
                    )
                    ->whereIn('btrp.id', $input['package_ids'])
                    ->where([['btrp.deleted_at', "=", null]])
                    ->get();
                //dd(@$packages);
                if (!$packages) {
                    return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid package !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
                // change based on package
                $input['frequency'] = "OD";
                $input['professionals_count'] = 1;
                $input['hours'] = 2;
                $input['cleaning_materials'] = 'no';
            }
            /************************************************************* */
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['PaymentMethod']])
                ->first();
            if (!$payment_type) {
                // validate payment mode
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid payment type !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            $input['time_from'] = Carbon::createFromFormat('H:i', $input['time'])->format('H:i:s');
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinute($input['hours'] * 60)->format('H:i:s');
            /************************************************************* */
            $response['input'] = $input;
            $response['status'] = 'success';
            $response['message'] = 'Amount calculated successfully';
            return Response::json($response, 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
