<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingController extends Controller
{
    public function hourly_rate(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['working_hours'] = 2;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'working_hours' => 'required|integer|min:2',
            ],
            [],
            [
                'working_hours' => 'Working Hours',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['debug_input'] = new stdClass();
        $response['debug_input']->working_hours = $input['working_hours'];
        $response['hourly_rate'] = new stdClass();
        $response['hourly_rate']->service = getHourlyRateByHourAndMaterial($input['working_hours'], 'N');
        $response['hourly_rate']->cleaning_material = getHourlyRateByHourAndMaterial($input['working_hours'], 'Y') - getHourlyRateByHourAndMaterial($input['working_hours'], 'N');
        $response['hourly_rate']->service_with_cleaning_material = getHourlyRateByHourAndMaterial($input['working_hours'], 'Y');
        $response['hourly_rate']->service_without_cleaning_material = getHourlyRateByHourAndMaterial($input['working_hours'], 'N');
        $response['message'] = 'Rates fetched successfully.';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
