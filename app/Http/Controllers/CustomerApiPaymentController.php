<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Carbon\Carbon;
use App\Models\Customer;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\OnlinePayment;
use Response;
use stdClass;

class CustomerApiPaymentController extends Controller
{
    public function change_payment_mode(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_id'] = 403455;
            $data['params']['PaymentMethod'] = 1;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
                'PaymentMethod' => 'required|integer',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'PaymentMethod' => 'Payment Method',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        // validate booking
        $booking = DB::table('bookings as b')
            ->select(
                'b.*',
            )
            ->where([['b.booking_id', '=', $input['booking_id']], ['b.customer_id', '=', $input['id']]])->first();
        if (!$booking) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        // format for machine :/
        $input['time_from'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->format('H:i:s');
        $input['hours'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->diffInMinutes(Carbon::createFromFormat('H:i:s', $booking->time_to)) / 60; // based on db
        $input['time_to'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->addMinutes($input['hours'] * 60)->format('H:i:s');
        //dd($input['hours']);
        /****************************************************************************************** */
        if ($booking->booking_common_id) {
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'b.booking_common_id',
                )
                ->where([['booking_common_id', '=', $booking->booking_common_id], ['b.customer_id', '=', $input['id']]])->get();
            $booking_ids = array_column($bookings->toArray(), 'booking_id');

        } else {
            $booking_ids = [$input['booking_id']];
        }
        /************************ */
        $payment_type = DB::table('payment_types as pt')
            ->select(
                'pt.*',
            )
            ->where(['pt.id' => $input['PaymentMethod']])
            ->first();
        /************************ */
        $service_fee_total = 0;
        $cleaning_material_fee_total = 0;
        $total_discount = 0;
        $tax_amount = 0;
        $payment_type_charge = 0;
        $amount_payable = 0;
        DB::beginTransaction();
        try {
            foreach ($booking_ids as $key => $booking_id) {
                $row = Booking::find($booking_id);
                $total_amount_without_payament_charge = $row->total_amount - $row->payment_type_charge;
                $row->payment_type_charge = $payment_type->charge;
                $row->payment_type_id = $payment_type->id;
                $row->total_amount = $total_amount_without_payament_charge + $payment_type->charge;
                $row->web_book_show = 1;
                $row->booking_status = 0;
                /***************************************** */
                $row->_total_payable = $row->total_amount;
                $row->save();
                $service_fee_total += ($row->service_charge - $row->cleaning_material_fee);
                $cleaning_material_fee_total += $row->cleaning_material_fee;
                $total_discount += $row->discount;
                $tax_amount += $row->vat_charge;
                $payment_type_charge += $row->payment_type_charge;
                $amount_payable += $row->total_amount;
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /********************************************************************************************* */
        if ($payment_type->id == "1") {
            // cash mode
            $notify = new stdClass();
            $notify->customer_id = $input['id'];
            $notify->booking_id = $booking->booking_id;
            $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
            $notify->content = "Booking is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
            addCustomerNotification($notify);
        }
        /********************************************************************************************* */
        if ($input['PaymentMethod'] == 2) {
            // card payment
            $requestParams = [
                'command' => 'PURCHASE',
                'access_code' => Config::get('values.payfort_access_code'),
                'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
                'amount' => (float) $amount_payable * 100,
                'currency' => Config::get('values.currency_code'),
                'language' => strtolower(Config::get('values.language_code')),
                'customer_email' => Customer::where('customer_id', $booking->customer_id)->first()->email_address ?: 'test@example.com',
                'order_description' => $booking->reference_id,
                'merchant_reference' => $booking->reference_id,
                'merchant_extra' => $booking->reference_id,
                'return_url' => Config::get('values.web_booking_url') . "payment/payfort/processing",
            ];
            ksort($requestParams);
            $hash_value = Config::get('values.payfort_sha_request_phrase');
            foreach ($requestParams as $name => $value) {
                if ($value != null) {
                    $hash_value .= $name . '=' . $value;
                }
            }
            $hash_value .= Config::get('values.payfort_sha_request_phrase');
            $requestParams['signature'] = hash('sha256', $hash_value);
            $response['checkout_url'] = Config::get('values.payfort_payment_page_url');
            $response['checkout_data']['payfort_data'] = $requestParams;
        }
        DB::commit();
        $input['professionals_count'] = sizeof($booking_ids);
        $response['status'] = 'success';
        $response['booking_details'] = array(
            "booking_id" => $booking_ids[0],
            "total_hours" => $input['hours'],
            "professionals_count" => $input['professionals_count'],
            "booking_ref" => $booking->reference_id,
            "payment_method" => $payment_type->name,
            "status" => $input['PaymentMethod'] == 1 ? "Pending Approval" : "Payment Pending",
        );
        $response['payment_details'] = array(
            'service_fee' => $service_fee_total,
            "cleaning_materials" => $cleaning_material_fee_total,
            "discount" => $total_discount,
            "tax_amount" => $tax_amount,
            "payment_type_charge" => $payment_type_charge,
            "total" => $amount_payable,
        );
        $response['message'] = 'Payment mode changed to ' . $payment_type->name . ' successfully.';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function retry_payment_data(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_id'] = 403455;
            $data['params']['PaymentMethod'] = 1;
        }
        $input = @$data['params'];
        /************************************************************* */
    }
    public function new_online_payment(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            $input = @$data['params'];
            $response['input'] = $input;
            $response['customer'] = null;
            if (@$input['customer_id']) {
                $response['customer'] = Customer::find($input['customer_id']);
            }
            $reference_id = Config::get('values.online_payment_ref_prefix'). time();
            $online_payment = new OnlinePayment();
            $online_payment->transaction_id = null;
            $online_payment->booking_id = null;
            $online_payment->reference_id = $reference_id;
            $online_payment->amount = $input['amount'];
            $online_payment->transaction_charge = @$input['transaction_charge'];
            $online_payment->customer_id = @$input['customer_id'];
            $online_payment->description = null;
            $online_payment->payment_status = 'initialized';
            $online_payment->user_agent = @$input['user_agent'];
            $online_payment->post_data = null;
            $online_payment->return_data = null;
            $online_payment->paid_from = bookingPlatform('web');
            $online_payment->payment_datetime = now();
            $online_payment->save();
            $response['online_payment'] = $online_payment;
            $response['message'] = 'New payment initialized successfully.';
            /********************************************************************************************* */
            $requestParams = [
                'command' => 'PURCHASE',
                'access_code' => Config::get('values.payfort_access_code'),
                'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
                'amount' => (float) $input['total_payable'] * 100,
                'currency' => Config::get('values.currency_code'),
                'language' => strtolower(Config::get('values.language_code')),
                //'customer_name' => $response['customer']->customer_name,
                'customer_name' => preg_replace('/[^A-Za-z0-9\-]/', '', $response['customer']->customer_name),
                'customer_email' => Customer::where('customer_id', $input['customer_id'])->first()->email_address ?: 'test@example.com',
                'phone_number' => $response['customer']->mobile_number_1,
                'order_description' => preg_replace("![^a-z0-9]+!i", "-", $reference_id),
                'merchant_reference' => $reference_id,
                'return_url' => Config::get('values.web_booking_url') . 'payment/payfort/online-payment/process',
            ];
            ksort($requestParams);
            $hash_value = Config::get('values.payfort_sha_request_phrase');
            foreach ($requestParams as $name => $value) {
                if ($value != null) {
                    $hash_value .= $name . '=' . $value;
                }
            }
            $hash_value .= Config::get('values.payfort_sha_request_phrase');
            $requestParams['signature'] = hash('sha256', $hash_value);
            $response['payfort_payment_url'] = Config::get('values.payfort_payment_page_url');
            $response['payfort_params'] = $requestParams;
            /********************************************************************************************* */
            return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT);
            /************************************************************* */
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), JSON_PRETTY_PRINT);
        }
    }
    public function process_payfort_online_payment(Request $request)
    {
        try {
            $payfort_data = json_decode($request->getContent(), true);
            $response['payfort_data'] = $payfort_data;
            $online_payment = OnlinePayment::where('reference_id', $payfort_data['merchant_reference'])->first();
            $response['online_payment'] = $online_payment;
            if (@$payfort_data['fort_id'] && @$payfort_data['response_message'] == 'Success') {
                $response['status'] = 'success';
                $response['message'] = 'Payment verified successfully.';
                // update database
                $online_payment->transaction_id = $payfort_data['fort_id'];
                $online_payment->payment_status = 'success';
                $online_payment->post_data = null;
                $online_payment->return_data = json_encode($payfort_data);
                $online_payment->paid_from = bookingPlatform('web');
                $online_payment->payment_datetime = now();
                $online_payment->save();
                $response['online_payment'] = $online_payment;
                if (@$online_payment->customer_id) {
                }
            } else {
                throw new \ErrorException('Payment failed.');
            }
            return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
            return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function get_online_payment(Request $request, $payment_id)
    {
        $data['online_payment'] = OnlinePayment::find($payment_id);
        $data['customer'] = Customer::find($data['online_payment']->customer_id);
        return $data;
    }
}
