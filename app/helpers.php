<?php

use App\Models\Booking;
use App\Models\CouponCode;
use App\Models\Customer;
use App\Models\CustomerNotifications;
use App\Models\Frequencies;
use App\Models\OnlinePayment;
use App\Models\CustomerPaymentToken;
use App\Models\ServiceAddons;
use App\Models\RushSlotCharges;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AdminApiMailController;
use Config as Config;

function toggleDebug($debug = null)
{
    return false;
    // overide default
    if (isset($debug)) {
        return $debug;
    }
    // default true for local testing
    $locals = array(
        '127.0.0.1',
        'localhost',
        '::1',
    );
    if (in_array($_SERVER['REMOTE_ADDR'], $locals)) {
        return true;
    }
    return false;
}
function customerResponseJsonConstants()
{
    return JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
}
function getHourlyRateByHourAndMaterial($working_hours, $cleaning_material)
{
    $price_settings = DB::table('price_settings as ps')
        ->select(
            'ps.id',
            'ps.hour',
            'ps.price_c',
            'ps.price_n'
        )
        ->where(function ($query) use ($working_hours) {
            $query->where([['ps.hour', '=', $working_hours]]);
            $query->orWhere([['ps.hour', '<', $working_hours]]);
        })
        ->orderBy('ps.hour', 'DESC')
        ->first();
    if ($cleaning_material == 'Y') {
        return $price_settings->price_c;
    } else {
        return $price_settings->price_n;
    }
}
function applyCoupon($coupon_id, $customer_id, $date, $hours, $amount)
{
    $coupon = CouponCode::where([['coupon_id', '=', $coupon_id], ['status', '=', 1]])->first();
    if ($coupon) {
        $booking = Booking::where([['customer_id', '=', $customer_id], ['booking_status', '=', 1]])->first();
        if ($date > $coupon->expiry_date) {
            // expired coupon
            return 0;
        } else if ($booking && $coupon->coupon_type == "FT") {
            // not first booking
            return 0;
        } else if ($hours < $coupon->min_hrs) {
            // min hours check failed
            return 0;
        }
        $valid_week_days = explode(',', $coupon->valid_week_day);
        $week_day = Carbon::createFromFormat('Y-m-d', $date)->format('N') - 1;
        if (!in_array($week_day, $valid_week_days)) {
            return 0;
        }
        // valid coupon
        if ($coupon->discount_type == 0) {
            // percentage
            return $amount * ($coupon->percentage / 100);
        } else {
            // flat rate
            return $coupon->percentage;
        }
    } else {
        // not found
        return 0;
    }
}
function get_busy_bookings($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    $time_from = $booking->time_from;
    $time_to = $booking->time_to;
    $where_in_maids = @$booking->where_in_maids;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id'
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($where_in_maids) {
        $data->whereIn('m.maid_id', $where_in_maids);
    }
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    $data->where(function ($query) use ($time_from, $time_to) {
        $query->where([['b.time_from', '=', $time_from]])
            ->orWhere([['b.time_from', '=', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '>', $time_from], ['b.time_to', '<=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_to], ['b.time_to', '>', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_from]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_to]]);
    });
    return $data->get();
}
function get_busy_bookings_by_date($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id'
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    return $data->get();
}
function getActiveMaids()
{
    return DB::table('maids as m')
        ->select(
            'm.maid_id',
        )
        ->where(['m.maid_status' => 1])
        ->get();
}
function getActiveOtherCompanyMaids()
{
    return DB::table('maids as m')
        ->select(
            'm.maid_id',
        )
        ->where(['m.maid_status' => 1])
        ->where(function ($query) {
            $query->where([['m.maid_is_matic', "=", 1]])
                ->orWhere([['m.maid_is_justmaid', "=", 1]])
                ->orWhere([['m.maid_is_helpling', "=", 1]])
                ->orWhere([['m.maid_for_justmop', "=", 1]])
                ->orWhere([['m.maid_is_servicemarket', "=", 1]]);
        })->get();
}
function getLeaveMaidsByDate($date)
{
    return DB::table('maid_leave as ml')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->select(
            'm.maid_id',
            'ml.leave_id',
        )
        ->where([['ml.leave_date', "=", $date], ['ml.leave_status', "=", 1], ['m.maid_status', "=", 1]])
        ->get();
}
function is_time_slot_available($date, $time_from, $time_to)
{
    // date format - Y-m-d
    // time format - H:i:s
    $blocked_slot = DB::table('booking_slots as bs')
        ->select(
            'bs.*',
        )
        ->where(['bs.date' => $date, 'bs.status' => 1])
        ->where(function ($query) use ($time_from, $time_to) { // matching more fields
            $query->where([['bs.from_time', '=', $time_from]])
                ->orWhere([['bs.from_time', '<', $time_from], ['bs.to_time', '>', $time_to]])
                ->orWhere([['bs.from_time', '<', $time_to], ['bs.from_time', '>', $time_from]])
                ->orWhere([['bs.to_time', '>', $time_from], ['bs.to_time', '<', $time_to]]);
        })->first();
    if ($blocked_slot) {
        return "Sorry, time slots not available between " . Carbon::createFromFormat('H:i:s', $blocked_slot->from_time)->format('h:i A') . " - " . Carbon::createFromFormat('H:i:s', $blocked_slot->to_time)->format('h:i A') . " on " . Carbon::parse($date)->format('d/m/Y') . ".";
    }
    return true;
}
function isCustomerEligibleForBooking($customer_id)
{
    $customer = Customer::where(['customer_id' => $customer_id])->first();
    if ($customer->is_flag == 'Y') {
        return "Sorry, you're not allowed for booking, contact us.";
    }
    return true;
}
function addCustomerNotification($notify)
{
    $notification = new CustomerNotifications();
    $notification->customer_id = $notify->customer_id;
    $notification->booking_id = @$notify->booking_id;
    $notification->service_date = @$notify->service_date;
    $notification->content = $notify->content;
    $notification->save();
}
function time_elapsed_string($notify_time)
{
    // Code by : [Samnad S]
    if (date('Y') > Carbon::parse($notify_time)->format('Y')) {
        // less than this year
        $string = Carbon::parse($notify_time)->format('Y M d \a\t h:i A');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addWeeks(1)) {
        // greater than 1 week
        $string = Carbon::parse($notify_time)->format('M d \a\t h:i A');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addDays(2)) {
        // than 1 day
        $string = Carbon::parse($notify_time)->format('D \a\t h:i A');
    } else if (Carbon::now()->format('Y-m-d') == Carbon::parse($notify_time)->addDays(1)->format('Y-m-d')) {
        // 1 day before
        $string = Carbon::parse($notify_time)->format('\Y\e\s\t\e\r\d\a\y \a\t h:i A');
    } else if (Carbon::now()->format('Y-m-d H:i:s') >= Carbon::parse($notify_time)->addMinutes(60)->format('Y-m-d H:i:s') && Carbon::now()->format('Y-m-d H:i:s') < Carbon::parse($notify_time)->addMinutes(120)->format('Y-m-d H:i:s')) {
        // than 1 hour < 2 hour
        $string = Carbon::parse($notify_time)->format('1 \h\o\u\r \a\g\o');
    } else if (Carbon::now()->format('Y-m-d H:i:s') > Carbon::parse($notify_time)->addMinutes(120)) {
        // than 1 hour < 2 hour
        $hours = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%H'));
        $string = Carbon::parse($notify_time)->format($hours . ' \h\o\u\r\s \a\g\o');
    } else if (Carbon::now() >= Carbon::parse($notify_time)->addMinutes(1) && Carbon::now() < Carbon::parse($notify_time)->addMinutes(2)) {
        // 1 minute
        $string = Carbon::parse($notify_time)->format('1 \m\i\n\u\t\e \a\g\o');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addMinutes(2)) {
        // than 1 miutte
        $minutes = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%i'));
        $string = Carbon::parse($notify_time)->format($minutes . ' \m\i\n\u\t\e\s \a\g\o');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addSeconds(10)) {
        // than 10 seconds
        $seconds = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%s'));
        $string = Carbon::parse($notify_time)->format($seconds . ' \s\e\c\o\n\d\s \a\g\o');
    } else {
        // less than 10 seconds
        $string = 'Just now';
    }
    return $string;
}
function weekArray()
{
    // 0 for sunday
    return array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
}
function debug_mobiles()
{
    // for hijack real otp for testing purpose
    // add yours for debug
    return array(
        '979797979',
        '512341234',
        '501234556',
        '501234566'
    );
}
function send_customer_login_otp($mobile_number, $otp)
{
    $message = 'Spectrum login OTP - ' . $otp;
    $num = substr($mobile_number, -9);
    $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=spectrum&passwd=Spectrum@2040&mobilenumber=971' . $num . '&message=' . urlencode($message) . '&sid=Spectrom&mtype=N';
    $sms_url = str_replace(" ", '%20', $sms_url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $sms_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $res = curl_exec($ch);
    curl_close($ch);
}
function rushSlotsByDate($date)
{
    $rush_slot_charges = RushSlotCharges::select('id', 'date_from as debug_date_from', 'date_to as debug_date_to', 'time', 'extra_charge')
        //->where([['date_from', '>=', $date], ['date_to', '<=', $date]])
        ->whereRaw("? BETWEEN `date_from` AND `date_to`", [$date])
        ->where(['deleted_at' => null])
        ->orderBy('time', 'ASC')
        ->get();
    foreach ($rush_slot_charges as $key => $rush_slot) {
        $rush_slot_charges[$key]->time = Carbon::createFromFormat('H:i:s', $rush_slot->time)->format('H:i');
        $rush_slot_charges[$key] = $rush_slot;
    }
    return $rush_slot_charges;
}
function bookingHistoryMenu()
{
    $menu = new stdClass();
    $menu->retry_payment = false;
    $menu->change_payment_method = false;
    $menu->reschedule = false;
    $menu->cancel_this = false;
    $menu->cancel_all = false;
    return $menu;
}
function calculate($input)
{
    $service_type = DB::table('service_types as st')
        ->select(
            'st.service_type_model_id'
        )
        ->where([['st.service_type_id', "=", $input['service_type_id']]])
        ->first();
    $input['time'] = @$input['time'] ?: '08:00';
    $input['time_from'] = Carbon::createFromFormat('H:i', $input['time'])->format('H:i:s');
    /************************************************************************************************** */
    if (@$input['offer_id']) {
        $data['special_offer'] = DB::table('special_offers as so')
            ->select(
                'so.id as offer_id',
                'so.name as name',
                'so.service_type_id',
                'st.service_type_name as service_type',
                DB::raw('CONCAT("' . Config::get('values.special_offer_banner_prefix_url') . '",so.customer_app_banner_file) as imageurl'),
                DB::raw('"type" as type'),
                'so.start_date',
                'so.end_date',
                'f.name as frequency',
                'f.code as frequency_code',
                'f.id as frequency_id',
                'so.no_of_maids',
                'so.no_of_hours',
                'so.no_of_visits',
                DB::raw('"' . Carbon::parse(Config::get('values.work_end_time'))->format("H:i") . '" as max_working_hour'),
                DB::raw('(CASE WHEN so.cleaning_material_included= "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                DB::raw('so.offer_cleaning_material_rate as cleaning_material_rate'),
                DB::raw('so.actual_cleaning_material_rate as actual_material_cost'),
                DB::raw('so.offer_hourly_rate as hourly_rate'),
                DB::raw('so.actual_hourly_rate as actual_price'),
                'so.days as db_days',
                'so.dates as db_dates',
            )
            ->leftJoin('frequencies as f', 'so.frequency_id', 'f.id')
            ->leftJoin('service_types as st', 'so.service_type_id', 'st.service_type_id')
            ->where([['so.end_date', ">=", date('Y-m-d')], ['so.deleted_at', "=", null], ['so.id', "=", $input['offer_id']]])
            ->orderBy('so.end_date', 'ASC')
            ->first();
        // change based on offer
        $input['cleaning_materials'] = $data['special_offer']->cleaning_materials;
        $input['hours'] = $data['special_offer']->no_of_hours;
        $input['professionals_count'] = $data['special_offer']->no_of_maids;
        $input['frequency'] = $data['special_offer']->frequency_code;
        $input['No_weeks'] = $data['special_offer']->no_of_visits;
    }
    /************************************************************************************************** */
    $data['cleaning_material'] = (strtoupper(@$input['cleaning_materials'][0]) == 'Y') ? 'Y' : 'N';
    /************************************************************************************************** */
    $data['tax_percentage'] = Config::get('values.vat_percentage');
    $data['service_rate_per_hour'] = getHourlyRateByHourAndMaterial($input['hours'], 'N');
    $data['cleaning_material_rate_per_hour'] = $data['cleaning_material'] == 'Y' ? (getHourlyRateByHourAndMaterial($input['hours'], 'Y') - getHourlyRateByHourAndMaterial($input['hours'], 'N')) : null;
    $data['service_fee'] = $data['service_rate_per_hour'] * $input['hours'] * $input['professionals_count'];
    $data['cleaning_material_fee'] = $data['cleaning_material_rate_per_hour'] * $input['hours'] * $input['professionals_count'];
    /************************************************************************************************** */
    $data['rush_slot_id'] = @$input['rush_slot_id'];
    $data['rush_slot_charge'] = null;
    if (@$input['rush_slot_id']) {
        $rush_slot = RushSlotCharges::where(['id' => $input['rush_slot_id'], 'time' => $input['time_from'], 'deleted_at' => null])->first();
        $data['rush_slot_charge'] = @$rush_slot->extra_charge;
    }
    /************************************************************************************************** */
    $data['addons'] = [];
    $data['addons_amount'] = null;
    $data['addons_strike_amount'] = null;
    $data['addons_strike_discount'] = null;
    if (@$input['addons']) {
        foreach ($input['addons'] as $key => $addon) {
            $service_addon = ServiceAddons::findOrFail($addon['service_addons_id']);
            // save real db data so we can use this on booking creation
            $service_addon->quantity = $addon['quantity'];
            $data['addons_amount'] += $service_addon->amount * $addon['quantity'];
            $data['addons_strike_amount'] += $service_addon->strike_amount;
            $data['addons'][] = $service_addon;
        }
        $data['addons_strike_discount'] = $data['addons_strike_amount'] - $data['addons_amount'];
    }
    /************************************************************************************************** */
    $data['packages'] = [];
    $data['packages_strike_amount'] = null;
    $data['packages_amount'] = null;
    $data['packages_strike_discount'] = null;
    if ($service_type->service_type_model_id == 2) {
        foreach ((array) @$input['packages'] as $key => $value) {
            $package = DB::table('building_type_room_packages as btrp')
                ->select(
                    'btrp.*'
                )
                ->where('btrp.id', $value['package_id'])
                ->where([['btrp.deleted_at', "=", null]])
                ->first();
            $package->quantity = $value['quantity'];
            $data['packages_amount'] += $package->total_amount * $value['quantity'];
            $data['packages_strike_amount'] += $package->actual_total_amount * $value['quantity'];
            $data['packages'][] = $package;
        }
        $data['packages_strike_discount'] = $data['packages_strike_amount'] - $data['packages_amount'];
        $data['service_fee'] = $data['packages_amount'];
        $data['cleaning_material_fee'] = 0;
    }
    /************************************************************************************************** */
    $data['amount_before_discount'] = $data['service_fee'] + $data['cleaning_material_fee'] + $data['rush_slot_charge'] + $data['addons_amount'];
    /************************************************************************************************** */
    $data['coupon_applied'] = null;
    $data['coupon_discount'] = null;
    $data['coupon_code'] = null;
    $data['coupon_id'] = null;
    if (@$input['coupon_code']) {
        $coupon = DB::table('coupon_code as cc')
            ->select(
                'cc.*',
            )
            ->where(['cc.coupon_name' => $input['coupon_code']])
            ->where(['cc.status' => 1])
            ->where('cc.percentage', '>', 0)
            ->first();
        $coupon_discount = applyCouponNew($input, $data['amount_before_discount']);
        if (is_numeric($coupon_discount) && $coupon_discount > 0) {
            $percentage = number_format((($coupon_discount / ($data['amount_before_discount']))) * 100, 2);
            $data['coupon_applied'] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $coupon->coupon_name, 'title' => (float) $percentage . '% offer', 'message' => $coupon_discount . ' ' . Config::get('values.currency_code') . ' Discount applied !', 'status' => 'success'];
            $data['coupon_discount'] = $coupon_discount;
            $data['coupon_code'] = $coupon->coupon_name;
            $data['coupon_id'] = $coupon->coupon_id;
        } else {
            $data['coupon_applied'] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $input['coupon_code'], 'title' => 'Coupon code not valid !', 'message' => $coupon_discount, 'status' => 'failed'];
        }
    }
    /************************************************************************************************** */
    $data['frequency_discount'] = null;
    if (@$input['frequency']) {
        $frequency = Frequencies::where('code', $input['frequency'])->first();
        $data['frequency_discount'] = ($frequency->offer_percentage / 100) * $data['amount_before_discount'];
    }
    $data['discount_total'] = $data['frequency_discount'] + $data['coupon_discount'];
    $data['taxable_amount'] = $data['amount_before_discount'] - $data['discount_total'];
    $data['vat_amount'] = $data['taxable_amount'] * ($data['tax_percentage'] / 100);
    $data['taxed_amount'] = $data['taxable_amount'] + $data['vat_amount'];
    /************************************************************************************************** */
    $data['payment_type_charge'] = null;
    if (@$input['PaymentMethod']) {
        $payment_type = DB::table('payment_types as pt')
            ->select(
                'pt.*',
            )
            ->where(['pt.id' => $input['PaymentMethod']])
            ->first();
        if (@$payment_type->charge_type == "F") {
            // fixed charge
            $data['payment_type_charge'] = $payment_type->charge;
        } else if (@$payment_type->charge_type == "P") {
            // percentage of total
            $data['payment_type_charge'] = ($payment_type->charge / 100) * $data['taxed_amount'];
        } else {
            // percentage of total
            $data['payment_type_charge'] = 0;
        }
    }
    /************************************************************************************************** */
    $data['total_payable'] = $data['taxed_amount'] + $data['payment_type_charge'];
    if ($input['id'] == '87812') {
        $data['total_payable'] = 1;
    }
    $data['input'] = $input;
    $data['data'] = $data;
    return $data;
}
function bookingPlatform($platform)
{
    $platform = strtolower($platform);
    $platforms = [
        'mobile' => "M",
        'web' => "W",
        'admin' => "A",
        'unknown' => "A",
    ];
    return isset($platforms[$platform]) ? $platforms[$platform] : $platforms['unknown'];
}
function applyCouponNew($input, $amount)
{
    /************************************************** */
    $coupon = DB::table('coupon_code as cc')
        ->select(
            'cc.*',
            //'f.name as frequency',
            //DB::raw('IFNULL(st.service_type_name,st.service_type_name) as service_type_name'),
        )
        //->leftJoin('frequencies as f', 'cc.coupon_id', 'f.coupon_id')
        //->leftJoin('service_types as st', 'cc.service_id', 'st.service_type_id')
        ->where(['cc.coupon_name' => $input['coupon_code']])
        ->where(['cc.status' => 1])
        ->where('cc.percentage', '>', 0)
        ->first();
    /************************************************** */
    if ($coupon) {
        if (@$input['frequency']) {
            $frequency = DB::table('frequencies as f')
                ->select(
                    'f.*',
                )
                ->where(['f.code' => $input['frequency']])
                ->first();
        }
        /************************************************** */
        // frequency coupon found
        if (@$coupon->type == 'F') {
            // frequency coupon code
            if (@$frequency->coupon_id != $coupon->coupon_id) {
                return 'Coupon valid for ' . $coupon->frequency . ' bookings only.';
            }
        } else {
            if (@$input['frequency'] != "OD" && @$coupon->type != 'F') {
                return 'Coupon valid for one time bookings only.';
            }
            //dd($input['id']);
            $booking = Booking::where([['customer_id', '=', $input['id']], ['booking_status', '=', 1]])->first();
            $onetimecheck = Booking::where([['customer_id', '=', $input['id']], ['booking_status', '!=', 2], ['coupon_id', '=', $coupon->coupon_id]])->first();
            $input['date'] = @$input['date'] ?: date('Y-m-d');
            $input['hours'] = @$input['hours'] ?: 2;
            if ($input['date'] > $coupon->expiry_date) {
                // expired coupon
                return 'Coupon expired on ' . $coupon->expiry_date;
            } else if ($booking && $coupon->coupon_type == "FT") {
                // not first booking
                return 'Coupon is already used.';
            } else if ($input['hours'] < $coupon->min_hrs) {
                // min hours check failed
                return 'Coupon valid for minimum of ' . $coupon->min_hrs . ' working hours only.';
            } else if ($onetimecheck && $coupon->coupon_type == "OT") {
                // min hours check failed
                return 'Coupon valid for one time use only.';
            } /*else if ($coupon->service_id > 0 && $input['service_type_id'] != $coupon->service_id) {
      // min hours check failed
      return 'Coupon valid for ' . $coupon->service_type_name . ' only.';
  }*/
            $valid_week_days = explode(',', $coupon->valid_week_day); // 0 - SUNDAY
            $week_day = Carbon::createFromFormat('Y-m-d', $input['date'])->format('w'); // 0 - SUNDAY
            if (!in_array($week_day, $valid_week_days)) {
                return 'Coupon not valid for ' . weekArray()[$week_day] . '.';
            }
        }
        // valid coupon
        if ($coupon->discount_type == 0) {
            // percentage
            return $amount * ($coupon->percentage / 100);
        } else {
            // flat rate
            return $coupon->percentage;
        }
    } else {
        // not found
        return "Coupon code '" . $input['coupon_code'] . "' is invalid.";
    }
}
function after_payfort_apple_pay_success($payfort_data, $booking_common_id, $platform)
{
    $bookings = DB::table('bookings as b')
        ->select(
            'b.*',
        )
        ->where(function ($query) use ($booking_common_id) {
            $query->where('b.booking_common_id', $booking_common_id);
            $query->orWhere('b.booking_id', $booking_common_id);
        })
        ->orderBy('b.booking_id', 'ASC')
        ->get();
    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]); // update
    /******************************************************************** */
    // update database
    $online_payment = OnlinePayment::where('booking_id', $bookings[0]->booking_id)->where('reference_id', $bookings[0]->reference_id)->first();
    if ($online_payment) {
        if ($online_payment->payment_status != 'success') {
            $online_payment->transaction_id = $payfort_data['fort_id'];
            $online_payment->amount = $payfort_data['amount'] / 100;
            $online_payment->transaction_charge = 0;
            $online_payment->customer_id = $bookings[0]->customer_id;
            $online_payment->description = null;
            $online_payment->payment_status = 'success';
            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
            $online_payment->post_data = null;
            $online_payment->return_data = json_encode($payfort_data);
            $online_payment->paid_from = bookingPlatform(@$platform);
            $online_payment->payment_datetime = now();
            $online_payment->save();
        } else {
            throw new \ErrorException('Payment Already Verfied !');
        }
    } else {
        $online_payment = new OnlinePayment();
        $online_payment->transaction_id = $payfort_data['fort_id'];
        $online_payment->booking_id = $bookings[0]->booking_id;
        $online_payment->reference_id = $bookings[0]->reference_id;
        $online_payment->amount = $payfort_data['amount'] / 100;
        $online_payment->transaction_charge = 0;
        $online_payment->customer_id = $bookings[0]->customer_id;
        $online_payment->description = null;
        $online_payment->payment_status = 'success';
        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
        $online_payment->post_data = null;
        $online_payment->return_data = json_encode($payfort_data);
        $online_payment->paid_from = bookingPlatform(@$platform);
        $online_payment->payment_datetime = now();
        $online_payment->save();
        /**************************************** */
    }
    /**
     * send mails
     */
    $mail = new AdminApiMailController();
    $mail->booking_confirmation_to_admin($booking_common_id);
    $mail->booking_confirmation_to_customer($booking_common_id);
}
function save_card_data_from_payfort_payment_data($customer_id, $payfort_data)
{
    if (preg_match('~[0-9]+~', $payfort_data['card_number'])) {
        // if card number contains number, it's valid
        // if contains valid card number in response (card number not full of **** **** **** ****)
        $customer_payment_token = CustomerPaymentToken::where('customer_id', $customer_id)->where('card_number', $payfort_data['card_number'])->first();
        if ($customer_payment_token) {
            $customer_payment_token->payment_token = $payfort_data['token_name'];
            $customer_payment_token->card_holder_name = $payfort_data['card_holder_name'];
            $customer_payment_token->expiry_date = $payfort_data['expiry_date'];
            $customer_payment_token->card_type = $payfort_data['payment_option'];
            $customer_payment_token->addedAt = now();
            $customer_payment_token->status = 1;
            $customer_payment_token->save();
        } else {
            $customer_payment_token = new CustomerPaymentToken();
            $customer_payment_token->customer_id = $customer_id;
            $customer_payment_token->payment_token = $payfort_data['token_name'];
            $customer_payment_token->card_number = $payfort_data['card_number'];
            $customer_payment_token->card_holder_name = $payfort_data['card_holder_name'];
            $customer_payment_token->expiry_date = $payfort_data['expiry_date'];
            $customer_payment_token->card_type = $payfort_data['payment_option'];
            $customer_payment_token->addedAt = now();
            $customer_payment_token->status = 1;
            $customer_payment_token->save();
        }
    }
}
function initiate_online_payment_for_booking($booking_id, $post_data, $amount, $transaction_charge)
{
    $booking = Booking::find($booking_id);
    $online_payment = new OnlinePayment();
    $online_payment->transaction_id = null;
    $online_payment->booking_id = $booking_id;
    $online_payment->reference_id = $booking->reference_id;
    $online_payment->amount = $amount;
    $online_payment->transaction_charge = $transaction_charge;
    $online_payment->customer_id = $booking->customer_id;
    $online_payment->post_data = $post_data ? json_encode($post_data) : null;
    $online_payment->payment_status = 'initiated';
    $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
    $online_payment->paid_from = 'W';
    $online_payment->payment_datetime = now();
    $online_payment->save();
}

function last_digits($number, $limit)
{
    return substr($number, -$limit);
}